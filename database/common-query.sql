-- Delete all ticks
--DELETE from tick;
--DELETE FROM tick t WHERE t.datetime > 1604106539;

-- Count number of tick
SELECT COUNT(*) FROM tick;

-- Retrieve the latest downloaded tick
SELECT * FROM tick t ORDER BY t.datetime DESC, t.id DESC LIMIT 1;

SELECT * FROM tick t INNER JOIN instrument i on t.instrumentid = i.id INNER JOIN instrumenttype it on i.instrumenttypeid = it.id WHERE i.symbol = 'ES' AND it.name = 'FUT' ORDER BY t.datetime DESC, t.id DESC LIMIT 1;
SELECT * FROM tick t INNER JOIN instrument i on t.instrumentid = i.id WHERE i.symbol = 'ES' ORDER BY t.datetime DESC, t.id DESC LIMIT 1;
SELECT * FROM tick t INNER JOIN instrument i on t.instrumentid = i.id WHERE i.symbol = 'MES' ORDER BY t.datetime DESC, t.id DESC LIMIT 1;
SELECT * FROM tick t WHERE t.instrumentid=3 ORDER BY t.datetime DESC, t.id DESC LIMIT 1;


DELETE FROM candlestick;