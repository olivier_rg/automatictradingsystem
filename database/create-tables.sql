create table timeframe
(
	id bigserial not null
		constraint timeframe_pkey
			primary key,
	name text not null
);

alter table timeframe owner to postgres;

create table exchange
(
	id integer generated always as identity
		constraint "Exchange_pkey"
			primary key,
	name text not null
);

alter table exchange owner to postgres;

create table instrumenttype
(
	id serial not null
		constraint instrumenttype_pk
			primary key,
	name text not null
);

alter table instrumenttype owner to postgres;

create table instrument
(
	id bigserial not null
		constraint instrument_pkey
			primary key,
	symbol text not null,
	instrumentid integer not null
		constraint instrument_instrumenttype_id_fk
			references instrumenttype
);

alter table instrument owner to postgres;

create unique index instrument_id_uindex
	on instrument (id);

create table candlestick
(
	id bigserial not null
		constraint candlestick_pkey
			primary key,
	datetime timestamp not null,
	high numeric not null,
	open numeric not null,
	close numeric not null,
	low numeric not null,
	volume real not null,
	instrumentid bigint not null
		constraint candlestick_instrument_id_fk
			references instrument,
	timeframeid bigint not null
		constraint candlestick_timeframe_id_fk
			references timeframe
);

alter table candlestick owner to postgres;

create table tick
(
	id bigint generated always as identity (maxvalue 2147483647)
		constraint tick_pkey
			primary key,
	datetime bigint not null,
	price numeric not null,
	volume integer not null,
	exchangeid integer not null
		constraint tick_exchange
			references exchange,
	instrumentid integer
		constraint tick_instrument
			references instrument
);

alter table tick owner to postgres;

create index fki_tick_exchange
	on tick (exchangeid);

create index fki_tick_instrument
	on tick (instrumentid);

