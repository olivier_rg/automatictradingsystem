package com.automatictradingsystem.webapp;

import com.automatictradingsystem.webapp.communication.JmsProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
public class AppController {
    @Autowired
    JmsProducer jmsProducer;

    @RequestMapping("/")
    public String welcome() {
        return "index";
    }

    @RequestMapping("/start-counter")
    public String startCounter() {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.execute(() -> {
            for (int i = 0; i < 100; i++) {
                jmsProducer.send(i);
                System.out.println("Counter: " + i);
            }
        });

        return "index";
    }
}
