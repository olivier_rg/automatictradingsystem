package com.automatictradingsystem.webapp.communication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

public class to_delete {

    JmsTemplate jmsTemplate;

    @Value("/topic/counter")
    private String topic;

    public void sendMessage(int count){
        try{
            jmsTemplate.convertAndSend(topic, count);
        } catch(Exception e){
        }
    }
}