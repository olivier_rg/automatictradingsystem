package com.automatictradingsystem.webapp;


import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQTopic;

import javax.jms.*;

/**
 * Hello world!
 */
public class MessageBrokerTestbed {

    // todo: Implement the javax.jms.MessageListener interface rather than calling consumer.receive()
    // todo: Use transactional sessions
    // todo: Use a Topic rather than a queue

    public MessageBrokerTestbed() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616"); // ?create=false
        Connection connection;


        try {
            connection = connectionFactory.createTopicConnection();
            connection.start();
            ActiveMQSession session = (ActiveMQSession) connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            //Destination destination = session.createTopic("/topic/statechange");

            TopicPublisher topicPublisher = session.createPublisher((Topic) destination);

            int i = 100000000;
            for (int i1 = 0; i1 < i; i1++) {

                try {
                    TextMessage message = session.createTextMessage("Hey! I send you a tick!");
                    topicPublisher.send(message);
                } catch (JMSException e) {
                    e.printStackTrace();
                }


                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        thread(MessageBrokerTestbed.createBroker(), false);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread(MessageBrokerTestbed::new, false);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread(new HelloWorldConsumer(), false);
    }

    static ActiveMQDestination destination = new ActiveMQTopic("/topic/statechange");

    private static Runnable createBroker() {
        return () -> {
            BrokerService broker = new BrokerService();
            broker.setUseJmx(false);
            broker.setPersistent(false);
            //broker.setBrokerName("localhost");

            broker.setDestinations(new ActiveMQDestination[]{destination});

            try {
                broker.addConnector("vm://localhost:61618");
                broker.addConnector("tcp://localhost:61616");
                broker.addConnector("ws://localhost:61614");

                broker.start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    public static void thread(Runnable runnable, boolean daemon) {
        Thread brokerThread = new Thread(runnable);
        brokerThread.setDaemon(daemon);
        brokerThread.start();
    }

    public static class HelloWorldConsumer implements Runnable, ExceptionListener {
        public void run() {
            try {
                // Create a ConnectionFactory
                ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost:61618");

                // Create a Connection
                Connection connection = connectionFactory.createTopicConnection();
                connection.start();

                connection.setExceptionListener(this);

                // Create a Session
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

                // Create the destination (Topic or Queue)
                Destination destination = session.createTopic("/topic/statechange");

                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(message -> {
                    TextMessage textMessage = (TextMessage) message;
                    String text = null;
                    try {
                        text = textMessage.getText();
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Received: " + text);
                });

                /*consumer.close();
                session.close();
                connection.close();*/
            } catch (Exception e) {
                System.out.println("Caught: " + e);
                e.printStackTrace();
            }
        }

        public synchronized void onException(JMSException ex) {
            System.out.println("JMS Exception occured.  Shutting down client.");
        }
    }
}
