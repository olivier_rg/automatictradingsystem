package com.system.trading.chart.di;

import com.system.trading.chart.TradingChart;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.OrderManager;
import com.system.trading.core.ta.slope.SlopeFactory;
import dagger.BindsInstance;
import dagger.Component;

import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Component(modules = {PriceActionRegressionChartModule.class})
public interface ChartFactoryComponent {

    @Named("PriceActionRegressionStrategyChart")
    TradingChart createPriceActionRegressionStrategyChart();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder setFeedProvider(FeedProvider feedProvider);

        @BindsInstance
        Builder setOrderManager(OrderManager orderManager);

        @BindsInstance
        Builder setSlopeFactory(SlopeFactory slopeFactory);

        ChartFactoryComponent build();
    }
}
