package com.system.trading.chart;

import com.system.trading.core.order.Order;
import com.system.trading.core.order.OrderListener;
import com.system.trading.core.order.OrderStatus;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;

import java.util.Date;

public class TimeSeriesBuyListener implements OrderListener {
    private TimeSeries timeSeriesBuy;

    public TimeSeriesBuyListener(TimeSeries timeSeriesBuy) {
        this.timeSeriesBuy = timeSeriesBuy;
    }

    @Override
    public void notify(Order order) {
        if (order.getOrderStatus().equals(OrderStatus.FILLED)) {
            timeSeriesBuy.addOrUpdate(new Millisecond(new Date(order.getTimeFilledPrice() * 1000)), order.getFilledPrice());
        }
    }
}
