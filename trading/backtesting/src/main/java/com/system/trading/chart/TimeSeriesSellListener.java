package com.system.trading.chart;

import com.system.trading.core.order.Order;
import com.system.trading.core.order.OrderListener;
import com.system.trading.core.order.OrderStatus;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;

import java.util.Date;

public class TimeSeriesSellListener implements OrderListener {
    private TimeSeries timeSeriesSell;

    public TimeSeriesSellListener(TimeSeries timeSeriesSell) {
        this.timeSeriesSell = timeSeriesSell;
    }

    @Override
    public void notify(Order order) {
        if (order.getOrderStatus().equals(OrderStatus.CLOSED))
            timeSeriesSell.addOrUpdate(new Millisecond(new Date(order.getTimeClosePrice() * 1000)), order.getClosedPrice());
    }
}
