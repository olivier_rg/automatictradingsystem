package com.system.trading.backtesting.order;

import com.system.trading.core.Contract;
import com.system.trading.core.order.*;
import lombok.Data;

@Data
public class BackTestOrder implements Order {
    private static int idCount;
    private int id;
    private OrderType orderType;
    private OrderActionType orderActionType;
    private PriceCondition priceCondition;
    private Contract contract;
    private Float trailingStopPoint;
    private Float requestPrice = null;
    private Float filledPrice = null;
    private Long timeFilledPrice = null;
    private Float closedPrice = null;
    private Float priceStopLoss = null;
    private Float priceTakeProfit = null;
    private Long timeClosePrice = null;
    private int quantity;
    private boolean isMarketSell = false;

    public BackTestOrder() {
        id = idCount++;
    }

    @Override
    public int orderId() {
        return id;
    }

    @Override
    public OrderStatus getOrderStatus() {
        OrderStatus orderStatus = null;

        if (requestPrice != null && filledPrice == null && closedPrice == null)
            orderStatus = OrderStatus.REQUESTED;
        else if (requestPrice != null && filledPrice != null && closedPrice == null)
            orderStatus = OrderStatus.FILLED;
        else if (requestPrice != null && filledPrice != null && closedPrice != null)
            orderStatus = OrderStatus.CLOSED;

        return orderStatus;
    }

    public void updateTrailingStop(float tickPrice) {
        float newTrailingStop;
        if (orderActionType.equals(OrderActionType.BUY)) {
            newTrailingStop = tickPrice - trailingStopPoint;//.getStopPoint();
            if (priceStopLoss == null || priceStopLoss < newTrailingStop) {
                priceStopLoss = newTrailingStop;
            }
        } else if (orderActionType.equals(OrderActionType.SELL)) {
            newTrailingStop = tickPrice + trailingStopPoint;//.getStopPoint();
            if (priceStopLoss == null || priceStopLoss > newTrailingStop) {
                priceStopLoss = newTrailingStop;
            }
        }
    }

    @Override
    public String toString() {
        return "BackTestOrder = id: " + id + " request price: " + requestPrice + " fill price: " + filledPrice + " close price: " + closedPrice;
    }
}
