package com.system.trading.chart;

import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;

import java.util.Date;

public class TimeSeriesTickListener implements TickListener {
    private final TimeSeries series;

    public TimeSeriesTickListener(TimeSeries series) {
        this.series = series;
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        series.addOrUpdate(new Millisecond(new Date(tick.getDatetime() * 1000)), tick.getPrice());
    }
}
