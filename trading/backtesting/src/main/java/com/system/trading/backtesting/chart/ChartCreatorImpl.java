package com.system.trading.backtesting.chart;

import com.system.trading.chart.TradingChart;
import com.system.trading.chart.di.ChartFactoryComponent;
import com.system.trading.core.strategy.StrategyListener;
import com.system.trading.core.strategy.StrategyManager;
import com.system.trading.core.strategy.par.PriceActionRegressionStrategy;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class ChartCreatorImpl implements ChartCreator, StrategyListener {
    private final ChartFactoryComponent chartFactoryComponent;
    private final List<TradingChart> charts;
    private boolean enabled;

    @Inject
    public ChartCreatorImpl(StrategyManager strategyManager, ChartFactoryComponent chartFactoryComponent) {
        this.chartFactoryComponent = chartFactoryComponent;
        strategyManager.addStrategyListener(this);
        charts = new ArrayList<>();
    }

    @Override
    public void display() {
        charts.forEach(TradingChart::display);
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void addedNewStrategy(String strategyName) {
        if (strategyName.equals(PriceActionRegressionStrategy.class.getName()) && enabled)
            charts.add(chartFactoryComponent.createPriceActionRegressionStrategyChart());
    }
}
