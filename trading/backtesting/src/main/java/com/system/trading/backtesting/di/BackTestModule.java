package com.system.trading.backtesting.di;

import com.system.trading.backtesting.feed.BackTestFeedProvider;
import com.system.trading.backtesting.order.Account;
import com.system.trading.backtesting.order.BackTestOrderManager;
import com.system.trading.chart.di.ChartFactoryComponent;
import com.system.trading.chart.di.DaggerChartFactoryComponent;
import com.system.trading.core.CoreObjectId;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.InstrumentConfiguration;
import com.system.trading.core.order.OrderManager;
import com.system.trading.core.ta.slope.SlopeFactory;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Properties;

@Module
public class BackTestModule {
    @Singleton
    @Provides
    OrderManager orderManager(FeedProvider feedProvider, Account account, InstrumentConfiguration instrumentConfiguration) {
        return new BackTestOrderManager(feedProvider, account, instrumentConfiguration);
    }

    @Singleton
    @Provides
    ChartFactoryComponent provideChartFactoryComponent(FeedProvider feedProvider, OrderManager orderManager, SlopeFactory slopeFactory) {
        return DaggerChartFactoryComponent.builder().setFeedProvider(feedProvider).setOrderManager(orderManager)
                .setSlopeFactory(slopeFactory).build();
    }

    @Singleton
    @Provides
    FeedProvider feedProvider(@Named(CoreObjectId.PROPERTY_GENERAL) Properties properties) {
        return new BackTestFeedProvider(properties);
    }
}
