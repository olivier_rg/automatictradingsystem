package com.system.trading.backtesting.order;

import com.system.trading.core.order.AccountConfiguration;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Account {
    @Getter
    @Setter
    private float capital;

    private Account() {
    }

    public Account(AccountConfiguration accountConfiguration) {
        capital = accountConfiguration.getInitialCapital();
    }
}
