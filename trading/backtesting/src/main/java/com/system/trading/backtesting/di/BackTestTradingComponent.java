package com.system.trading.backtesting.di;

import com.system.trading.backtesting.chart.ChartManagerImpl;
import com.system.trading.backtesting.order.Account;
import com.system.trading.core.di.CommonPropertiesModule;
import com.system.trading.core.di.CoreTradingComponent;
import com.system.trading.core.di.CoreTradingModule;
import com.system.trading.core.order.InstrumentConfiguration;
import dagger.BindsInstance;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {BackTestModule.class, CoreTradingModule.class, CommonPropertiesModule.class})
public interface BackTestTradingComponent extends CoreTradingComponent {

    ChartManagerImpl chartManager();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder setAccount(Account account);

        @BindsInstance
        Builder setInstrumentConfiguration(InstrumentConfiguration instrumentConfiguration);

        BackTestTradingComponent build();
    }
}
