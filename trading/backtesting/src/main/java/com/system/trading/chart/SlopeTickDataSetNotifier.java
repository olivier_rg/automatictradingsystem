package com.system.trading.chart;

import com.system.trading.core.ta.slope.SlopeTickListener;
import com.system.trading.core.pojo.Tick;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import java.util.Date;
import java.util.List;

public class SlopeTickDataSetNotifier implements SlopeTickListener {
    private TimeSeriesCollection slopeDataSet;

    public SlopeTickDataSetNotifier(TimeSeriesCollection slopeDataSet) {

        this.slopeDataSet = slopeDataSet;
    }

    @Override
    public void calculatedSlopeValue(List<Tick> ticks, SimpleRegression regression) {
        if (ticks.size() > 0) {
            TimeSeries series = new TimeSeries("slope-" + ticks.get(0).getDatetime());
            series.setNotify(false);
            slopeDataSet.addSeries(series);

            ticks.forEach(tick -> {
                double predict = regression.predict(tick.getDatetime());
                series.addOrUpdate(new Millisecond(new Date(tick.getDatetime() * 1000)), predict);
            });
        }
    }
}
