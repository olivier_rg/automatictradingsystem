package com.system.trading.backtesting;

import com.system.trading.backtesting.di.BackTestTradingComponent;
import com.system.trading.backtesting.di.DaggerBackTestTradingComponent;
import com.system.trading.backtesting.feed.BackTestFeedProvider;
import com.system.trading.backtesting.order.Account;
import com.system.trading.backtesting.order.InstrumentConfigurationImpl;
import com.system.trading.core.Contract;
import com.system.trading.core.util.LoggingUtil;
import com.system.trading.core.SecurityType;
import com.system.trading.core.order.AccountConfiguration;
import com.system.trading.core.strategy.par.PriceActionRegressionPropertiesName;
import com.system.trading.core.strategy.par.PriceActionRegressionStrategy;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 *  Test a list of parameter and find the optimized parameter strategy of price action regression strategy.
 *  Print the profit of each tested parameter in console.
 *  VM Argument: -Doutput.filename=C:\Users\orhea\Desktop\strategy-parameter-optimizer.csv
 */
public class StrategyParameterOptimizer {
    private static final Logger LOGGER = Logger.getLogger(StrategyParameterOptimizer.class.getName());
    private static final int INITIAL_ACCOUNT_CAPITAL = 2000000000;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    private static String OUTPUT_FILENAME;
    private static int counter = 1;


    public static void main(String[] args) {
        OUTPUT_FILENAME = System.getProperty("output.filename");
        LoggingUtil.initializeLoggingLevel();

        // todo: what happen if margin requirement is not meet?

        //List<PriceActionRegressionPropertiesName> propertiesList = generateProperties();
        //Contract contract = getContract();

        /*try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_FILENAME, true));
            writer.append("Progress,Profit,StrategyParameters\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

       /* for (PriceActionRegressionPropertiesName properties : propertiesList) {
            Account account = createAccount();
            InstrumentConfigurationImpl instrumentConfiguration = new InstrumentConfigurationImpl();
            BackTestTradingComponent coreTrading = DaggerBackTestTradingComponent.builder().setAccount(account)
                    .setInstrumentConfiguration(instrumentConfiguration).build();
            coreTrading.strategyManager().add(PriceActionRegressionStrategy.class, contract, properties);
            BackTestFeedProvider feedProvider = (BackTestFeedProvider) coreTrading.feedProvider();

            try {
                feedProvider.setStartDate(sdf.parse("20200921 08:30:00").getTime() / 1000);
                feedProvider.setEndDate(sdf.parse("20201002 23:30:00").getTime() / 1000);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            coreTrading.feedProvider().add(contract);
            coreTrading.feedProvider().subscribe();

            if (coreTrading.orderManager().hasPosition(contract))
                account.setCapital(account.getCapital() + instrumentConfiguration.getMarginRequirement(contract.getSymbol()));

            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_FILENAME, true));
                writer.append(String.valueOf(counter)).append("/").append(String.valueOf(propertiesList.size())).append(",").append(String.valueOf(account.getCapital() - INITIAL_ACCOUNT_CAPITAL)).append(",").append(properties.toString()).append("\n");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            LOGGER.info("Progress " + counter + "/" + propertiesList.size() + " Profit: " + (account.getCapital() - INITIAL_ACCOUNT_CAPITAL) + " " + properties.toString());
            counter++;
        }*/
    }

    /*private static List<PriceActionRegressionPropertiesName> generateProperties() {
        List<PriceActionRegressionPropertiesName> properties = new ArrayList<>();

        // slopeSize 25 50 75 100 150 200 250 300 400 500
        // slopeValue 0.01 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5
        // takeProfitPoint  0.50 0.75 1 1.25 1.50 1.75 2
        // stopLossPoint  5 10 15 20

        List<Integer> slopSizeValues = Arrays.asList(*//*25, 50, 75, 100, 150,*//* 200, 250, 300, 400, 500);
        List<Float> slopValues = Arrays.asList(0.01f, 0.05f, 0.1f, 0.15f, 0.2f, 0.25f, 0.3f, 0.35f, 0.4f, 0.45f, 0.5f);
        List<Float> profitPointValues = Arrays.asList(0.50f, 0.75f, 1f, 1.25f, 1.50f, 1.75f, 2f);
        List<Integer> stopLossPointValues = Arrays.asList(5, 10, 15, 20);

        for (Integer slopSizeValue : slopSizeValues) {
            for (Float slopValue : slopValues) {
                for (Float profitPointValue : profitPointValues) {
                    for (Integer stopLossPointValue : stopLossPointValues) {
                        properties.add(
                                PriceActionRegressionPropertiesName.builder()
                                        .slopeSize(slopSizeValue)
                                        .slopeValue(slopValue)
                                        //.takeProfitPoint(profitPointValue)
                                        //.stopLossPoint(stopLossPointValue)
                                        .build());
                    }
                }
            }
        }

        return properties;
    }*/

    private static Account createAccount() {
        return new Account(AccountConfiguration.builder().initialCapital(INITIAL_ACCOUNT_CAPITAL).build());
    }

    private static Contract getContract() {
        return Contract.builder().
                symbol("ES").currency("USD").exchange("GLOBEX").secType(SecurityType.FUTURE).build();
    }
}
