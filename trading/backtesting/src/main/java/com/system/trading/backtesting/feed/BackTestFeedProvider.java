package com.system.trading.backtesting.feed;

import com.system.trading.core.Contract;
import com.system.trading.core.candle.CandleListener;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.feed.FeedProviderState;
import com.system.trading.core.feed.FeedProviderStateListener;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class BackTestFeedProvider implements FeedProvider {
    private static final Logger LOGGER = Logger.getLogger(BackTestFeedProvider.class.getName());
    private final List<TickListener> tickListeners;
    private final List<FeedProviderStateListener> feedProviderStateListeners;
    private long startDate = 0;
    private long endDate = 0;
    private Contract contract;
    private final Properties properties;

    public BackTestFeedProvider(Properties properties) {
        this.properties = properties;
        tickListeners = new ArrayList<>();
        feedProviderStateListeners = new ArrayList<>();
    }

    @Override
    public void addFeedProviderStateListener(FeedProviderStateListener feedProviderStateListener) {
        feedProviderStateListeners.add(feedProviderStateListener);
    }

    @Override
    public void addTickListener(TickListener tickListener) {
        tickListeners.add(tickListener);
    }

    @Override
    public void addCandleListener(CandleListener candleListener) {
        // Method not used
    }

    @Override
    public void add(Contract contract) {
        LOGGER.warn("Support only one contract at a time.");
        this.contract = contract;
    }

    @Override
    public void loadWithCandle() {
        LOGGER.error("Load with candle not implemented");
    }

    @Override
    public void loadWithTick(Date startLoadingDate) {
        LOGGER.error("Load with tick not implemented");
    }

    @Override
    public void subscribe() {
        notifyFeedProviderState(FeedProviderState.SUBSCRIBE);

        if (endDate == 0 || startDate == 0) {
            LOGGER.error("Start date and end date must be set");
        }

        long totalSecond = endDate - startDate;
        long NUMBER_OF_SECOND_PER_REQUEST = 86400 / 2 / 2;
        long requestNumber = totalSecond / NUMBER_OF_SECOND_PER_REQUEST;
        if (requestNumber == 0)
            requestNumber = 1;

        long requestStartDate = startDate;
        long requestEndDate = startDate + NUMBER_OF_SECOND_PER_REQUEST;

        String url = properties.getProperty("database.url");
        String user = properties.getProperty("database.username");
        String password = properties.getProperty("database.password");
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, user, password);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        for (long i = 0; i < requestNumber; i++) {
            String sqlQuery = "SELECT t.id, t.datetime, t.price, t.volume FROM tick t INNER JOIN exchange e ON " +
                    "e.id = t.exchangeid INNER JOIN " +
                    "instrument i ON i.id = t.instrumentid WHERE e.name = '" + contract.getExchange() + "' " +
                    " AND i.symbol = '" + contract.getSymbol() + "' AND t.datetime > " + requestStartDate + " And " +
                    "t.datetime < " + requestEndDate + " ORDER BY t.datetime ASC, t.id ASC";

            try {
                ResultSet rs = con.prepareStatement(sqlQuery).executeQuery();

                while (rs.next()) {
                    Tick tick = new Tick(rs.getLong("datetime"), rs.getFloat("price"), rs.getInt("volume"));
                    for (TickListener tickListener : tickListeners) {
                        tickListener.notify(tick, false);
                    }
                }

                rs.close();
            } catch (SQLException ex) {
                LOGGER.error(ex.getMessage(), ex);
            }

            requestStartDate = requestEndDate;
            requestEndDate = requestEndDate + NUMBER_OF_SECOND_PER_REQUEST;
        }

        try {
            con.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        notifyFeedProviderState(FeedProviderState.FINISHED_SUBSCRIBED);
    }

    private void notifyFeedProviderState(FeedProviderState feedProviderState) {
        feedProviderStateListeners.forEach(feedProviderStateListener -> feedProviderStateListener.notify(feedProviderState));
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
}
