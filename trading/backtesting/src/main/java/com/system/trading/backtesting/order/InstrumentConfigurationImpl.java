package com.system.trading.backtesting.order;

import com.system.trading.core.order.InstrumentConfiguration;

public class InstrumentConfigurationImpl implements InstrumentConfiguration {
    @Override
    public float getPipValue(String symbol) {
        float pipValue = 0;

        if (symbol.toUpperCase().equals("ES"))
            pipValue = 0.25f;
        else if(symbol.toUpperCase().equals("MES"))
            pipValue = 0.25f;


        return pipValue;
    }

    @Override
    public float getProfitPipValue(String symbol) {
        float profitPipValue = 0;

        if (symbol.toUpperCase().equals("ES"))
            profitPipValue = 12.5f;
        else if(symbol.toUpperCase().equals("MES"))
            profitPipValue = 1.25f;

        return profitPipValue;
    }

    @Override
    public float getTotalOneSideCommission(String symbol) {
        float totalCostCommission = 0;

        if (symbol.toUpperCase().equals("ES"))
            totalCostCommission = 2.10f;
        else if(symbol.toUpperCase().equals("MES"))
            totalCostCommission = 1.1f;

        return totalCostCommission;
    }

    @Override
    public float getMarginRequirement(String symbol) {
        float margin = 0;

        if(symbol.toUpperCase().equals("ES"))
            margin = 13800;
        else if(symbol.toUpperCase().equals("MES"))
            margin = 1380;

        return margin;
    }
}
