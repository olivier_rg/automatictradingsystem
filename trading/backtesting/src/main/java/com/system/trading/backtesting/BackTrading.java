package com.system.trading.backtesting;

import com.system.trading.backtesting.di.BackTestTradingComponent;
import com.system.trading.backtesting.di.DaggerBackTestTradingComponent;
import com.system.trading.backtesting.feed.BackTestFeedProvider;
import com.system.trading.backtesting.order.Account;
import com.system.trading.backtesting.order.BackTestOrder;
import com.system.trading.backtesting.order.BackTestOrderManager;
import com.system.trading.backtesting.order.InstrumentConfigurationImpl;
import com.system.trading.core.Contract;
import com.system.trading.core.SecurityType;
import com.system.trading.core.order.AccountConfiguration;
import com.system.trading.core.order.OrderActionType;
import com.system.trading.core.strategy.par.PriceActionRegressionStrategy;
import com.system.trading.core.util.LoggingUtil;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BackTrading {
    private static final Logger LOGGER = Logger.getLogger(BackTrading.class.getName());
    private final BackTestTradingComponent coreTrading;
    private final Account account;

    public BackTrading() {
        Contract contract = getContract();
        account = createAccount();
        coreTrading = DaggerBackTestTradingComponent.builder()
                .setAccount(account)
                .setInstrumentConfiguration(new InstrumentConfigurationImpl())
                .build();
        coreTrading.chartManager().initialize();
        BackTestFeedProvider feedProvider = (BackTestFeedProvider) coreTrading.feedProvider();

        coreTrading.strategyManager().add(PriceActionRegressionStrategy.class, contract);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        try {
            feedProvider.setStartDate(sdf.parse("20200919 08:30:00").getTime() / 1000);
            feedProvider.setEndDate(sdf.parse("20201120 17:30:00").getTime() / 1000);

            //feedProvider.setStartDate(sdf.parse("20201012 08:30:00").getTime() / 1000);
            //feedProvider.setEndDate(sdf.parse("20201016 11:30:00").getTime() / 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * -Dlogging.level=off|error|warn|info  default is off
     * -Dchart.enable=true default is false
     * <p>
     * -Dlogging.level=info -Dchart.enable=true
     *
     * @param args main argument
     */
    public static void main(String[] args) {
        LoggingUtil.initializeLoggingLevel();
        BackTrading backTrading = new BackTrading();
        backTrading.start();
    }

    private void start() {
        Date timeStart = new Date();
        coreTrading.feedProvider().subscribe();
        Date timeEnd = new Date();

        LOGGER.info("Simulation took:" + (timeEnd.getTime() - timeStart.getTime()) / 1000 + " seconds");
        calculatePositions(((BackTestOrderManager) coreTrading.orderManager()).getCloseOrders());
        System.out.println("End account capital: " + account.getCapital());
    }

    private Account createAccount() {
        return new Account(AccountConfiguration.builder().initialCapital(20000).build());
    }

    private Contract getContract() {
        return Contract.builder().
                symbol("ES").currency("USD").exchange("GLOBEX").secType(SecurityType.FUTURE).build();
    }

    private void calculatePositions(List<BackTestOrder> closeOrders) {
        int numberTradePositive = 0;

        for (BackTestOrder order : closeOrders) {
            float profit = order.getClosedPrice() - order.getFilledPrice();
            if (order.getOrderActionType().equals(OrderActionType.SELL))
                profit = profit * -1;

            if (profit > 0)
                numberTradePositive++;
        }

        System.out.println("Number of trades: " + closeOrders.size());
        System.out.println("Number of positive trade: " + numberTradePositive);
    }
}
