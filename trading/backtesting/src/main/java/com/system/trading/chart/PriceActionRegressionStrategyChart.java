package com.system.trading.chart;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.Iterator;

public class PriceActionRegressionStrategyChart extends JFrame implements TradingChart {
    private TimeSeries timeSeriesTick;
    private TimeSeries timeSeriesBuy;
    private TimeSeries timeSeriesSell;
    private TimeSeriesCollection slopeDataSet;

    public PriceActionRegressionStrategyChart(TimeSeries timeSeriesTick, TimeSeries timeSeriesBuy, TimeSeries timeSeriesSell, TimeSeriesCollection slopeDataSet) {
        this.timeSeriesTick = timeSeriesTick;
        this.timeSeriesBuy = timeSeriesBuy;
        this.timeSeriesSell = timeSeriesSell;
        this.slopeDataSet = slopeDataSet;

        XYPlot mainPlot = new XYPlot();
        mainPlot.setDomainAxis(new DateAxis("Date"));
        NumberAxis rangeAxis = new NumberAxis("Price");
        mainPlot.setRangeAxis(rangeAxis);

        // Tick
        mainPlot.setDataset(10, createTickDataSet());
        mainPlot.setRenderer(10, createTickRenderer());

        // Buy
        mainPlot.setDataset(2, createBuyDataSet());
        mainPlot.setRenderer(2, createBuyDotsRenderer());

        // Sell
        mainPlot.setDataset(3, createSellDataSet());
        mainPlot.setRenderer(3, createSellRenderer());

        // Slopes
        mainPlot.setDataset(4, slopeDataSet);
        mainPlot.setRenderer(4, createSlopeRenderer());
        disableSlopeLegend(mainPlot);

        rangeAxis.setAutoRangeIncludesZero(false);

        //Now create the chart and chart panel
        JFreeChart chart = new JFreeChart("Price Action Regression Strategy", null, mainPlot, true);
        ChartPanel chartPanel = createChartPanel(chart);

        mainPlot.setDomainPannable(true);
        mainPlot.setRangePannable(true);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(chartPanel);
        this.pack();
    }

    private void disableSlopeLegend(XYPlot plot) {
        LegendItemCollection legend = plot.getLegendItems();
        Iterator iterator = legend.iterator();
        while (iterator.hasNext()) {
            LegendItem item = (LegendItem) iterator.next();
            if (item.getLabel().toLowerCase().contains("slope-"))
                iterator.remove();
        }
        plot.setFixedLegendItems(legend);
    }

    private AbstractXYItemRenderer createSlopeRenderer() {
        XYLineAndShapeRenderer xyLineAndShapeRenderer = new XYLineAndShapeRenderer();
        //xyLineAndShapeRenderer.setDefaultPaint(Color.ORANGE);
        xyLineAndShapeRenderer.setDefaultShapesVisible(false);

        return xyLineAndShapeRenderer;
    }

    private XYItemRenderer createSellRenderer() {
        XYDotRenderer xyDotRenderer = new XYDotRenderer();
        xyDotRenderer.setDotWidth(3);
        xyDotRenderer.setDotHeight(3);
        xyDotRenderer.setSeriesPaint(0, Color.ORANGE);

        return xyDotRenderer;
    }

    private XYDataset createSellDataSet() {
        TimeSeriesCollection dataSet = new TimeSeriesCollection();
        dataSet.addSeries(timeSeriesSell);
        return dataSet;
    }

    private XYDataset createBuyDataSet() {
        TimeSeriesCollection dataSet = new TimeSeriesCollection();
        dataSet.addSeries(timeSeriesBuy);
        return dataSet;
    }

    private AbstractXYItemRenderer createBuyDotsRenderer() {
        XYDotRenderer xyDotRenderer = new XYDotRenderer();
        xyDotRenderer.setDotWidth(3);
        xyDotRenderer.setDotHeight(3);
        xyDotRenderer.setSeriesPaint(0, Color.BLUE);

        return xyDotRenderer;
    }

    private XYDataset createTickDataSet() {
        TimeSeriesCollection dataSet = new TimeSeriesCollection();
        dataSet.addSeries(timeSeriesTick);
        return dataSet;
    }

    private XYItemRenderer createTickRenderer() {
        XYDotRenderer xyDotRenderer = new XYDotRenderer();
        xyDotRenderer.setDotWidth(3);
        xyDotRenderer.setDotHeight(3);
        xyDotRenderer.setSeriesPaint(0, Color.BLACK);

        return xyDotRenderer;
    }

    private ChartPanel createChartPanel(JFreeChart chart) {
        ChartPanel chartPanel = new ChartPanel(chart, false);

        chartPanel.setMouseZoomable(false);
        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setDomainZoomable(true);
        chartPanel.setRangeZoomable(false);
        chartPanel.setPreferredSize(new Dimension(1680, 1100));
        chartPanel.setZoomTriggerDistance(Integer.MAX_VALUE);
        chartPanel.setFillZoomRectangle(false);
        chartPanel.setZoomOutlinePaint(new Color(0f, 0f, 0f, 0f));
        chartPanel.setZoomAroundAnchor(true);
        try {
            Field mask = ChartPanel.class.getDeclaredField("panMask");
            mask.setAccessible(true);
            mask.set(chartPanel, 0);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        chartPanel.addMouseWheelListener(arg0 -> chartPanel.restoreAutoRangeBounds());
        return chartPanel;
    }

    @Override
    public void display() {
        EventQueue.invokeLater(() -> {
            slopeDataSet.setNotify(true);
            timeSeriesBuy.setNotify(true);
            timeSeriesTick.setNotify(true);
            this.setVisible(true);
        });
    }
}


