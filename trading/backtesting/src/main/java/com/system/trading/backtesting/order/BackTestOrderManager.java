package com.system.trading.backtesting.order;

import com.system.trading.core.Contract;
import com.system.trading.core.SecurityType;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.*;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import lombok.Getter;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class BackTestOrderManager implements OrderManager, TickListener {
    private static final Logger LOGGER = Logger.getLogger(BackTestOrderManager.class.getName());
    private final List<BackTestOrder> requestOrders;
    private final List<BackTestOrder> activeOrders;
    private final List<OrderListener> orderListeners;
    private final InstrumentConfiguration instrumentConfiguration;
    @Getter
    private final List<BackTestOrder> closeOrders;
    private final Account account;


    public BackTestOrderManager(FeedProvider feedProvider, Account account, InstrumentConfiguration instrumentConfiguration) {
        this.account = account;
        this.instrumentConfiguration = instrumentConfiguration;
        orderListeners = new ArrayList<>();
        requestOrders = new ArrayList<>();
        activeOrders = new ArrayList<>();
        closeOrders = new ArrayList<>();

        feedProvider.addTickListener(this);
    }

    @Override
    public void cancelPendingOrders() {
        requestOrders.clear();
    }

    @Override
    public int buy(Contract contract, OrderType orderType, OrderActionType orderActionType, int quantityContract, OrderPriceInformation orderPriceInformation) throws Exception {
        if (quantityContract != 1) {
            throw new Exception("BackTestOrderManager - Does not support more or less than 1 traded contract at a time.");
        }
        if (orderPriceInformation.getRequestPrice() == null) {
            throw new Exception("BackTestOrderManager - When purchasing, the price cannot be null");
        }
        if (orderPriceInformation.getRequestPrice() == 0) {
            LOGGER.warn("Buying contract with price 0$");
        }

        BackTestOrder order = new BackTestOrder();
        order.setContract(contract);
        order.setOrderActionType(orderActionType);
        order.setOrderType(orderType);
        order.setQuantity(quantityContract);
        order.setRequestPrice(orderPriceInformation.getRequestPrice());
        order.setTrailingStopPoint(orderPriceInformation.getTrailingStopPoint());
        order.setPriceCondition(orderPriceInformation.getPriceCondition());

        if (orderPriceInformation.getStopLossPoint() != null) {
            if (orderActionType.equals(OrderActionType.BUY))
                order.setPriceStopLoss(order.getRequestPrice() - orderPriceInformation.getStopLossPoint());
            else
                order.setPriceStopLoss(order.getRequestPrice() + orderPriceInformation.getStopLossPoint());
        }

        if (orderPriceInformation.getTakeProfitPoint() != null) {
            if (order.getOrderActionType().equals(OrderActionType.BUY)) {
                order.setPriceTakeProfit(orderPriceInformation.getRequestPrice() + orderPriceInformation.getTakeProfitPoint());
            } else {
                order.setPriceTakeProfit(orderPriceInformation.getRequestPrice() - orderPriceInformation.getTakeProfitPoint());
            }
        }

        if (enoughMargin(order)) {
            requestOrders.add(order);
            orderListeners.forEach(orderListener -> orderListener.notify(order));
        }

        return order.orderId();
    }

    @Override
    public void sell(int i) {

    }

    private boolean enoughMargin(BackTestOrder order) {
        float marginRequirement = instrumentConfiguration.getMarginRequirement(order.getContract().getSymbol());
        boolean marginRequirementMeet = account.getCapital() >= marginRequirement;

        if (!marginRequirementMeet) {
            LOGGER.warn("Margin requirement not meet for order " + order.toString());
        }
        return marginRequirementMeet;
    }

    @Override
    public boolean hasPosition(Contract contract) {
        return activeOrders.size() > 0;
    }

    @Override
    public boolean hasPendingOrders() {
        return requestOrders.size() > 0;
    }

    @Override
    public List<Integer> getLimitPendingOrderIds() {
        List<Integer> ids = new ArrayList<>();
        requestOrders.forEach(backTestOrder -> ids.add(backTestOrder.orderId()));
        return ids;
    }

    @Override
    public void cancelPendingOrderById(int id) {
        List<BackTestOrder> ids = requestOrders.stream().filter(backTestOrder -> backTestOrder.orderId() == id).collect(Collectors.toList());
        boolean isContained = requestOrders.removeAll(ids);
    }

    @Override
    public Order getLimitPendingOrderById(int orderId) {
        List<BackTestOrder> orders = requestOrders.stream().filter(backTestOrder -> backTestOrder.orderId() == orderId).collect(Collectors.toList());
        return orders.get(0);
    }

    @Override
    public void addOrderListener(OrderListener orderListener) {
        orderListeners.add(orderListener);
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        updateOrdersTrailingStop(tick.getPrice());

        handleActiveOrders(tick);
        removeActiveOrders();

        handleRequestedOrder(tick);
        removeRequestedOrders();

        // Update trailing stop for newly created orders
        updateOrdersTrailingStop(tick.getPrice());
    }

    private void updateOrdersTrailingStop(float tickPrice) {
        for (BackTestOrder order : activeOrders) {
            if (order.getTrailingStopPoint() != null) {
                order.updateTrailingStop(tickPrice);
            }
        }
    }

    private void removeActiveOrders() {
        for (BackTestOrder closeOrder : closeOrders) {
            activeOrders.remove(closeOrder);
        }
    }

    private void handleActiveOrders(Tick tick) {
        for (BackTestOrder activeOrder : activeOrders) {
            // todo implement multi quantity position.
            if (tick.getVolume() > 0) {
                if (activeOrder.getOrderActionType().equals(OrderActionType.BUY) && isSellingLong(tick, activeOrder)) {
                    sell("Sell Long Filled", activeOrder, tick);
                } else if (activeOrder.getOrderActionType().equals(OrderActionType.SELL) && isSellingShort(tick, activeOrder)) {
                    sell("Sell Short Filled", activeOrder, tick);
                }
            }
        }
    }

    private boolean isSellingShort(Tick tick, BackTestOrder order) {
        boolean takeProfit = false;
        if (order.getPriceTakeProfit() != null) {
            takeProfit = tick.getPrice() <= order.getPriceTakeProfit();
        }

        boolean stoploss = false;
        if (order.getPriceStopLoss() != null) {
            stoploss = tick.getPrice() >= order.getPriceStopLoss();
        }

        return takeProfit || stoploss;
    }

    private boolean isSellingLong(Tick tick, BackTestOrder order) {
        // Take profit condition
        boolean takeProfit = false;
        if (order.getPriceTakeProfit() != null) {
            takeProfit = tick.getPrice() >= order.getPriceTakeProfit();
        }

        // Stop loss condition
        boolean stopLoss = false;
        if (order.getPriceStopLoss() != null) {
            stopLoss = tick.getPrice() <= order.getPriceStopLoss();
        }

        return takeProfit || stopLoss || order.isMarketSell();
    }

    private void sell(String logAction, BackTestOrder order, Tick tick) {
        if (tick.getVolume() <= 0)
            return;

        if (order.getContract().getSecType().equals(SecurityType.FUTURE)) {
            if (tick.getVolume() >= order.getQuantity()) {
                order.setClosedPrice(tick.getPrice());
                closeOrders.add(order);

                float symbolPLValue = (order.getClosedPrice() - order.getFilledPrice()) * order.getQuantity();

                if (order.getOrderActionType().equals(OrderActionType.SELL))
                    symbolPLValue = symbolPLValue * -1;

                float pipValue = instrumentConfiguration.getPipValue(order.getContract().getSymbol());
                float totalPipValue = symbolPLValue / pipValue;
                float profitLoss = totalPipValue * instrumentConfiguration.getProfitPipValue(order.getContract().getSymbol());
                profitLoss = profitLoss - instrumentConfiguration.getTotalOneSideCommission(order.getContract().getSymbol());
                float marginRequirement = instrumentConfiguration.getMarginRequirement(order.getContract().getSymbol());
                account.setCapital(account.getCapital() + marginRequirement + profitLoss);
                order.setTimeClosePrice(tick.getDatetime());
                logActionOrder(logAction, order, tick);
                tick.setVolume(tick.getVolume() - order.getQuantity());

                LOGGER.info("Account: " + account.toString());

                if (profitLoss < 0) {
                    LOGGER.info("Order sell with loss");
                    // Print into console if trade is successful. Used for building an feature data set
                    //LOGGER.info(0);
                } else {
                    LOGGER.info("Order sell with Profit");
                    // Print into console if trade is successful. Used for building an feature data set
                    //LOGGER.info(1);
                }

                orderListeners.forEach(orderListener -> orderListener.notify(order));
            }
        }
    }

    private void removeRequestedOrders() {
        for (BackTestOrder activeOrder : activeOrders) {
            requestOrders.remove(activeOrder);
        }
    }

    private void handleRequestedOrder(Tick tick) {
        if (requestOrders.size() > 0) {

            for (BackTestOrder order : requestOrders) {
                if (order.getOrderType().equals(OrderType.LIMIT)) {
                    if (order.getOrderActionType().equals(OrderActionType.BUY)) {
                        if (isBuyingLong(tick, order)) {
                            buy("Buy Long Filled", order, tick);
                        }
                    } else if (order.getOrderActionType().equals(OrderActionType.SELL)) {
                        if (isBuyingShort(tick, order)) {
                            buy("Buy Short Filled", order, tick);
                        }
                    }
                }
            }
        }
    }

    private void buy(String logAction, BackTestOrder order, Tick tick) {
        if (tick.getVolume() <= 0)
            return;

        if (order.getContract().getSecType().equals(SecurityType.FUTURE)) {
            if (tick.getVolume() >= order.getQuantity()) {
                float commission = instrumentConfiguration.getTotalOneSideCommission(order.getContract().getSymbol());
                float marginRequirement = instrumentConfiguration.getMarginRequirement(order.getContract().getSymbol());
                account.setCapital(account.getCapital() - marginRequirement - commission);

                order.setFilledPrice(tick.getPrice());
                order.setTimeFilledPrice(tick.getDatetime());
                activeOrders.add(order);
                logActionOrder(logAction, order, tick);
                tick.setVolume(tick.getVolume() - order.getQuantity());
                orderListeners.forEach(orderListener -> orderListener.notify(order));
            }
        }
    }

    private boolean isBuyingShort(Tick tick, BackTestOrder order) {
        return order.getRequestPrice() >= tick.getPrice() && isPriceConditionMeet(tick, order);
    }

    private boolean isBuyingLong(Tick tick, BackTestOrder order) {
        return order.getRequestPrice() <= tick.getPrice() && isPriceConditionMeet(tick, order);
    }

    private boolean isPriceConditionMeet(Tick tick, BackTestOrder order) {
        boolean isPriceConditionMeet = true;
        if (order.getPriceCondition() != null) {
            isPriceConditionMeet = order.getPriceCondition().state(tick.getPrice());
        }
        return isPriceConditionMeet;
    }

    private void logActionOrder(String orderAction, BackTestOrder order, Tick tick) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String formatDate = simpleDateFormat.format(new Date(tick.getDatetime() * 1000));
        LOGGER.info(orderAction + " - Time: " + formatDate + " " + order.toString());
    }

    public List<BackTestOrder> getPositionIds() {
        return this.activeOrders;
    }
}
