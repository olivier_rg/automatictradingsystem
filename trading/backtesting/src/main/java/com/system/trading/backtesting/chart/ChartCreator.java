package com.system.trading.backtesting.chart;

public interface ChartCreator {
    void display();

    void setEnabled(boolean enableChart);
}
