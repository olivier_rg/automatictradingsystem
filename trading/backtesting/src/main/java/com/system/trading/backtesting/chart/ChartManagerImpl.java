package com.system.trading.backtesting.chart;

import com.system.trading.core.Initializer;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.feed.FeedProviderState;
import com.system.trading.core.feed.FeedProviderStateListener;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ChartManagerImpl implements Initializer, FeedProviderStateListener {
    private static final Logger LOGGER = Logger.getLogger(ChartManagerImpl.class.getName());
    private final ChartCreator chartCreator;
    private boolean enableChart;

    @Inject
    public ChartManagerImpl(FeedProvider feedProvider, ChartCreatorImpl chartCreator) {
        this.chartCreator = chartCreator;
        feedProvider.addFeedProviderStateListener(this);
    }

    @Override
    public void initialize() {
        String enableChartProperty = System.getProperty("chart.enable");
        if (enableChartProperty != null && enableChartProperty.toLowerCase().equals("true"))
            enableChart = true;
        else
            enableChart = false;

        chartCreator.setEnabled(enableChart);
    }

    @Override
    public void notify(FeedProviderState feedProviderState) {
        if (feedProviderState.equals(FeedProviderState.FINISHED_SUBSCRIBED))
            handleChart();
    }

    private void handleChart() {
        if (enableChart) {
            LOGGER.info("Show chart");
            chartCreator.display();
        }
    }
}

