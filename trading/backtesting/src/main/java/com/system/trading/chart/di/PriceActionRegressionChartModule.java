package com.system.trading.chart.di;

import com.system.trading.chart.*;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.OrderManager;
import com.system.trading.core.strategy.par.PriceActionRegressionObjectId;
import com.system.trading.core.ta.slope.SlopeFactory;
import com.system.trading.core.ta.slope.SlopeMovingTick;
import dagger.Module;
import dagger.Provides;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
class PriceActionRegressionChartModule {

    @Singleton
    @Provides
    @Named("TimeSeriesTick")
    TimeSeries provideTimeSeriesTick() {
        TimeSeries series = new TimeSeries("Tick");
        series.setNotify(false);
        return series;
    }

    @Singleton
    @Provides
    @Named("TimeSeriesBuy")
    TimeSeries provideTimeSeriesBuy() {
        TimeSeries series = new TimeSeries("Buy");
        series.setNotify(false);
        return series;
    }

    @Singleton
    @Provides
    @Named("TimeSeriesSell")
    TimeSeries provideTimeSeriesSell() {
        TimeSeries series = new TimeSeries("Sell");
        series.setNotify(false);
        return series;
    }

    @Provides
    @Singleton
    @Named("PriceActionRegressionSlopeDataSet")
    TimeSeriesCollection provideSlopeDataSet() {
        TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection();
        timeSeriesCollection.setNotify(false);
        return timeSeriesCollection;
    }

    @Provides
    @Singleton
    @Named("PriceActionRegressionStrategyChart")
    TradingChart providePriceActionRegressionStrategyChart(OrderManager orderManager, FeedProvider feedProvider, SlopeFactory slopeFactory,
                                                           @Named("TimeSeriesTick") TimeSeries timeSeriesTick,
                                                           @Named("TimeSeriesBuy") TimeSeries timeSeriesBuy,
                                                           @Named("TimeSeriesSell") TimeSeries timeSeriesSell,
                                                           @Named("PriceActionRegressionSlopeDataSet") TimeSeriesCollection slopeDataSet) {
        feedProvider.addTickListener(new TimeSeriesTickListener(timeSeriesTick));
        orderManager.addOrderListener(new TimeSeriesBuyListener(timeSeriesBuy));
        orderManager.addOrderListener(new TimeSeriesSellListener(timeSeriesSell));

        SlopeMovingTick slope = (SlopeMovingTick) slopeFactory.retrieveSlope(PriceActionRegressionObjectId.SLOPE_MOVING);
        SlopeTickDataSetNotifier slopeDataSetNotifier = new SlopeTickDataSetNotifier(slopeDataSet);
        slope.addListener(slopeDataSetNotifier);

        return new PriceActionRegressionStrategyChart(timeSeriesTick, timeSeriesBuy, timeSeriesSell, slopeDataSet);
    }
}
