package com.system.trading.backtesting.order;

import com.system.trading.core.order.InstrumentConfiguration;

class InstrumentConfigurationMock implements InstrumentConfiguration {
    @Override
    public float getPipValue(String symbol) {
        return 0.25f;
    }

    @Override
    public float getProfitPipValue(String symbol) {
        return 12.5f;
    }

    @Override
    public float getTotalOneSideCommission(String symbol) {
        return 2.1f;
    }

    @Override
    public float getMarginRequirement(String symbol) {
        return 13800;
    }
}
