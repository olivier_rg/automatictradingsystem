package com.system.trading.backtesting.order;

import com.system.trading.core.Contract;
import com.system.trading.core.SecurityType;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.*;
import com.system.trading.core.pojo.Tick;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertEquals;

public class BackTestOrderManagerTest {
    @Mock
    FeedProvider feedProvider;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Testing commission
     */
    @Test
    public void whenProfitSellLongOneFutureContract_TakeCommission() {
        Account account = createAccount(30000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").lastTradeDateOrContractMonth("202009").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(2999f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertEquals(1, orderManager.getLimitPendingOrderIds().size());
        orderManager.notify(new Tick(3, 3000f, 1), false); // buy 1 position
        Assert.assertEquals(0, orderManager.getLimitPendingOrderIds().size());
        Assert.assertEquals(1, orderManager.getPositionIds().size());
        orderManager.notify(new Tick(3, 3000.25f, 1), false); // sell 1 position take profit
        Assert.assertEquals(0, orderManager.getPositionIds().size());
        Assert.assertEquals(30008.3, Math.round(account.getCapital() * 100.0) / 100.0, 0.001);
    }

    @Test
    public void whenProfitSellShortOneFutureContract_TakeCommission() {
        Account account = createAccount(30000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").lastTradeDateOrContractMonth("202009").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(3001f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderManager.notify(new Tick(3, 3000f, 1), false); // buy 1 position
        orderManager.notify(new Tick(3, 2999.75f, 1), false); // sell 1 position take profit
        Assert.assertEquals(30008.3, Math.round(account.getCapital() * 100.0) / 100.0, 0.001);
    }

    @Test
    public void whenLossSellLongOneFutureContract_TakeCommission() {
        Account account = createAccount(30000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").lastTradeDateOrContractMonth("202009").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(1f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderManager.notify(new Tick(3, 3000f, 1), false); // buy 1 position
        orderManager.notify(new Tick(3, 2999, 1), false); // sell 1 position take profit
        Assert.assertEquals(29945.8, Math.round(account.getCapital() * 100.0) / 100.0, 0.001);
    }

    @Test
    public void whenLossSellShortOneFutureContract_TakeCommission() {
        Account account = createAccount(30000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").lastTradeDateOrContractMonth("202009").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(1f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(3, 3000f, 1), false); // buy 1 position
        orderManager.notify(new Tick(3, 3001, 1), false); // sell 1 position take profit
        Assert.assertEquals(29945.8, Math.round(account.getCapital() * 100.0) / 100.0, 0.001);
    }

    /**
     * Testing Buy
     */
    @Test
    public void whenBuying_AccountCapitalIsUpdated() {
        Account account = createAccount(50000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(3001f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(3, 3000f, 1), false); // buy 1 position
        Assert.assertEquals(36197.9, account.getCapital(), 0.05);
    }

    /**
     * Testing
     */
    @Test
    public void whenBuyingTwoContractInARow_NotEnoughVolumeWithOneTick() {
        Account account = createAccount(50000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(3001f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            Assert.assertEquals(0, orderManager.getPositionIds().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(3, 3000f, 1), false); // buy 1 position
        Assert.assertEquals(1, orderManager.getPositionIds().size());
    }

    @Test
    public void whenBuyingTwoContractInARow_EnoughVolumeWithOneTick() {
        Account account = createAccount(50000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(2999.75f).stopLossPoint(3001f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            Assert.assertEquals(0, orderManager.getPositionIds().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(3, 3000f, 2), false); // buy 1 position
        Assert.assertEquals(2, orderManager.getPositionIds().size());
    }

    @Test
    public void whenSellingTwoContractInARow_OrdersArePartiallyFilled() {
        Account account = createAccount(50000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(3001f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            Assert.assertEquals(0, orderManager.getPositionIds().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(3, 3000f, 2), false);
        Assert.assertEquals(2, orderManager.getPositionIds().size());

        orderManager.notify(new Tick(4, 2999.75f, 1), false); // Sell one order
        Assert.assertEquals(1, orderManager.getPositionIds().size());
        Assert.assertEquals(1, orderManager.getCloseOrders().size());
    }

    @Test
    public void whenSellingTwoContractInARow_OrdersAreFullyFilled() {
        Account account = createAccount(50000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(0.25f).stopLossPoint(3001f).build();
        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.SELL, 1, orderPriceInformation);
            Assert.assertEquals(0, orderManager.getPositionIds().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(3, 3000f, 2), false); // buy 1 position
        Assert.assertEquals(2, orderManager.getPositionIds().size());

        orderManager.notify(new Tick(3, 2999.75f, 2), false);
        Assert.assertEquals(0, orderManager.getPositionIds().size());
        Assert.assertEquals(2, orderManager.getCloseOrders().size());
    }

    /**
     * Testing Sell
     */
    @Test
    public void whenBuyingAndSellingFuture_AccountCapitalIsUpdated() {
        Account account = createAccount(50000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3000f)
                .takeProfitPoint(3000.25f).stopLossPoint(2999f).build();

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(3, 3000f, 1), false); // buy 1 position
        Assert.assertEquals(36197.9, account.getCapital(), 0.05);
    }

    /**
     * Testing trailing stop
     */
    @Test
    public void whenOrderIsLongWithTrailingStop_AndTrailingStopIsTriggered() {
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, createAccount(20000), new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();

        assertEquals(0, orderManager.getLimitPendingOrderIds().size());

        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(25f)
                .trailingStopPoint(2f).build();

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderManager.notify(new Tick(1, 20, 1), false);
        assertEquals(1, orderManager.getLimitPendingOrderIds().size());
        orderManager.notify(new Tick(1, 25, 1), false); // buy
        assertEquals(0, orderManager.getLimitPendingOrderIds().size());
        assertEquals(1, orderManager.getPositionIds().size());
        assertEquals(0, orderManager.getCloseOrders().size());
        orderManager.notify(new Tick(1, 26, 1), false);
        assertEquals(0, orderManager.getLimitPendingOrderIds().size());
        assertEquals(1, orderManager.getPositionIds().size());
        assertEquals(0, orderManager.getCloseOrders().size());
        orderManager.notify(new Tick(1, 23, 1), false);
        assertEquals(0, orderManager.getLimitPendingOrderIds().size());
        assertEquals(0, orderManager.getPositionIds().size()); // failed
        assertEquals(1, orderManager.getCloseOrders().size());
        orderManager.notify(new Tick(1, 20, 1), false);
        assertEquals(0, orderManager.getLimitPendingOrderIds().size());
        assertEquals(0, orderManager.getPositionIds().size());
        assertEquals(1, orderManager.getCloseOrders().size());
    }


    /**
     * Testing of leverage
     */
    @Test
    public void whenBuyingOneFutureContract_MarginRequirementIsNotMeet_NotEnoughCapital() {
        Account account = createAccount(2000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();
        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3080f)
                .trailingStopPoint(2f).build();

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderManager.notify(new Tick(1, 3080, 1), false);
        assertEquals(0, orderManager.getLimitPendingOrderIds().size());
        assertEquals(0, orderManager.getPositionIds().size());
        assertEquals(0, orderManager.getCloseOrders().size());
    }

    @Test
    public void whenBuyingOneFutureContract_MarginRequirementIsMeet() {
        Account account = createAccount(20000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();
        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3080f)
                .trailingStopPoint(2f).build();

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(1, orderManager.getLimitPendingOrderIds().size());
        orderManager.notify(new Tick(1, 3080, 1), false);
        assertEquals(0, orderManager.getLimitPendingOrderIds().size());
        assertEquals(1, orderManager.getPositionIds().size());
        assertEquals(0, orderManager.getCloseOrders().size());
    }

    /**
     * Testing account maintenance
     */
    @Test
    public void whenBuyingOneFutureContract_AccountIsUpdated() {
        Account account = createAccount(20000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();
        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3080f)
                .trailingStopPoint(2f).build();

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderManager.notify(new Tick(1, 3080, 1), false); // buy
        assertEquals(6197.9, account.getCapital(), 0.05);
    }

    @Test
    public void whenSellingOneFutureContract_AndStopLossIsTrigger_AccountIsUpdated() {
        Account account = createAccount(20000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();
        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3080f)
                .trailingStopPoint(2f).build();

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderManager.notify(new Tick(1, 3080, 1), false); // buy
        assertEquals(6197.9, account.getCapital(), 0.05);
        orderManager.notify(new Tick(1, 3078, 1), false); // sell
        assertEquals(0, orderManager.getPositionIds().size());
        assertEquals(19895.8, account.getCapital(), 0.05);
    }

    /**
     * Testing of order listener
     */
    @Test
    public void whenBuyingOneContract_NotifyOrderIsRequested() {
        Account account = createAccount(20000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();
        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3080f).build();

        AtomicReference<OrderStatus> lastStatusOrder = new AtomicReference<>(null);
        orderManager.addOrderListener(order -> lastStatusOrder.set(order.getOrderStatus()));

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(OrderStatus.REQUESTED, lastStatusOrder.get());
    }

    @Test
    public void whenBuyingOneContract_NotifyOrderIsFilled() {
        Account account = createAccount(20000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();
        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(3080f).build();

        AtomicReference<OrderStatus> lastStatusOrder = new AtomicReference<>(null);
        orderManager.addOrderListener(order -> lastStatusOrder.set(order.getOrderStatus()));

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(1, 3080, 1), false); // buy
        assertEquals(OrderStatus.FILLED, lastStatusOrder.get());
    }

    @Test
    public void whenBuyingOneContract_NotifyOrderIsClosed() {
        Account account = createAccount(20000);
        BackTestOrderManager orderManager = new BackTestOrderManager(feedProvider, account, new InstrumentConfigurationMock());
        Contract contract = Contract.builder().symbol("ES").secType(SecurityType.FUTURE).currency("USD").exchange("GLOBEX").build();
        OrderPriceInformation orderPriceInformation =
                OrderPriceInformation.builder().requestPrice(3080f).takeProfitPoint(1f).build();

        AtomicReference<OrderStatus> lastStatusOrder = new AtomicReference<>(null);
        orderManager.addOrderListener(order -> lastStatusOrder.set(order.getOrderStatus()));

        try {
            orderManager.buy(contract, OrderType.LIMIT, OrderActionType.BUY, 1, orderPriceInformation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderManager.notify(new Tick(1, 3080, 1), false); // buy
        orderManager.notify(new Tick(1, 3081, 1), false); // sell

        assertEquals(OrderStatus.CLOSED, lastStatusOrder.get());
    }

    private Account createAccount(float initialCapital) {
        return new Account(AccountConfiguration.builder().initialCapital(initialCapital).build());
    }
}