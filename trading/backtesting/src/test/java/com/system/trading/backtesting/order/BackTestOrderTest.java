package com.system.trading.backtesting.order;

import com.system.trading.core.order.OrderActionType;
import com.system.trading.core.order.OrderStatus;
import org.junit.Test;

import static org.junit.Assert.*;

public class BackTestOrderTest {

    public static final double DELTA = 0.005;

    /**
     * Testing of order initialization
     */
    @Test
    public void whenOrderCreated_OrderIsInitialize() {
        BackTestOrder order = new BackTestOrder();
        assertEquals(0, order.getQuantity());
        assertNull(order.getClosedPrice());
        assertNull(order.getFilledPrice());
        assertNull(order.getRequestPrice());
        assertNull(order.getPriceStopLoss());
        assertNull(order.getPriceTakeProfit());
        assertNull(order.getTrailingStopPoint());
        assertNull(order.getOrderType());
        assertNull(order.getPriceCondition());
        assertNull(order.getOrderActionType());
        assertNull(order.getContract());
    }

    @Test
    public void whenOrderCreated_IdIsIncremented() {
        BackTestOrder order1 = new BackTestOrder();
        BackTestOrder order2 = new BackTestOrder();
        assertTrue(order2.orderId() > order1.orderId());
        assertEquals(1, order2.orderId() - order1.orderId());
    }

    @Test
    public void whenOrderCreated_AndTrailingStopIsSet_TrailingStopExist() {
        BackTestOrder order = new BackTestOrder();
        order.setTrailingStopPoint(2f);
        assertEquals(2f, order.getTrailingStopPoint(), 0.005);
    }

    /**
     * Testing of updating trailing stop
     */
    @Test
    public void whenOrderIsLong_AndTrailingStopCreated_StopLossIsUpdated() {
        BackTestOrder order = new BackTestOrder();
        order.setOrderActionType(OrderActionType.BUY);
        order.setTrailingStopPoint(2f);

        assertNull(order.getPriceStopLoss());
        order.updateTrailingStop(4);
        assertEquals(2, order.getPriceStopLoss(), DELTA);
        order.updateTrailingStop(4.1f);
        assertEquals(2.1, order.getPriceStopLoss(), DELTA);
        order.updateTrailingStop(3.1f);
        assertEquals(2.1, order.getPriceStopLoss(), DELTA);
        order.updateTrailingStop(5.1f);
        assertEquals(3.1, order.getPriceStopLoss(), DELTA);
    }

    @Test
    public void whenOrderIsShort_AndTrailingStopCreated_StopLossIsUpdated() {
        BackTestOrder order = new BackTestOrder();
        order.setOrderActionType(OrderActionType.SELL);
        order.setTrailingStopPoint(2f);

        assertNull(order.getPriceStopLoss());
        order.updateTrailingStop(4);
        assertEquals(6, order.getPriceStopLoss(), DELTA);
        order.updateTrailingStop(4.1f);
        assertEquals(6, order.getPriceStopLoss(), DELTA);
        order.updateTrailingStop(3.1f);
        assertEquals(5.1, order.getPriceStopLoss(), DELTA);
        order.updateTrailingStop(3.3f);
        assertEquals(5.1, order.getPriceStopLoss(), DELTA);
        order.updateTrailingStop(1.3f);
        assertEquals(3.3, order.getPriceStopLoss(), DELTA);
    }

    /**
     * Testing of order status
     */
    @Test
    public void whenOrderPriceIsRequested_StatusIsRequested() {
        BackTestOrder order = new BackTestOrder();
        order.setRequestPrice(10f);
        assertEquals(OrderStatus.REQUESTED, order.getOrderStatus());
    }

    @Test
    public void whenOrderPriceIsRequestedAndFilled_StatusIsFilled() {
        BackTestOrder order = new BackTestOrder();
        order.setRequestPrice(10f);
        order.setFilledPrice(10.1f);
        assertEquals(OrderStatus.FILLED, order.getOrderStatus());
    }

    @Test
    public void whenOrderPriceIsRequestedAndFilledAndClosed_StatusIsClose() {
        BackTestOrder order = new BackTestOrder();
        order.setRequestPrice(10f);
        order.setFilledPrice(10.1f);
        order.setClosedPrice(11f);
        assertEquals(OrderStatus.CLOSED, order.getOrderStatus());
    }

    @Test
    public void whenOrderPriceIsFilledAndClose_StatusIsNull() {
        BackTestOrder order = new BackTestOrder();
        order.setFilledPrice(10f);
        order.setClosedPrice(11f);
        assertNull(order.getOrderStatus());
    }

    @Test
    public void whenOrderPriceIsRequestedAndClose_StatusIsNull() {
        BackTestOrder order = new BackTestOrder();
        order.setRequestPrice(10f);
        order.setClosedPrice(11f);
        assertNull(order.getOrderStatus());
    }

}