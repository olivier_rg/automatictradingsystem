package com.system.trading.screener;

import com.system.trading.core.di.CommonPropertiesModule;
import com.system.trading.core.di.DatabaseModule;
import dagger.Component;

import javax.inject.Singleton;


@Singleton
@Component(modules = {DatabaseModule.class, CommonPropertiesModule.class, ApplicationModule.class})
public interface ApplicationComponent {
    EntryPoint entryPoint();
}
