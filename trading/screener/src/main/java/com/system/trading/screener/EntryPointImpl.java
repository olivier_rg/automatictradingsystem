package com.system.trading.screener;

import com.system.trading.core.orm.CandleDAO;
import com.system.trading.core.orm.InstrumentDAO;
import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.pojo.Instrument;
import com.system.trading.core.pojo.InstrumentType;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Singleton
public class EntryPointImpl implements EntryPoint {
    private final InstrumentDAO instrumentDAO;
    private final CandleDAO candleDAO;

    private int CONSOLIDATION_PERIOD_SIZE = 15;

    @Inject
    public EntryPointImpl(InstrumentDAO instrumentDAO, CandleDAO candleDAO) {
        this.instrumentDAO = instrumentDAO;
        this.candleDAO = candleDAO;
    }

    @Override
    public void start() {
        var instruments = instrumentDAO.getInstrumentsByType(InstrumentType.STOCK);

        for (Instrument instrument : instruments) {
            var candles = candleDAO.getCandlesByInstrument(instrument);

            if (candles.size() >= CONSOLIDATION_PERIOD_SIZE) {
                var currentCandles = candles.subList(candles.size() - CONSOLIDATION_PERIOD_SIZE, candles.size());
                if (isConsolidating(currentCandles)) {
                    System.out.println(instrument.getSymbol() + " is consolidating at " +
                            new Date(currentCandles.get(currentCandles.size() - 1).getTime()));
                }
            }

/*
     // Run through all candle from 2017
            for (CandleImpl candle : candles) {
                if (indexCandle >= CONSOLIDATION_PERIOD_SIZE) {

                    var currentCandles = candles.subList(indexCandle - CONSOLIDATION_PERIOD_SIZE, indexCandle);

                    if (isConsolidating(currentCandles)) {
                        System.out.println(instrument.getSymbol() + " is consolidating at " +
                                new Date(currentCandles.get(currentCandles.size() - 1).getTime()));
                    }
                }
                indexCandle++;
            }*/
        }
    }

    private boolean isConsolidating(List<CandleImpl> currentCandles) {
        var maxClose = currentCandles.stream().max(Comparator.comparing(CandleImpl::getClose)).get().getClose();
        var minClose = currentCandles.stream().min(Comparator.comparing(CandleImpl::getClose)).get().getClose();

        return minClose > (maxClose * 0.98);
    }
}
