package com.system.trading.screener;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class ApplicationModule {

    @Singleton
    @Provides
    EntryPoint provideEntryPoint(EntryPointImpl entryPoint){
        return entryPoint;
    }
}
