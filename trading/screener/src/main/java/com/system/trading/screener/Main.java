package com.system.trading.screener;

public class Main {
    public static void main(String[] args) {
        ApplicationComponent application = DaggerApplicationComponent.builder().build();
        application.entryPoint().start();
    }
}

