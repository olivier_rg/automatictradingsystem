package com.system.trading.ib.feed;

import com.ib.client.Contract;
import com.ib.client.EClientSocket;
import com.system.trading.core.util.DateUtil;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import com.system.trading.ib.IBController;
import com.system.trading.ib.order.HistoricalTickLastListener;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class IBTickLoaderImpl implements HistoricalTickLastListener, IBTickLoader {
    private static final Logger LOGGER = Logger.getLogger(IBTickLoaderImpl.class.getName());
    private final List<Contract> contracts;
    private final List<TickListener> tickListeners;
    private final AtomicBoolean historicalTickEmptyRequest = new AtomicBoolean(false);
    private final AtomicInteger lastReqId;
    private final AtomicBoolean isLoading;
    private final SimpleDateFormat simpleDateFormat;
    private final EClientSocket ibClient;
    private Long lastTime;
    private int currentReqId = 0;

    public IBTickLoaderImpl(IBController ibController, List<Contract> contracts, List<TickListener> tickListeners,
                            SimpleDateFormat simpleDateFormat) {
        this.contracts = contracts;
        this.tickListeners = tickListeners;
        this.simpleDateFormat = simpleDateFormat;

        ibClient = ibController.getClient();
        lastReqId = new AtomicInteger(0);
        isLoading = new AtomicBoolean(false);
        lastTime = 0L;

        ibController.addHistoricalTickLastListener(this);
        ibController.addNextValidIdListener(nextValidId -> currentReqId = nextValidId);
    }

    @Override
    public void notify(int reqId, long tickTime, double price, long volume, boolean done) {
        if (done) {
            lastReqId.set(reqId);
            // tick time represent number of second. Multiply by 1000 to convert time to millisecond
            lastTime = tickTime * 1000;
        }

        tickListeners.forEach(tickListener -> tickListener.notify(new Tick(tickTime, (float) price, (int) volume), true));
    }

    @Override
    public void notifyEmptyRequest(int reqId) {
        historicalTickEmptyRequest.set(true);
        lastReqId.set(reqId);
        lastTime = DateUtil.addSecond(new Date(lastTime)).getTime();
    }

    @Override
    public void load(Date startLoadingDate) {
        isLoading.set(true);
        for (Contract contract : contracts) {
            long endDateLong = new Date().getTime(); // convertDateStringToLong(endDate);
            lastTime = startLoadingDate.getTime();

            String timeFormatted = simpleDateFormat.format(startLoadingDate);
            while (lastTime <= endDateLong) {
                LOGGER.info("Loading historical ticks - starting date:" + timeFormatted);
                ibClient.reqHistoricalTicks(currentReqId, contract, timeFormatted, null, 999, "TRADES", 0, true, null);

                // todo notify when all tick are downloaded
                waitForRequestToFinish();

                Date time = DateUtil.addSecond(new Date(lastTime));
                timeFormatted = simpleDateFormat.format(time);
                currentReqId++;
            }
        }
        isLoading.set(false);
    }

    @Override
    public boolean isLoading() {
        return isLoading.get();
    }

    private void waitForRequestToFinish() {
        boolean waiting = true;

        while (waiting) {
            if (lastReqId.get() == currentReqId || historicalTickEmptyRequest.get()) {
                waiting = false;
                historicalTickEmptyRequest.set(false);
            }
        }
    }
}
