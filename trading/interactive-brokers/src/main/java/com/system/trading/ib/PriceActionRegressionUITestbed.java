package com.system.trading.ib;

import javax.swing.*;
import java.awt.*;

public class PriceActionRegressionUITestbed extends JFrame {

    public PriceActionRegressionUITestbed() {
        initializeUI();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        PriceActionRegressionUITestbed ui = new PriceActionRegressionUITestbed();
        ui.pack();
        ui.setVisible(true);
    }

    private void initializeUI() {
        this.setSize(900, 600);
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2, 2));

        // Status Connection
        JLabel label = new JLabel("TWS Status Connection:");
        mainPanel.add(label);
        JLabel labelTwsStatusConnection = new JLabel("Connected");
        mainPanel.add(labelTwsStatusConnection);


        // Latest Tick Received
        JLabel label1 = new JLabel("Latest Tick Received:");
        mainPanel.add(label1);
        JLabel labelLatestTickReceived = new JLabel("Time 39349394934 prie: 33443.34");
        mainPanel.add(labelLatestTickReceived);

        this.add(mainPanel);
    }
}
