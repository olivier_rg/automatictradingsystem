package com.system.trading.ib.error;

import com.system.trading.core.feed.FeedProvider;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TwsErrorCode1102Handler extends AbstractTwsErrorCode {
    private static final Logger LOGGER = Logger.getLogger(TwsErrorCode1102Handler.class.getName());
    private final FeedProvider feedProvider;

    @Inject
    public TwsErrorCode1102Handler(FeedProvider feedProvider) {
        super(TwsErrorCode._1102);
        this.feedProvider = feedProvider;
    }

    @Override
    public void handle() {
        //feedProvider.subscribe();
    }

}

