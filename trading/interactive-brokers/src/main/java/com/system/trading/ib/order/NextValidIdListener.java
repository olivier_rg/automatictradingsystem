package com.system.trading.ib.order;

public interface NextValidIdListener {
    void notify(int nextValidId);
}
