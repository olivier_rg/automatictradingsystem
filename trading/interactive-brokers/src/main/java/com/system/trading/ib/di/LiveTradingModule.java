package com.system.trading.ib.di;

import com.ib.client.Contract;
import com.system.trading.core.order.OrderManager;
import com.system.trading.ib.IBController;
import com.system.trading.ib.IBObjectName;
import com.system.trading.ib.TWSRequestId;
import com.system.trading.ib.order.IBOrderManager;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Module
public class LiveTradingModule {
    @Singleton
    @Provides
    IBController provideIbController(@Named(IBObjectName.IB_PROPERTIES) Properties properties) {
        return new IBController(properties);
    }

    @Singleton
    @Provides
    OrderManager provideOrderManager(IBController ibController) {
        IBOrderManager orderManager = new IBOrderManager(ibController);

        ibController.addOrderStatusListener(orderManager);
        ibController.addNextValidIdListener(orderManager);
        ibController.addContractDetailsListener(orderManager);
        ibController.addPositionListener(orderManager);

        ibController.getClient().reqIds(-1);
        ibController.getClient().reqPositionsMulti(TWSRequestId.POSITION_SUBSCRIPTION.getId(), "", "");

        return orderManager;
    }

    @Singleton
    @Provides
    List<Contract> provideTradedIBContracts() {
        return new ArrayList<>();
    }
}
