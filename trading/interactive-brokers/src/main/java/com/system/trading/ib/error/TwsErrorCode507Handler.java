package com.system.trading.ib.error;

import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TwsErrorCode507Handler extends AbstractTwsErrorCode {
    private static final Logger LOGGER = Logger.getLogger(TwsErrorCode507Handler.class.getName());

    @Inject
    public TwsErrorCode507Handler() {
        super(TwsErrorCode._507);
    }

    @Override
    public void handle() {
    }

}
