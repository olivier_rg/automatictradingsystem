package com.system.trading.ib.order;

public interface HistoricalTickLastListener {
    void notify(int reqId, long tickTime, double price, long volume, boolean done);

    void notifyEmptyRequest(int reqId);
}
