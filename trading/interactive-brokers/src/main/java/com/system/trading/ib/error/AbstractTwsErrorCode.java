package com.system.trading.ib.error;

public abstract class AbstractTwsErrorCode implements TwsErrorCodeHandler {
    private TwsErrorCode fixingErrorCode;

    public AbstractTwsErrorCode(TwsErrorCode fixingErrorCode) {
        this.fixingErrorCode = fixingErrorCode;
    }

    @Override
    public final boolean isFixing(int id) {
        return fixingErrorCode.getErrorCode() == id;
    }
}
