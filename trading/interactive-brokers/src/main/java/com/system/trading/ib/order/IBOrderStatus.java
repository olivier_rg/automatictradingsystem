package com.system.trading.ib.order;

import lombok.Getter;

@Getter
public class IBOrderStatus {
    private final double filled;


    private final int orderId;
    private final String status;
    private final double remaining;
    private final double avgFillPrice;
    private final int permId;
    private final int parentId;
    private final double lastFillPrice;
    private final int clientId;
    private final String whyHeld;
    private final double mktCapPrice;

    public IBOrderStatus(int orderId, String status, double filled,
                         double remaining, double avgFillPrice, int permId, int parentId,
                         double lastFillPrice, int clientId, String whyHeld, double mktCapPrice) {

        this.orderId = orderId;
        this.status = status;
        this.filled = filled;
        this.remaining = remaining;
        this.avgFillPrice = avgFillPrice;
        this.permId = permId;
        this.parentId = parentId;
        this.lastFillPrice = lastFillPrice;
        this.clientId = clientId;
        this.whyHeld = whyHeld;
        this.mktCapPrice = mktCapPrice;
    }
}
