package com.system.trading.ib.feed;

import java.util.Date;

public interface IBTickLoader {
    void load(Date startLoadingDate);

    boolean isLoading();
}
