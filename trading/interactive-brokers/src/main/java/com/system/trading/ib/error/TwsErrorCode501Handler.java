package com.system.trading.ib.error;

import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TwsErrorCode501Handler extends AbstractTwsErrorCode {
    private static final Logger LOGGER = Logger.getLogger(TwsErrorCode501Handler.class.getName());

    @Inject
    public TwsErrorCode501Handler() {
        super(TwsErrorCode._501);
    }

    @Override
    public void handle() {
    }
}
