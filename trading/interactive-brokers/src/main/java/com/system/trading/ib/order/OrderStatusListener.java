package com.system.trading.ib.order;

public interface OrderStatusListener {
    void notify(IBOrderStatus IBOrderStatus);
}
