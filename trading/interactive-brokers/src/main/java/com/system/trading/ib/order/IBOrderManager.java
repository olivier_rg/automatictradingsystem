package com.system.trading.ib.order;

import com.ib.client.PriceCondition;
import com.ib.client.*;
import com.system.trading.core.SecurityType;
import com.system.trading.core.order.Order;
import com.system.trading.core.order.OrderType;
import com.system.trading.core.order.*;
import com.system.trading.core.util.CollectorUtil;
import com.system.trading.ib.IBController;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

// todo: implement order synchronisation with IB

public class IBOrderManager implements OrderManager, NextValidIdListener, OrderStatusListener,
        ContractDetailsListener, PositionListener {
    private static final Logger LOGGER = Logger.getLogger(IBOrderManager.class.getName());
    private final EClientSocket ibClient;
    // pendingOrders is a list of order waiting to be filled. Once the order is filled, it is remove from the list because it is now an position.
    private final List<IBOrder> parentPendingOrders;
    private int numberActivePositions;
    private final Map<com.system.trading.core.Contract, ContractDetails> contractDetailsMap;
    private int nextOrderId = 0;

    public IBOrderManager(IBController ibController) {
        LOGGER.warn("Only support limit order.");
        ibClient = ibController.getClient();

        parentPendingOrders = new ArrayList<>();
        contractDetailsMap = new HashMap<>();
        numberActivePositions = 0;
    }

    @Override
    public void notify(int reqId, ContractDetails contractDetails) {

        Contract ibContract = contractDetails.contract();

        SecurityType securityType = null;
        if (ibContract.secType().name().equals("FUT"))
            securityType = SecurityType.FUTURE;
        else
            LOGGER.error("Requested Interactive Broker security type from contract details is not supported.");

        com.system.trading.core.Contract contract = com.system.trading.core.Contract.builder()
                .secType(securityType)
                .exchange(ibContract.exchange())
                .currency(ibContract.currency())
                .symbol(ibContract.symbol())
                .lastTradeDateOrContractMonth(ibContract.lastTradeDateOrContractMonth())
                .build();

        if (!contractDetailsMap.containsValue(contractDetails)) {
            contractDetailsMap.put(contract, contractDetails);
        }
    }

    @Override
    public void notify(IBOrderStatus status) {
        LOGGER.info("IB notify order status");

        if (status.getStatus().toLowerCase().equals("cancelled") || status.getStatus().toLowerCase().equals("pendingcancel")) {
            removePendingOrderById(status.getOrderId());
        } else if (status.getStatus().toLowerCase().equals("filled")) {
            // No more remaining contract to buy in the order. Remove the order from the active orders list.
            if (status.getRemaining() == 0) {
                removePendingOrderById(status.getOrderId());
            }

            // todo: set Time Order Filled with the status.getOrderId()
        }

        LOGGER.info("Number of pending orders: " + parentPendingOrders.size());
    }

    @Override
    public void cancelPendingOrders() {
        parentPendingOrders.forEach(ibOrder -> ibClient.cancelOrder(ibOrder.orderId()));
        parentPendingOrders.clear();
    }

    @Override
    public void notify(int nextValidId) {
        nextValidId++;
        this.nextOrderId = nextValidId;
    }

    @Override
    public int buy(com.system.trading.core.Contract contract, OrderType orderType, OrderActionType orderActionType, int quantityContract, OrderPriceInformation orderPriceInformation) throws Exception {
        if (orderPriceInformation.getRequestPrice() == null)
            throw new Exception("IBOrderManager - When purchasing, the price cannot be null");

        if (quantityContract < 0)
            throw new Exception("Cannot order with a quantity less of 0");

        if(quantityContract > 1)
            throw new Exception("Cannot order with more quantity than 1");

        if (orderPriceInformation.getRequestPrice() == 0) {
            LOGGER.warn("Buying contract with requested price of 0 $.");
        }

        Contract ibContract = getIbContract(contract);

        IBOrder order = new IBOrder();
        order.orderId(getNextOrderId());
        order.action(orderActionType.name());
        order.orderType("LMT");
        order.totalQuantity(quantityContract);
        order.lmtPrice(orderPriceInformation.getRequestPrice());
        order.outsideRth(true);
        order.transmit(false);
        parentPendingOrders.add(order);

        // Price Condition
        if (orderPriceInformation.getPriceCondition() != null) {
            float price = orderPriceInformation.getPriceCondition().getPrice();
            PriceConditionRequirement priceConditionRequirement = orderPriceInformation.getPriceCondition().getPriceConditionRequirement();

            PriceCondition priceCondition = (PriceCondition) OrderCondition.create(OrderConditionType.Price);
            priceCondition.conId(ibContract.conid());
            priceCondition.exchange(ibContract.exchange());
            priceCondition.isMore(priceConditionRequirement.equals(PriceConditionRequirement.SUPERIOR_OR_EQUAL));
            //priceCondition.isMore(orderActionType.name().equals("BUY") ? true : false);
            priceCondition.price(price);

            order.conditionsCancelOrder(true);
            order.conditions().add(priceCondition);
        }

        String reverseAction = orderActionType.name().equals("BUY") ? "SELL" : "BUY";

        IBOrder takeProfitOrder = null;
        if (orderPriceInformation.getTakeProfitPoint() != null) {

            float takeProfitPrice;
            if (orderActionType.name().equals("BUY"))
                takeProfitPrice = order.getRequestPrice() + orderPriceInformation.getTakeProfitPoint();
            else
                takeProfitPrice = order.getRequestPrice() - orderPriceInformation.getTakeProfitPoint();

            takeProfitOrder = new IBOrder();
            takeProfitOrder.tif("GTC");
            takeProfitOrder.orderId(getNextOrderId());
            takeProfitOrder.action(reverseAction);
            takeProfitOrder.orderType("LMT");
            takeProfitOrder.totalQuantity(quantityContract);
            takeProfitOrder.lmtPrice(takeProfitPrice);
            takeProfitOrder.parentId(order.orderId());
            takeProfitOrder.outsideRth(true);
            takeProfitOrder.transmit(false);
        }

        IBOrder stopLossOrder = null;
        if (orderPriceInformation.getStopLossPoint() != null) {
            float stopLossPrice;
            if (orderActionType.name().equals("BUY"))
                stopLossPrice = order.getRequestPrice() - orderPriceInformation.getStopLossPoint();
            else
                stopLossPrice = order.getRequestPrice() + orderPriceInformation.getStopLossPoint();

            stopLossOrder = new IBOrder();
            stopLossOrder.orderId(getNextOrderId());
            stopLossOrder.tif("GTC");
            stopLossOrder.parentId(order.orderId());
            stopLossOrder.action(reverseAction);
            stopLossOrder.orderType("STP");
            stopLossOrder.lmtPrice(stopLossPrice);
            stopLossOrder.auxPrice(stopLossPrice);
            stopLossOrder.totalQuantity(quantityContract);
            stopLossOrder.outsideRth(true);
            stopLossOrder.transmit(false);
        }

        IBOrder trailingStopOrder = null;
        if (orderPriceInformation.getTrailingStopPoint() != null) {
            trailingStopOrder = new IBOrder();
            trailingStopOrder.orderId(getNextOrderId());
            trailingStopOrder.tif("GTC");
            trailingStopOrder.parentId(order.orderId());
            trailingStopOrder.action(reverseAction);
            trailingStopOrder.orderType("TRAIL");
            trailingStopOrder.totalQuantity(quantityContract);
            trailingStopOrder.auxPrice(orderPriceInformation.getTrailingStopPoint());
            trailingStopOrder.transmit(false);
            trailingStopOrder.outsideRth(true);
        }

        // The last created order must be transmit
        if (trailingStopOrder != null)
            trailingStopOrder.transmit(true);
        else if (stopLossOrder != null)
            stopLossOrder.transmit(true);
        else if (takeProfitOrder != null)
            takeProfitOrder.transmit(true);
        else
            order.transmit(true);

        this.ibClient.placeOrder(order.orderId(), ibContract, order);
        if (takeProfitOrder != null)
            this.ibClient.placeOrder(takeProfitOrder.orderId(), ibContract, takeProfitOrder);
        if (stopLossOrder != null)
            this.ibClient.placeOrder(stopLossOrder.orderId(), ibContract, stopLossOrder);
        if (trailingStopOrder != null)
            this.ibClient.placeOrder(trailingStopOrder.orderId(), ibContract, trailingStopOrder);

        return order.orderId();
    }

    @Override
    public void sell(int i) {
        LOGGER.error("Sell order by id is not implemented");
    }

    private int getNextOrderId() {
        int retId = nextOrderId;
        nextOrderId++;
        return retId;
    }

    @Override
    public boolean hasPosition(com.system.trading.core.Contract contract) {
        return numberActivePositions > 0;
    }

    @Override
    public boolean hasPendingOrders() {
        return parentPendingOrders.size() > 0;
    }

    @Override
    public List<Integer> getLimitPendingOrderIds() {
        List<Integer> ids = new ArrayList<>();
        parentPendingOrders.forEach(ibOrder -> {
            if (ibOrder.getOrderType().equals("LMT"))
                ids.add(ibOrder.orderId());
        });

        return ids;
    }

    @Override
    public void cancelPendingOrderById(int id) {
        ibClient.cancelOrder(id);
        removePendingOrderById(id);
    }

    @Override
    public Order getLimitPendingOrderById(int orderId) {
        return parentPendingOrders.stream()
                .filter(ibOrder -> ibOrder.orderId() == orderId && (ibOrder.getOrderType().equals("LMT")) /* && ibOrder.getOrderType().equals("TRAIL"))*/)
                .collect(CollectorUtil.toSingleton());
    }

    @Override
    public void addOrderListener(OrderListener orderListener) {
        // todo to implement
        LOGGER.error("Method addOrderListener is not implemented.");
    }

    @Override
    public void notify(int reqId, String account, String modelCode, Contract contract, double pos, double avgCost) {
        if (pos == 0) {
            // Can be notify even if there are no position left.
            if (numberActivePositions > 0) {
                numberActivePositions--;
            }
        } else {
            numberActivePositions++;
        }

        LOGGER.info("Number of active position: " + numberActivePositions);
    }

    private Contract getIbContract(com.system.trading.core.Contract contract) {
        AtomicReference<ContractDetails> contractDetails = new AtomicReference<>(null);

        contractDetailsMap.forEach((contract1, contractDetails1) -> {
            if (contract.compareTo(contract1) == 0) {
                contractDetails.set(contractDetails1);
            }
        });

        Contract ibContract = null;

        if (contractDetails.get() != null) {
            ibContract = contractDetails.get().contract();
        } else
            LOGGER.error("Cannot get contract details from contract container.");

        return ibContract;
    }

    private void removePendingOrderById(int orderId) {
        int indexToRemove = -1;
        for (int i = 0; i < parentPendingOrders.size(); i++) {
            if (parentPendingOrders.get(i).orderId() == orderId) {
                indexToRemove = i;
            }
        }

        if (indexToRemove != -1) {
            parentPendingOrders.remove(indexToRemove);
        }
    }
}
