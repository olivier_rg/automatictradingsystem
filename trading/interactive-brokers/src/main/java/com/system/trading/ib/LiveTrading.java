package com.system.trading.ib;

import com.system.trading.core.Contract;
import com.system.trading.core.Initializer;
import com.system.trading.core.SecurityType;
import com.system.trading.core.strategy.par.PriceActionRegressionStrategy;
import com.system.trading.core.util.LoggingUtil;
import com.system.trading.core.util.PropertiesUtil;
import com.system.trading.ib.di.DaggerLiveTradingComponent;
import com.system.trading.ib.di.LiveTradingComponent;
import org.apache.log4j.Logger;


public class LiveTrading {
    private static final Logger LOGGER = Logger.getLogger(LiveTrading.class.getName());
    private static final String TIME_FORMAT = "yyyyMMdd HH:mm:ss";

    /**
     * -Dlogging.level=off|error|warn|info  default is off
     * -Dstrategy.loading.start.time=
     * <p>
     * -Dlogging.level=info
     *
     * @param args main argument
     */
    public static void main(String[] args) {
        LoggingUtil.initializeLoggingLevel();

        LiveTradingComponent liveTrading = DaggerLiveTradingComponent.builder()
                .setInteractiveBrokerProperties(PropertiesUtil.retrievePropertiesFromBaseFolder("ib"))
                .build();
        liveTrading.initializers().forEach(Initializer::initialize);

        Contract contract = Contract.builder().
                symbol("ES").currency("USD").exchange("GLOBEX").secType(SecurityType.FUTURE).lastTradeDateOrContractMonth("202012").build();

        liveTrading.strategyManager().add(PriceActionRegressionStrategy.class, contract);
        loadingStrategies(liveTrading);
        liveTrading.feedProvider().subscribe();
    }

    private static void loadingStrategies(LiveTradingComponent liveTrading) {
        // todo: to test with vm argument ; does not load
        //Properties properties = PropertiesUtil.retrievePropertiesFromBaseFolder("settings");
        //String loadingStartTime = System.getProperty("strategy.loading.start.time");
        //if (loadingStartTime != null && !loadingStartTime.isEmpty()) {
            //Date loadingDate = DateUtil.getDateFromString(loadingStartTime, TIME_FORMAT);
            //if (loadingDate != null) {
                //Date loadingDate = DateUtil.createDate(2020,11,9,6,45);
                //Date startLoadingTime = new Date();
                //liveTrading.feedProvider().loadWithTick(loadingDate);
                //Date endLoadingTime = new Date();
                //LOGGER.info("Loading took:" + (startLoadingTime.getTime() - endLoadingTime.getTime()) / 1000 + " " +
                //"seconds");
            //}
        //}
    }
}
