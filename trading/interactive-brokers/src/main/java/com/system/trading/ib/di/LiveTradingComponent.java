package com.system.trading.ib.di;

import com.system.trading.core.Initializer;
import com.system.trading.core.di.CommonPropertiesModule;
import com.system.trading.core.di.CoreTradingComponent;
import com.system.trading.core.di.CoreTradingModule;
import com.system.trading.ib.IBObjectName;
import dagger.BindsInstance;
import dagger.Component;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Properties;
import java.util.Set;

@Singleton
@Component(modules = {CoreTradingModule.class, ConfigurationModule.class, LiveTradingModule.class, LiveFeedModule.class,
        ErrorModule.class, CommonPropertiesModule.class})
public interface LiveTradingComponent extends CoreTradingComponent {

    Set<Initializer> initializers();

    @Component.Builder
    interface Builder {
        LiveTradingComponent build();

        @BindsInstance
        Builder setInteractiveBrokerProperties(@Named(IBObjectName.IB_PROPERTIES) Properties properties);
    }
}
