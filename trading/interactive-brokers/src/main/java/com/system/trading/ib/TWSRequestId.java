package com.system.trading.ib;

public enum TWSRequestId {
    LOADING_DATA(4001),
    TICK_DATA(3001),
    POSITION_SUBSCRIPTION(9004);

    private final int requestId;

    TWSRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getId() {
        return this.requestId;
    }
}
