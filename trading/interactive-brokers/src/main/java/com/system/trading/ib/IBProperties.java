package com.system.trading.ib;

public class IBProperties {
    public static final String INTERACTIVE_BROKER_IP_ADDRESS = "interactive.broker.ip.address";
    public static final String INTERACTIVE_BROKER_PORT = "interactive.broker.port";
    public static final String INTERACTIVE_BROKER_CLIENT_ID = "interactive.broker.client.id";
}
