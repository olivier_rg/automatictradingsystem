package com.system.trading.ib.error;

import com.system.trading.core.memento.Caretaker;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TwsErrorCode502Handler extends AbstractTwsErrorCode {
    private static final Logger LOGGER = Logger.getLogger(TwsErrorCode502Handler.class.getName());
    private final Caretaker caretaker;

    @Inject
    public TwsErrorCode502Handler(Caretaker caretaker) {
        super(TwsErrorCode._502);
        this.caretaker = caretaker;
    }

    @Override
    public void handle() {
        caretaker.save();
    }
}
