package com.system.trading.ib.order;

import com.ib.client.ContractDetails;

public interface ContractDetailsListener {
    void notify(int reqId, ContractDetails contractDetails);
}
