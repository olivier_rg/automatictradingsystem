package com.system.trading.ib.error;

public interface TwsErrorCodeHandler {
    void handle();

    boolean isFixing(int errorCode);
}
