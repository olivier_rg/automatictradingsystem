package com.system.trading.ib.di;

import com.ib.client.Contract;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.tick.TickListener;
import com.system.trading.ib.IBController;
import com.system.trading.ib.IBObjectName;
import com.system.trading.ib.feed.IBFeedProvider;
import com.system.trading.ib.feed.IBTickLoader;
import com.system.trading.ib.feed.IBTickLoaderImpl;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Module
public class LiveFeedModule {
    @Singleton
    @Provides
    FeedProvider provideFeedProvider(IBController ibController, IBTickLoader ibTickLoader, List<Contract> contracts,
                                     @Named(IBObjectName.FEED_PROVIDER_TICK_LISTENERS) List<TickListener> tickListeners,
                                     @Named(IBObjectName.SIMPLE_DATE_FORMAT_MMDD_yyyyMMdd_HH_mm_ss) SimpleDateFormat simpleDateFormat) {
        IBFeedProvider feedProvider = new IBFeedProvider(ibController, ibTickLoader, contracts, tickListeners, simpleDateFormat);
        ibController.addTickListerner(feedProvider);
        ibController.addCandleListeners(feedProvider);
        ibController.setHistoricalEndListener(feedProvider);

        return feedProvider;
    }

    @Singleton
    @Provides
    @Named(IBObjectName.FEED_PROVIDER_TICK_LISTENERS)
    List<TickListener> provideFeedProviderTickListeners() {
        return new ArrayList<>();
    }

    @Singleton
    @Provides
    IBTickLoader provideIBTickLoader(IBController ibController, List<Contract> contracts,
                                     @Named(IBObjectName.FEED_PROVIDER_TICK_LISTENERS) List<TickListener> tickListeners,
                                     @Named(IBObjectName.SIMPLE_DATE_FORMAT_MMDD_yyyyMMdd_HH_mm_ss) SimpleDateFormat simpleDateFormat) {
        return new IBTickLoaderImpl(ibController, contracts, tickListeners, simpleDateFormat);
    }
}
