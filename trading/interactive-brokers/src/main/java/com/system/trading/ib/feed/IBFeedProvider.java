package com.system.trading.ib.feed;

import com.ib.client.Contract;
import com.ib.client.EClientSocket;
import com.system.trading.core.SecurityType;
import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.candle.CandleListener;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.feed.FeedProviderStateListener;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import com.system.trading.ib.IBController;
import com.system.trading.ib.TWSRequestId;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

// todo: Create IBCandleLoaderImpl to load all candleListeners with previous candle. The same thing like IBTickLoader.
public class IBFeedProvider implements FeedProvider, TickListener, CandleListener, HistoricalEndListener {
    private static final Logger LOGGER = Logger.getLogger(IBFeedProvider.class.getName());
    private final SimpleDateFormat simpleDateFormat;
    private final List<TickListener> tickListeners;
    private final List<CandleListener> candleListeners;
    private final List<Contract> contracts;
    private final EClientSocket eClientSocket;
    private final IBTickLoader tickLoader;
    private final AtomicBoolean isLoading;

    public IBFeedProvider(IBController ibController, IBTickLoader tickLoader, List<Contract> contracts,
                          List<TickListener> tickListeners, SimpleDateFormat simpleDateFormat) {
        this.eClientSocket = ibController.getClient();
        this.tickLoader = tickLoader;
        this.contracts = contracts;
        this.tickListeners = tickListeners;
        this.simpleDateFormat = simpleDateFormat;
        candleListeners = new ArrayList<>();
        isLoading = new AtomicBoolean(false);
    }

    @Override
    public void addFeedProviderStateListener(FeedProviderStateListener feedProviderStateListener) {
        // method not used
    }

    @Override
    public void addTickListener(TickListener tickListener) {
        tickListeners.add(tickListener);
    }

    @Override
    public void addCandleListener(CandleListener candleListener) {
        candleListeners.add(candleListener);
    }

    @Override
    public void add(com.system.trading.core.Contract contract) {
        if (contractIsRegistered(contract)) {
            return;
        }

        String ibSecType = "";
        if (contract.getSecType().equals(SecurityType.FUTURE))
            ibSecType = "FUT";

        Contract ibContract = new Contract();
        ibContract.symbol(contract.getSymbol());
        ibContract.secType(ibSecType);
        ibContract.currency(contract.getCurrency());
        ibContract.exchange(contract.getExchange());
        ibContract.lastTradeDateOrContractMonth(contract.getLastTradeDateOrContractMonth());

        contracts.add(ibContract);
    }

    /**
     * Loading candle listener with one hour worth of 10 sec candles.
     */
    @Override
    public void loadWithCandle() {
        isLoading.set(true);
        String loadingEndDateString = simpleDateFormat.format(new Date());

        contracts.forEach(contract -> eClientSocket.reqHistoricalData(TWSRequestId.LOADING_DATA.getId(), contract, loadingEndDateString, "10800 S", "5 secs", "TRADES", 0, 1, false, null));
    }

    @Override
    public void loadWithTick(Date startLoadingDate) {
        tickLoader.load(startLoadingDate);
    }

    private Boolean isLoading() {
        return isLoading.get() && tickLoader.isLoading();
    }

    // todo: extract the subscribe logic into its own class FeedSubscriber
    @Override
    public void subscribe() {
        // todo: while the feed provider is loading the core with ticks, save in cache the newest live ticks from IB. After loading, dump newest tick to ticks listeners with parameters isLoading = true. Then, the core is ready to start trading ..
        // Finish loading before subscribing
        while (isLoading()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        LOGGER.info("Request tick by tick data");
        contracts.forEach(contract -> {
            eClientSocket.reqTickByTickData(TWSRequestId.TICK_DATA.getId(), contract, "AllLast", 0, false);
            eClientSocket.reqContractDetails(1, contract);
        });
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        this.tickListeners.forEach(tickListener -> tickListener.notify(tick, isLoading));
    }

    private boolean contractIsRegistered(com.system.trading.core.Contract contract) {
        boolean isRegistered = false;

        for (Contract contractItem : contracts) {
            // todo: the use of equals methods is bad for getSecType since they are not the same type.
            if (contract.getSymbol().equals(contractItem.symbol()) &&
                    contract.getCurrency().equals(contractItem.currency()) &&
                    contract.getExchange().equals(contractItem.exchange()) &&
                    // fixme: 'equals()' between objects of inconvertible types 'SecurityType' and 'String'
                    contract.getSecType().equals(contractItem.secType().name())) {
                isRegistered = true;
            }
        }

        return isRegistered;
    }

    @Override
    public void notify(CandleImpl candleImpl, boolean isLoading) {
        candleListeners.forEach(candleListener -> candleListener.notify(candleImpl, isLoading));
    }

    @Override
    public void notifyEnd(int req, String startDateStr, String endDateStr) {
        if (req == TWSRequestId.LOADING_DATA.getId()) {
            isLoading.set(false);
        }
    }
}
