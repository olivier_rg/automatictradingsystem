package com.system.trading.ib;

import com.ib.client.*;
import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.candle.CandleListener;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import com.system.trading.core.util.DateUtil;
import com.system.trading.ib.error.ErrorListener;
import com.system.trading.ib.error.TwsErrorCode;
import com.system.trading.ib.feed.HistoricalEndListener;
import com.system.trading.ib.order.*;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.DecimalFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@Singleton
public class IBController implements EWrapper {
    private static final Logger LOGGER = Logger.getLogger(IBController.class.getName());
    private static final String TIME_FORMAT = "yyyyMMdd HH:mm:ss";
    private final AtomicBoolean runningApplication = new AtomicBoolean(false);
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final List<TickListener> tickListeners;
    private final List<CandleListener> candleListeners;
    private final List<NextValidIdListener> nextValidIdListeners;
    private final List<OrderStatusListener> orderStatusListeners;
    private final List<ContractDetailsListener> contractDetailsListeners;
    private final List<PositionListener> positionListeners;
    private final List<ErrorListener> errorListeners;
    private HistoricalEndListener historicalEndListener;
    private final List<HistoricalTickLastListener> historicalTickLastListeners;
    private final AtomicBoolean isConnected = new AtomicBoolean(false);
    private EReaderSignal readerSignal;
    private EClientSocket clientSocket;
    private final Properties properties;

    @Inject
    public IBController(Properties properties) {
        this.properties = properties;
        tickListeners = new ArrayList<>();
        nextValidIdListeners = new ArrayList<>();
        orderStatusListeners = new ArrayList<>();
        contractDetailsListeners = new ArrayList<>();
        positionListeners = new ArrayList<>();
        candleListeners = new ArrayList<>();
        historicalTickLastListeners = new ArrayList<>();
        errorListeners = new ArrayList<>();

        runningApplication.set(true);
        initializeConnection();
    }

    private void initializeConnection() {
        readerSignal = new EJavaSignal();
        clientSocket = new EClientSocket(this, readerSignal);

        try {
            String ipAddress = properties.getProperty(IBProperties.INTERACTIVE_BROKER_IP_ADDRESS);
            int port = Integer.parseInt(properties.getProperty(IBProperties.INTERACTIVE_BROKER_PORT));
            int clientId = Integer.parseInt(properties.getProperty(IBProperties.INTERACTIVE_BROKER_CLIENT_ID));
            connect(ipAddress, port, clientId);
        } catch (Exception e) {
            LOGGER.error("Error with interactive broker connection properties");
            LOGGER.error("Cannot connect to server.");
        }
    }

    //AtomicBoolean reconnecting = new AtomicBoolean(false);
   /* public boolean isReconnecting() {
        return reconnecting.get();
    }*/

    /*public void reconnect() {
        if (!reconnecting.get()) {
            reconnecting.set(true);

            if (clientSocket.isConnected())
                disconnect();

            try {
                LOGGER.info("Waiting for tws");
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            while (reconnecting.get() && runningApplication.get()) {
                LOGGER.info("Trying reconnect");
                clientSocket.eConnect("127.0.0.1", 7497, 35);

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LOGGER.info("Reconnected " + clientSocket.isConnected());
                if (clientSocket.isConnected()) {
                    reconnecting.set(false);
                }
            }
            *//*
            try {
                LOGGER.info("Waiting for tws");
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*//*
            LOGGER.info("End of reconnected!");
        }
    }*/

/*    private void disconnect() {
        LOGGER.info("Begin disconnection from tws.");
        clientSocket.eDisconnect();
        readerSignal.issueSignal();
        LOGGER.info("Disconnect from tws.");
    }*/

    private void connect(String ipAddress, int port, int clientId) {
        LOGGER.info("Begin connection to tws.");
        LOGGER.info("IP address: " + ipAddress);
        LOGGER.info("Port: " + port);
        LOGGER.info("Client id: " + clientId);

        clientSocket.eConnect(ipAddress, port, clientId);
        LOGGER.info("Client socket is connected: " + clientSocket.isConnected());
        executorService.execute(() -> {
            EReader reader;
            reader = new EReader(clientSocket, readerSignal);
            LOGGER.info("Reader created");
            reader.start();

            while (runningApplication.get()) {
                while (clientSocket.isConnected()) {
                    if (!isConnected.get())
                        isConnected.set(true);

                    readerSignal.waitForSignal();
                    try {
                        reader.processMsgs();
                    } catch (Exception e) {
                        LOGGER.info("Exception: " + e.getMessage());
                        for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                            System.err.println(stackTraceElement.toString());
                        }
                        LOGGER.info("");
                    }
                }
            }
            LOGGER.info("Exiting reader thread id " + Thread.currentThread().getId());
        });

        // todo: In a production application, it would be best to wait for callbacks to confirm the connection is
        LOGGER.info("Waiting for client to be connected.");
        while (isConnected.get()) {
        }
        LOGGER.info("Client is connected.");
    }


    public void reqHistoricalData(Contract contract, String formattedEndDate, String formattedDurationInSecond,
                                  String formattedBarSize){
        clientSocket.reqHistoricalData(4001, contract, formattedEndDate, formattedDurationInSecond, formattedBarSize,
                "TRADES", 1, 1, false, null);
    }

    @Override
    public void error(Exception e) {
        // At this point, try to reconnect by sending an Error Code 502
        LOGGER.error("Exception: " + e.getMessage());
        errorListeners.forEach(errorListener -> errorListener.handle(-1, TwsErrorCode._502.getErrorCode()));
    }

    public void setHistoricalEndListener(HistoricalEndListener historicalEndListener) {
        this.historicalEndListener = historicalEndListener;
    }

    @Override
    public void historicalDataEnd(int reqId, String startDateStr, String endDateStr) {
        LOGGER.info("HistoricalDataEnd. " + reqId + " - Start Date: " + startDateStr + ", End Date: " + endDateStr);
        historicalEndListener.notifyEnd(reqId, startDateStr, endDateStr);
    }

    public void addCandleListeners(CandleListener candleListener) {
        this.candleListeners.add(candleListener);
    }

    @Override
    public void historicalData(int reqId, Bar bar) {
        LOGGER.info("HistoricalData. " + reqId + " - Date: " + bar.time() + ", Open: " + bar.open() + ", High: " + bar.high() + ", Low: " + bar.low() + ", Close: " + bar.close() + ", Volume: " + bar.volume() + ", Count: " + bar.count() + ", WAP: " + bar.wap());
        candleListeners.forEach(candleListener -> {
            long time = DateUtil.getMillisecondFromTime(bar.time(), TIME_FORMAT);
            CandleImpl candleImpl = new CandleImpl(time, (float) bar.open(), (float) bar.close(), (float) bar.low(),
                    (float) bar.high(), (int) bar.volume());
            candleListener.notify(candleImpl, true);
        });
    }

    public void addHistoricalTickLastListener(HistoricalTickLastListener historicalTickLastListener) {
        this.historicalTickLastListeners.add(historicalTickLastListener);
    }

    public void addPositionListener(PositionListener positionListener) {
        this.positionListeners.add(positionListener);
    }

    @Override
    public void positionMulti(int reqId, String account, String modelCode,
                              Contract contract, double pos, double avgCost) {
        LOGGER.info("Position Multi. Request: " + reqId + ", Account: " + account + ", ModelCode: " + modelCode + ", Symbol: " + contract.symbol() + ", SecType: " + contract.secType() + ", Currency: " + contract.currency() + ", Position: " + pos + ", Avg cost: " + avgCost + "\n");
        positionListeners.forEach(positionListener -> positionListener.notify(reqId, account, modelCode, contract, pos, avgCost));
    }

    public void addOrderStatusListener(OrderStatusListener orderStatusListener) {
        this.orderStatusListeners.add(orderStatusListener);
    }

    //! [orderstatus]
    @Override
    public void orderStatus(int orderId, String status, double filled,
                            double remaining, double avgFillPrice, int permId, int parentId,
                            double lastFillPrice, int clientId, String whyHeld, double mktCapPrice) {
        LOGGER.info("OrderStatus. Id: " + orderId + ", Status: " + status + ", Filled" + filled + ", Remaining: " + remaining
                + ", AvgFillPrice: " + avgFillPrice + ", PermId: " + permId + ", ParentId: " + parentId + ", LastFillPrice: " + lastFillPrice +
                ", ClientId: " + clientId + ", WhyHeld: " + whyHeld + ", MktCapPrice: " + mktCapPrice);

        IBOrderStatus IBOrderStatus = new IBOrderStatus(orderId, status, filled,
                remaining, avgFillPrice, permId, parentId,
                lastFillPrice, clientId, whyHeld, mktCapPrice);
        orderStatusListeners.forEach(orderStatusListener -> orderStatusListener.notify(IBOrderStatus));
    }
    //! [orderstatus]

    public void addTickListerner(TickListener tickListener) {
        tickListeners.add(tickListener);
    }

    @Override
    public void tickByTickAllLast(int reqId, int tickType, long time, double price, int size, TickAttribLast tickAttribLast,
                                  String exchange, String specialConditions) {
        tickListeners.forEach(tickListener -> tickListener.notify(new Tick(time, (float) price, size), false));
    }

    public void addNextValidIdListener(NextValidIdListener nextValidIdListener) {
        this.nextValidIdListeners.add(nextValidIdListener);
    }

    @Override
    public void nextValidId(int orderId) {
        LOGGER.info("Next Valid Id: [" + orderId + "]");
        nextValidIdListeners.forEach(nextValidIdListener -> nextValidIdListener.notify(orderId));
    }

    public EClientSocket getClient() {
        return clientSocket;
    }

    @Override
    public void historicalTicksLast(int reqId, List<HistoricalTickLast> ticks, boolean done) {
        LOGGER.info("Received historical tick last: " + ticks.size() + " ticks");
        if (ticks.isEmpty()) {
            historicalTickLastListeners.forEach(historicalTickLastListener -> historicalTickLastListener.notifyEmptyRequest(reqId));
        } else {
            for (HistoricalTickLast tick : ticks) {
                boolean isDone = ticks.indexOf(tick) == ticks.size() - 1 && done;
                this.historicalTickLastListeners.forEach(historicalTickLastListener -> historicalTickLastListener.notify(reqId, tick.time(), tick.price(), tick.size(), isDone));
            }
        }
    }

    //! [tickprice]
    @Override
    public void tickPrice(int tickerId, int field, double price, TickAttrib attribs) {
        LOGGER.info("Tick Price. Ticker Id:" + tickerId + ", Field: " + field + ", Price: " + price + ", CanAutoExecute: " + attribs.canAutoExecute()
                + ", pastLimit: " + attribs.pastLimit() + ", pre-open: " + attribs.preOpen());
    }
    //! [tickprice]


    @Override
    public void tickSize(int tickerId, int field, int size) {
        LOGGER.info("Tick Size. Ticker Id:" + tickerId + ", Field: " + field + ", Size: " + size);
    }

    @Override
    public void tickOptionComputation(int i, int i1, int i2, double v, double v1, double v2, double v3, double v4, double v5, double v6, double v7) {
        LOGGER.info("tickOptionComputation ");
    }

    //! [tickgeneric]
    @Override
    public void tickGeneric(int tickerId, int tickType, double value) {
        LOGGER.info("Tick Generic. Ticker Id:" + tickerId + ", Field: " + TickType.getField(tickType) + ", Value: " + value);
    }
    //! [tickgeneric]

    //! [tickstring]

    @Override
    public void tickString(int tickerId, int tickType, String value) {
        LOGGER.info("Tick string. Ticker Id:" + tickerId + ", Type: " + tickType + ", Value: " + value);
    }
    //! [tickstring]

    @Override
    public void tickEFP(int tickerId, int tickType, double basisPoints,
                        String formattedBasisPoints, double impliedFuture, int holdDays,
                        String futureLastTradeDate, double dividendImpact,
                        double dividendsToLastTradeDate) {
        LOGGER.info("TickEFP. " + tickerId + ", Type: " + tickType + ", BasisPoints: " + basisPoints + ", FormattedBasisPoints: " +
                formattedBasisPoints + ", ImpliedFuture: " + impliedFuture + ", HoldDays: " + holdDays + ", FutureLastTradeDate: " + futureLastTradeDate +
                ", DividendImpact: " + dividendImpact + ", DividendsToLastTradeDate: " + dividendsToLastTradeDate);
    }

    //! [openorder]
    @Override
    public void openOrder(int orderId, Contract contract, Order order,
                          OrderState orderState) {
        LOGGER.info(EWrapperMsgGenerator.openOrder(orderId, contract, order, orderState));
    }
    //! [openorder]

    //! [openorderend]
    @Override
    public void openOrderEnd() {
        LOGGER.info("OpenOrderEnd");
    }
    //! [openorderend]

    //! [updateaccountvalue]
    @Override
    public void updateAccountValue(String key, String value, String currency,
                                   String accountName) {
        LOGGER.info("UpdateAccountValue. Key: " + key + ", Value: " + value + ", Currency: " + currency + ", AccountName: " + accountName);
    }
    //! [updateaccountvalue]

    //! [updateportfolio]
    @Override
    public void updatePortfolio(Contract contract, double position,
                                double marketPrice, double marketValue, double averageCost,
                                double unrealizedPNL, double realizedPNL, String accountName) {
        LOGGER.info("UpdatePortfolio. " + contract.symbol() + ", " + contract.secType() + " @ " + contract.exchange()
                + ": Position: " + position + ", MarketPrice: " + marketPrice + ", MarketValue: " + marketValue + ", AverageCost: " + averageCost
                + ", UnrealizedPNL: " + unrealizedPNL + ", RealizedPNL: " + realizedPNL + ", AccountName: " + accountName);
    }
    //! [updateportfolio]

    //! [updateaccounttime]
    @Override
    public void updateAccountTime(String timeStamp) {
        LOGGER.info("UpdateAccountTime. Time: " + timeStamp + "\n");
    }
    //! [updateaccounttime]

    //! [accountdownloadend]
    @Override
    public void accountDownloadEnd(String accountName) {
        LOGGER.info("Account download finished: " + accountName + "\n");
    }
    //! [accountdownloadend]

    //! [contractdetails]

    public void addContractDetailsListener(ContractDetailsListener contractDetailsListener) {
        contractDetailsListeners.add(contractDetailsListener);
    }

    @Override
    public void contractDetails(int reqId, ContractDetails contractDetails) {
        LOGGER.info(EWrapperMsgGenerator.contractDetails(reqId, contractDetails));

        contractDetailsListeners.forEach(contractDetailsListener -> contractDetailsListener.notify(reqId, contractDetails));
    }
    //! [contractdetails]

    @Override
    public void bondContractDetails(int reqId, ContractDetails contractDetails) {
        LOGGER.info(EWrapperMsgGenerator.bondContractDetails(reqId, contractDetails));
    }

    //! [contractdetailsend]
    @Override
    public void contractDetailsEnd(int reqId) {
        LOGGER.info("ContractDetailsEnd. " + reqId + "\n");
    }
    //! [contractdetailsend]

    //! [execdetails]
    @Override
    public void execDetails(int reqId, Contract contract, Execution execution) {
        LOGGER.info("ExecDetails. " + reqId + " - [" + contract.symbol() + "], [" + contract.secType() + "], [" + contract.currency() + "], [" + execution.execId() +
                "], [" + execution.orderId() + "], [" + execution.shares() + "]" + ", [" + execution.lastLiquidity() + "]");
    }
    //! [execdetails]

    //! [execdetailsend]
    @Override
    public void execDetailsEnd(int reqId) {
        LOGGER.info("ExecDetailsEnd. " + reqId + "\n");
    }
    //! [execdetailsend]

    //! [updatemktdepth]
    @Override
    public void updateMktDepth(int tickerId, int position, int operation,
                               int side, double price, int size) {
        LOGGER.info("UpdateMarketDepth. " + tickerId + " - Position: " + position + ", Operation: " + operation + ", Side: " + side + ", Price: " + price + ", Size: " + size + "");
    }
    //! [updatemktdepth]

    //! [updatemktdepthl2]
    @Override
    public void updateMktDepthL2(int tickerId, int position,
                                 String marketMaker, int operation, int side, double price, int size, boolean isSmartDepth) {
        LOGGER.info("UpdateMarketDepthL2. " + tickerId + " - Position: " + position + ", Operation: " + operation + ", Side: " + side + ", Price: " + price + ", Size: " + size + ", isSmartDepth: " + isSmartDepth);
    }
    //! [updatemktdepthl2]

    //! [updatenewsbulletin]
    @Override
    public void updateNewsBulletin(int msgId, int msgType, String message,
                                   String origExchange) {
        LOGGER.info("News Bulletins. " + msgId + " - Type: " + msgType + ", Message: " + message + ", Exchange of Origin: " + origExchange + "\n");
    }
    //! [updatenewsbulletin]

    //! [managedaccounts]
    @Override
    public void managedAccounts(String accountsList) {
        LOGGER.info("Account list: " + accountsList);
    }
    //! [managedaccounts]

    //! [receivefa]
    @Override
    public void receiveFA(int faDataType, String xml) {
        LOGGER.info("Receiving FA: " + faDataType + " - " + xml);
    }
    //! [receivefa]

    //! [scannerparameters]
    @Override
    public void scannerParameters(String xml) {
        LOGGER.info("ScannerParameters. " + xml + "\n");
    }
    //! [scannerparameters]

    //! [scannerdata]
    @Override
    public void scannerData(int reqId, int rank,
                            ContractDetails contractDetails, String distance, String benchmark,
                            String projection, String legsStr) {
        LOGGER.info("ScannerData. " + reqId + " - Rank: " + rank + ", Symbol: " + contractDetails.contract().symbol() + ", SecType: " + contractDetails.contract().secType() + ", Currency: " + contractDetails.contract().currency()
                + ", Distance: " + distance + ", Benchmark: " + benchmark + ", Projection: " + projection + ", Legs String: " + legsStr);
    }
    //! [scannerdata]

    //! [scannerdataend]
    @Override
    public void scannerDataEnd(int reqId) {
        LOGGER.info("ScannerDataEnd. " + reqId);
    }
    //! [scannerdataend]

    //! [realtimebar]

    @Override
    public void realtimeBar(int reqId, long time, double open, double high,
                            double low, double close, long volume, double wap, int count) {
        LOGGER.info("RealTimeBars. " + reqId + " - Time: " + time + ", Open: " + open + ", High: " + high + ", Low: " + low + ", Close: " + close + ", Volume: " + volume + ", Count: " + count + ", WAP: " + wap);
    }
    //! [realtimebar]

    @Override
    public void currentTime(long time) {
        LOGGER.info("currentTime");
    }
    //! [fundamentaldata]

    @Override
    public void fundamentalData(int reqId, String data) {
        LOGGER.info("FundamentalData. ReqId: [" + reqId + "] - Data: [" + data + "]");
    }
    //! [fundamentaldata]

    @Override
    public void deltaNeutralValidation(int reqId, DeltaNeutralContract deltaNeutralContract) {
        LOGGER.info("deltaNeutralValidation");
    }

    //! [ticksnapshotend]
    @Override
    public void tickSnapshotEnd(int reqId) {
        LOGGER.info("TickSnapshotEnd: " + reqId);
    }
    //! [ticksnapshotend]

    //! [marketdatatype]
    @Override
    public void marketDataType(int reqId, int marketDataType) {
        LOGGER.info("MarketDataType. [" + reqId + "], Type: [" + marketDataType + "]\n");
    }
    //! [marketdatatype]

    //! [commissionreport]
    @Override
    public void commissionReport(CommissionReport commissionReport) {
        LOGGER.info("CommissionReport. [" + commissionReport.execId() + "] - [" + commissionReport.commission() + "] [" + commissionReport.currency() + "] RPNL [" + commissionReport.realizedPNL() + "]");
    }
    //! [commissionreport]

    //! [position]
    @Override
    public void position(String account, Contract contract, double pos,
                         double avgCost) {
        LOGGER.info("Position. " + account + " - Symbol: " + contract.symbol() + ", SecType: " + contract.secType() + ", Currency: " + contract.currency() + ", Position: " + pos + ", Avg cost: " + avgCost);
    }
    //! [position]

    //! [positionend]
    @Override
    public void positionEnd() {
        LOGGER.info("PositionEnd \n");
    }
    //! [positionend]

    //! [accountsummary]

    @Override

    public void historicalTicksBidAsk(int reqId, List<HistoricalTickBidAsk> ticks, boolean done) {
        for (HistoricalTickBidAsk tick : ticks) {
            LOGGER.info(EWrapperMsgGenerator.historicalTickBidAsk(reqId, tick.time(), tick.tickAttribBidAsk(), tick.priceBid(), tick.priceAsk(), tick.sizeBid(),
                    tick.sizeAsk()));
        }
    }

    @Override
    public void accountSummary(int reqId, String account, String tag,
                               String value, String currency) {
        LOGGER.info("Acct Summary. ReqId: " + reqId + ", Acct: " + account + ", Tag: " + tag + ", Value: " + value + ", Currency: " + currency);
    }
    //! [accountsummary]
    //! [accountsummaryend]

    @Override
    public void accountSummaryEnd(int reqId) {
        LOGGER.info("AccountSummaryEnd. Req Id: " + reqId + "\n");
    }
    //! [accountsummaryend]


    @Override
    public void verifyMessageAPI(String apiData) {
        LOGGER.info("verifyMessageAPI");
    }

    @Override
    public void verifyCompleted(boolean isSuccessful, String errorText) {
        LOGGER.info("verifyCompleted");
    }

    @Override
    public void verifyAndAuthMessageAPI(String apiData, String xyzChallenge) {
        LOGGER.info("verifyAndAuthMessageAPI");
    }

    @Override
    public void verifyAndAuthCompleted(boolean isSuccessful, String errorText) {
        LOGGER.info("verifyAndAuthCompleted");
    }
    //! [displaygrouplist]

    @Override
    public void displayGroupList(int reqId, String groups) {
        LOGGER.info("Display Group List. ReqId: " + reqId + ", Groups: " + groups + "\n");
    }
    //! [displaygrouplist]
    //! [displaygroupupdated]

    @Override
    public void displayGroupUpdated(int reqId, String contractInfo) {
        LOGGER.info("Display Group Updated. ReqId: " + reqId + ", Contract info: " + contractInfo + "\n");
    }
    //! [displaygroupupdated]

    @Override
    public void error(String str) {
        LOGGER.info("Error STR");
    }
    //! [error]


    public void addErrorListener(ErrorListener errorListener) {
        this.errorListeners.add(errorListener);
    }

    @Override
    public void error(int id, int errorCode, String errorMsg) {
        LOGGER.info("Error. Id: " + id + ", Code: " + errorCode + ", Msg: " + errorMsg + "\n");
        errorListeners.forEach(errorListener -> errorListener.handle(id, errorCode));
    }
    //! [error]

    @Override
    public void connectionClosed() {
        LOGGER.info("Connection closed");
    }

    //! [connectack]
    @Override
    public void connectAck() {
        if (clientSocket.isAsyncEConnect()) {
            LOGGER.info("Acknowledging connection");
            clientSocket.startAPI();
        }
    }

    @Override
    public void positionMultiEnd(int reqId) {
        LOGGER.info("Position Multi End. Request: " + reqId + "\n");
    }

    //! [positionmultiend]
    //! [accountupdatemulti]
    @Override
    public void accountUpdateMulti(int reqId, String account, String modelCode,
                                   String key, String value, String currency) {
        LOGGER.info("Account Update Multi. Request: " + reqId + ", Account: " + account + ", ModelCode: " + modelCode + ", Key: " + key + ", Value: " + value + ", Currency: " + currency + "\n");
    }

    //! [accountupdatemulti]
    //! [accountupdatemultiend]
    @Override
    public void accountUpdateMultiEnd(int reqId) {
        LOGGER.info("Account Update Multi End. Request: " + reqId + "\n");
    }

    //! [accountupdatemultiend]
    //! [securityDefinitionOptionParameter]
    @Override
    public void securityDefinitionOptionalParameter(int reqId, String exchange,
                                                    int underlyingConId, String tradingClass, String multiplier,
                                                    Set<String> expirations, Set<Double> strikes) {
        LOGGER.info("Security Definition Optional Parameter. Request: " + reqId + ", Trading Class: " + tradingClass + ", Multiplier: " + multiplier + " \n");
    }

    //! [securityDefinitionOptionParameter]
    //! [securityDefinitionOptionParameterEnd]
    @Override
    public void securityDefinitionOptionalParameterEnd(int reqId) {
        LOGGER.info("Security Definition Optional Parameter End. Request: " + reqId);
    }

    //! [securityDefinitionOptionParameterEnd]
    //! [softDollarTiers]
    @Override
    public void softDollarTiers(int reqId, SoftDollarTier[] tiers) {
        for (SoftDollarTier tier : tiers) {
            LOGGER.info("tier: " + tier.toString() + ", ");
        }

        LOGGER.info("");
    }

    //! [softDollarTiers]
    //! [familyCodes]
    @Override
    public void familyCodes(FamilyCode[] familyCodes) {
        for (FamilyCode fc : familyCodes) {
            System.out.print("Family Code. AccountID: " + fc.accountID() + ", FamilyCode: " + fc.familyCodeStr());
        }

        LOGGER.info("");
    }

    //! [familyCodes]
    //! [symbolSamples]
    @Override
    public void symbolSamples(int reqId, ContractDescription[] contractDescriptions) {
        LOGGER.info("Contract Descriptions. Request: " + reqId + "\n");
        for (ContractDescription cd : contractDescriptions) {
            Contract c = cd.contract();
            StringBuilder derivativeSecTypesSB = new StringBuilder();
            for (String str : cd.derivativeSecTypes()) {
                derivativeSecTypesSB.append(str);
                derivativeSecTypesSB.append(",");
            }
            System.out.print("Contract. ConId: " + c.conid() + ", Symbol: " + c.symbol() + ", SecType: " + c.secType() +
                    ", PrimaryExch: " + c.primaryExch() + ", Currency: " + c.currency() +
                    ", DerivativeSecTypes:[" + derivativeSecTypesSB.toString() + "]");
        }

        LOGGER.info("");
    }

    //! [symbolSamples]
    //! [mktDepthExchanges]
    @Override
    public void mktDepthExchanges(DepthMktDataDescription[] depthMktDataDescriptions) {
        for (DepthMktDataDescription depthMktDataDescription : depthMktDataDescriptions) {
            LOGGER.info("Depth Mkt Data Description. Exchange: " + depthMktDataDescription.exchange() +
                    ", ListingExch: " + depthMktDataDescription.listingExch() +
                    ", SecType: " + depthMktDataDescription.secType() +
                    ", ServiceDataType: " + depthMktDataDescription.serviceDataType() +
                    ", AggGroup: " + depthMktDataDescription.aggGroup()
            );
        }
    }

    //! [mktDepthExchanges]
    //! [tickNews]
    @Override
    public void tickNews(int tickerId, long timeStamp, String providerCode, String articleId, String headline, String extraData) {
        LOGGER.info("Tick News. TickerId: " + tickerId + ", TimeStamp: " + timeStamp + ", ProviderCode: " + providerCode + ", ArticleId: " + articleId + ", Headline: " + headline + ", ExtraData: " + extraData + "\n");
    }

    //! [tickNews]
    //! [smartcomponents]
    @Override
    public void smartComponents(int reqId, Map<Integer, Entry<String, Character>> theMap) {
        LOGGER.info("smart components req id:" + reqId);

        for (Entry<Integer, Entry<String, Character>> item : theMap.entrySet()) {
            LOGGER.info("bit number: " + item.getKey() +
                    ", exchange: " + item.getValue().getKey() + ", exchange letter: " + item.getValue().getValue());
        }
    }

    //! [smartcomponents]
    //! [tickReqParams]
    @Override
    public void tickReqParams(int tickerId, double minTick, String bboExchange, int snapshotPermissions) {
        LOGGER.info("Tick req params. Ticker Id:" + tickerId + ", Min tick: " + minTick + ", bbo exchange: " + bboExchange + ", Snapshot permissions: " + snapshotPermissions);
    }

    //! [tickReqParams]
    //! [newsProviders]
    @Override
    public void newsProviders(NewsProvider[] newsProviders) {
        for (NewsProvider np : newsProviders) {
            System.out.print("News Provider. ProviderCode: " + np.providerCode() + ", ProviderName: " + np.providerName() + "\n");
        }

        LOGGER.info("");
    }

    //! [newsProviders]
    //! [newsArticle]
    @Override
    public void newsArticle(int requestId, int articleType, String articleText) {
        LOGGER.info("News Article. Request Id: " + requestId + ", ArticleType: " + articleType +
                ", ArticleText: " + articleText);
    }

    //! [newsArticle]
    //! [historicalNews]
    @Override
    public void historicalNews(int requestId, String time, String providerCode, String articleId, String headline) {
        LOGGER.info("Historical News. RequestId: " + requestId + ", Time: " + time + ", ProviderCode: " + providerCode + ", ArticleId: " + articleId + ", Headline: " + headline + "\n");
    }

    //! [historicalNews]
    //! [historicalNewsEnd]
    @Override
    public void historicalNewsEnd(int requestId, boolean hasMore) {
        LOGGER.info("Historical News End. RequestId: " + requestId + ", HasMore: " + hasMore + "\n");
    }

    //! [historicalNewsEnd]
    //! [headTimestamp]
    @Override
    public void headTimestamp(int reqId, String headTimestamp) {
        LOGGER.info("Head timestamp. Req Id: " + reqId + ", headTimestamp: " + headTimestamp);
    }

    //! [headTimestamp]
    //! [histogramData]
    @Override
    public void histogramData(int reqId, List<HistogramEntry> items) {
        LOGGER.info(EWrapperMsgGenerator.histogramData(reqId, items));
    }

    //! [histogramData]
    //! [historicalDataUpdate]
    @Override
    public void historicalDataUpdate(int reqId, Bar bar) {
        LOGGER.info("HistoricalDataUpdate. " + reqId + " - Date: " + bar.time() + ", Open: " + bar.open() + ", High: " + bar.high() + ", Low: " + bar.low() + ", Close: " + bar.close() + ", Volume: " + bar.volume() + ", Count: " + bar.count() + ", WAP: " + bar.wap());
    }

    //! [historicalDataUpdate]
    //! [rerouteMktDataReq]
    @Override
    public void rerouteMktDataReq(int reqId, int conId, String exchange) {
        LOGGER.info(EWrapperMsgGenerator.rerouteMktDataReq(reqId, conId, exchange));
    }

    //! [rerouteMktDataReq]
    //! [rerouteMktDepthReq]
    @Override
    public void rerouteMktDepthReq(int reqId, int conId, String exchange) {
        LOGGER.info(EWrapperMsgGenerator.rerouteMktDepthReq(reqId, conId, exchange));
    }

    //! [rerouteMktDepthReq]
    //! [marketRule]
    @Override
    public void marketRule(int marketRuleId, PriceIncrement[] priceIncrements) {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setMaximumFractionDigits(340);
        LOGGER.info("Market Rule Id: " + marketRuleId);
        for (PriceIncrement pi : priceIncrements) {
            LOGGER.info("Price Increment. Low Edge: " + df.format(pi.lowEdge()) + ", Increment: " + df.format(pi.increment()));
        }
    }

    //! [marketRule]
    //! [pnl]
    @Override
    public void pnl(int reqId, double dailyPnL, double unrealizedPnL, double realizedPnL) {
        LOGGER.info(EWrapperMsgGenerator.pnl(reqId, dailyPnL, unrealizedPnL, realizedPnL));
    }

    //! [pnl]
    //! [pnlsingle]
    @Override
    public void pnlSingle(int reqId, int pos, double dailyPnL, double unrealizedPnL, double realizedPnL, double value) {
        LOGGER.info(EWrapperMsgGenerator.pnlSingle(reqId, pos, dailyPnL, unrealizedPnL, realizedPnL, value));
    }

    //! [pnlsingle]
    //! [historicalticks]
    @Override
    public void historicalTicks(int reqId, List<HistoricalTick> ticks, boolean done) {
        for (HistoricalTick tick : ticks) {
            LOGGER.info(EWrapperMsgGenerator.historicalTick(reqId, tick.time(), tick.price(), tick.size()));
        }
    }

    //! [historicalticks]
    //! [historicalticksbidask]
    //! [historicalticksbidask]


    //! [tickbytickbidask]
    @Override
    public void tickByTickBidAsk(int reqId, long time, double bidPrice, double askPrice, int bidSize, int askSize,
                                 TickAttribBidAsk tickAttribBidAsk) {
        LOGGER.info(EWrapperMsgGenerator.tickByTickBidAsk(reqId, time, bidPrice, askPrice, bidSize, askSize, tickAttribBidAsk));
    }
    //! [tickbytickbidask]

    //! [tickbytickmidpoint]
    @Override
    public void tickByTickMidPoint(int reqId, long time, double midPoint) {
        LOGGER.info(EWrapperMsgGenerator.tickByTickMidPoint(reqId, time, midPoint));
    }
    //! [tickbytickmidpoint]

    //! [orderbound]
    @Override
    public void orderBound(long orderId, int apiClientId, int apiOrderId) {
        LOGGER.info(EWrapperMsgGenerator.orderBound(orderId, apiClientId, apiOrderId));
    }
    //! [orderbound]

    //! [completedorder]
    @Override
    public void completedOrder(Contract contract, Order order, OrderState orderState) {
        LOGGER.info(EWrapperMsgGenerator.completedOrder(contract, order, orderState));
    }
    //! [completedorder]

    //! [completedordersend]
    @Override
    public void completedOrdersEnd() {
        LOGGER.info(EWrapperMsgGenerator.completedOrdersEnd());
    }

    @Override
    public void replaceFAEnd(int i, String s) {
        LOGGER.info("Replace FA End i " + i + " " + s);
    }


    //! [completedordersend]
}

