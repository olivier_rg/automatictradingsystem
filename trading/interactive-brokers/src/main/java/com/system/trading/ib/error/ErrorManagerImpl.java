package com.system.trading.ib.error;

import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class ErrorManagerImpl implements ErrorListener {
    private static final Logger LOGGER = Logger.getLogger(ErrorManagerImpl.class.getName());
    private final List<TwsErrorCodeHandler> twsErrorCodeHandlers;

    @Inject
    public ErrorManagerImpl(TwsErrorCode1102Handler twsErrorCode1102Handler, TwsErrorCode507Handler twsErrorCode507Handler,
                            TwsErrorCode502Handler twsErrorCode502Handler, TwsErrorCode501Handler twsErrorCode501Handler,
                            TwsErrorCode504Handler twsErrorCode504Handler) {
        twsErrorCodeHandlers = new ArrayList<>();
        twsErrorCodeHandlers.add(twsErrorCode1102Handler);
        twsErrorCodeHandlers.add(twsErrorCode507Handler);
        twsErrorCodeHandlers.add(twsErrorCode502Handler);
        twsErrorCodeHandlers.add(twsErrorCode501Handler);
        twsErrorCodeHandlers.add(twsErrorCode504Handler);
    }

    @Override
    public void handle(int id, int errorCode) {
        LOGGER.info("Received error with errorCode=" + errorCode + " to handle");
        twsErrorCodeHandlers.forEach(twsErrorCodeHandler -> {
            if (twsErrorCodeHandler.isFixing(errorCode)) {
                twsErrorCodeHandler.handle();
            }
        });
    }
}


