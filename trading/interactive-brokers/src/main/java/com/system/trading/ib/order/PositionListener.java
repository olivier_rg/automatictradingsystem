package com.system.trading.ib.order;

import com.ib.client.Contract;

public interface PositionListener {
    void notify(int reqId, String account, String modelCode, Contract contract, double pos, double avgCost);
}
