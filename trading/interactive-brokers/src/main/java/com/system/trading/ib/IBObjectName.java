package com.system.trading.ib;

public class IBObjectName {
    public static final String FEED_PROVIDER_TICK_LISTENERS = "FEED_PROVIDER_TICK_LISTENERS";
    public static final String SIMPLE_DATE_FORMAT_MMDD_yyyyMMdd_HH_mm_ss = "SIMPLE_DATE_FORMAT_MMDD_yyyyMMdd_HH_mm_ss";
    public static final String IB_PROPERTIES = "IB_PROPERTIES";
}
