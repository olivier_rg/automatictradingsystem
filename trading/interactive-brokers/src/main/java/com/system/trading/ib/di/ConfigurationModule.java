package com.system.trading.ib.di;

import com.system.trading.ib.IBObjectName;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.text.SimpleDateFormat;

@Module
public class ConfigurationModule {
    @Singleton
    @Provides
    @Named(IBObjectName.SIMPLE_DATE_FORMAT_MMDD_yyyyMMdd_HH_mm_ss)
    SimpleDateFormat provideSimpleDateFormat() {
        return new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    }

}
