package com.system.trading.ib.error;

public interface ErrorListener {
    void handle(int id, int errorCode);
}
