package com.system.trading.ib.order;

import com.ib.client.Order;
import com.system.trading.core.order.OrderStatus;

public class IBOrder extends Order implements com.system.trading.core.order.Order {
    @Override
    public Float getRequestPrice() {
        return (float) lmtPrice();
    }

    @Override
    public OrderStatus getOrderStatus() {
        // todo: to implement
        return null;
    }

    @Override
    public Float getFilledPrice() {
        // todo: to implement
        return null;
    }

    @Override
    public Long getTimeFilledPrice() {
        // todo: to implement
        return null;
    }

    @Override
    public Float getClosedPrice() {
        // todo: to implement
        return null;
    }

    @Override
    public Long getTimeClosePrice() {
        // todo: to implement
        return null;
    }

    @Override
    public boolean isMarketSell() {
        // todo: to implement
        return false;
    }
}
