package com.system.trading.ib.error;

public enum TwsErrorCode {
    // Connectivity between IB and Trader Workstation has been restored - data maintained. All data farms are connected: usfuture; euhmds; fundfarm; ushmds; secdefil.
    _1102(1102),
    //Error Code: 507, Msg: Bad Message Length null
    _507(507),
    //Error Code: 502, Msg: Couldn't connect to TWS. Confirm that "Enable ActiveX and Socket Clients" is enabled and connection port is the same as "Socket Port" on the TWS "Edit->Global Configuration...->API->Settings" menu. Live Trading ports: TWS: 7496; IB Gateway: 4001. Simulated Trading ports for new installations of version 954.1 or newer: TWS: 7497; IB Gateway: 4002
    _502(502),
    // Error Code: 501, Msg: Already connected.
    _501(501),
    // Error Code: 504, Msg: Not connected.
    _504(504);

    private int errorCode;

    TwsErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}