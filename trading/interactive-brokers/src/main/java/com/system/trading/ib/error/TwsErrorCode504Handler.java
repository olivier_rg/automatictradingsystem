package com.system.trading.ib.error;

import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TwsErrorCode504Handler extends AbstractTwsErrorCode {
    private static final Logger LOGGER = Logger.getLogger(TwsErrorCode504Handler.class.getName());

    @Inject
    public TwsErrorCode504Handler() {
        super(TwsErrorCode._504);
    }

    @Override
    public void handle() {
    }
}
