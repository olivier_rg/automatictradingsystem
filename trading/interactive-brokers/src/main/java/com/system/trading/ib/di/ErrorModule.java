package com.system.trading.ib.di;

import com.system.trading.core.Initializer;
import com.system.trading.ib.IBController;
import com.system.trading.ib.error.ErrorManagerImpl;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

import javax.inject.Singleton;

@Module
public class ErrorModule {
    @Singleton
    @Provides
    @IntoSet
    public Initializer provideErrorModuleInitializer(IBController ibController, ErrorManagerImpl errorManager) {
        return () -> ibController.addErrorListener(errorManager);
    }
}
