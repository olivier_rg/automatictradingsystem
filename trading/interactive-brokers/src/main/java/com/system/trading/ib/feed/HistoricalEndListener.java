package com.system.trading.ib.feed;

public interface HistoricalEndListener {
    void notifyEnd(int req, String startDateStr, String endDateStr);
}
