package com.system.trading.downloader.di;

import com.system.trading.core.orm.InstrumentDAO;
import com.system.trading.core.orm.ExchangeDAO;
import com.system.trading.core.pojo.Exchange;
import com.system.trading.core.pojo.Instrument;
import com.system.trading.core.pojo.InstrumentType;
import dagger.Module;
import dagger.Provides;

import java.util.List;

@Module
public class ModelModule {

    @Provides
    List<Instrument> provideSymbols(InstrumentDAO instrumentDAO){
        return instrumentDAO.getInstruments();
    }

    @Provides
    List<InstrumentType> provideInstrumentType(InstrumentDAO instrumentDAO) { return instrumentDAO.getExchangeTypes();}

    @Provides
    List<Exchange> provideExchanges(ExchangeDAO exchangeDAO) { return exchangeDAO.getExchanges();}
}
