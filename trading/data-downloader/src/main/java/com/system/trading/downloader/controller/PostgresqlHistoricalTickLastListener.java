package com.system.trading.downloader.controller;

import com.ib.client.Util;
import com.system.trading.core.SettingPropertyNames;
import com.system.trading.ib.order.HistoricalTickLastListener;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Properties;

public class PostgresqlHistoricalTickLastListener implements HistoricalTickLastListener {
    private static final Logger LOGGER = Logger.getLogger(PostgresqlHistoricalTickLastListener.class.getName());

    private Connection connection;
    private int lastReqId = -1;
    private int exchangeId;
    private int instrumentId;

    // fixme: should inject session instead of the properties file
    public PostgresqlHistoricalTickLastListener(Properties properties) {
        try {
            connection = DriverManager.getConnection(properties.getProperty(SettingPropertyNames.DATABASE_URL),
                    properties.getProperty(SettingPropertyNames.DATABASE_USERNAME),
                    properties.getProperty(SettingPropertyNames.DATABASE_PASSWORD));
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void notify(int reqId, long tickTime, double price, long volume, boolean done) {
        if (reqId != lastReqId) {
            System.out.println("Tick: " + Util.UnixSecondsToString(tickTime, "yyyyMMdd-HH:mm:ss") + " Price" +
                    ": " + price + " " +
                    "Volume: " + volume);
            lastReqId = reqId;
        }


        String query =
                "INSERT INTO public.tick(datetime, price, volume, exchangeid, instrumentid) VALUES " +
                        "(" + tickTime + ", " + price + ", " + volume + ", " + exchangeId + ", " + instrumentId + ");";

        try (PreparedStatement pst = connection.prepareStatement(query)) {
            pst.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void notifyEmptyRequest(int reqId) {

    }

    public void setExchange(String exchange) {
        String query = "SELECT e.id FROM exchange e WHERE e.name = '" + exchange + "';";

        try {
            ResultSet rs = connection.createStatement().executeQuery(query);

            if (rs.next()) {
                exchangeId = rs.getInt("id");

                LOGGER.info("Registered exchange id:" + exchangeId + " for " + exchange);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

    }

    public void setInstrument(String symbol) {
        String query = "SELECT i.id FROM instrument i WHERE i.symbol = '" + symbol + "';";

        try {
            ResultSet rs = connection.createStatement().executeQuery(query);

            if (rs.next()) {
                instrumentId = rs.getInt("id");

                LOGGER.info("Registered instrument id:" + instrumentId + " for " + symbol);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
