package com.system.trading.downloader;

public enum DataType {
    BAR_DAILY("Bar Daily");

    private final String name;

    DataType(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
