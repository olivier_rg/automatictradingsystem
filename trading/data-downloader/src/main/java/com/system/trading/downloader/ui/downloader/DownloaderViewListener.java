package com.system.trading.downloader.ui.downloader;

import com.system.trading.core.pojo.Instrument;
import com.system.trading.downloader.DataType;

import java.util.Date;

public interface DownloaderViewListener {
    void onButtonStart(Instrument instrument, Date startDate,
                       Date endDate, DataType dataType);
}
