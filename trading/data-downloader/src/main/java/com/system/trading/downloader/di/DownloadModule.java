package com.system.trading.downloader.di;

import com.system.trading.core.pojo.Instrument;
import com.system.trading.downloader.DataType;
import com.system.trading.downloader.ui.downloader.DownloaderView;
import com.system.trading.downloader.ui.downloader.DownloaderViewImpl;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import javax.swing.*;
import java.util.List;

@Module
public class DownloadModule {
    @Provides
    @Singleton
    DownloaderView provideDownloaderView(DownloaderViewImpl downloaderView) {
        return downloaderView;
    }

    @Provides
    @Singleton
    DefaultComboBoxModel<DataType> provideDataTypeComboboxModel() {
        return new DefaultComboBoxModel<>(DataType.values());
    }

    @Provides
    @Singleton
    DefaultComboBoxModel<Instrument> provideComboboxModelInstrument(List<Instrument> instruments) {
        var model = new DefaultComboBoxModel<Instrument>();
        model.addElement(null);
        model.addAll(instruments);
        return model;
    }

    @Provides
    @Singleton
    BoundedRangeModel provideBoundedRangeModel(){
        return  new DefaultBoundedRangeModel();
    }
}
