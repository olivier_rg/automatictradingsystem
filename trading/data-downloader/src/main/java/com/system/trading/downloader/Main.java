package com.system.trading.downloader;

import com.formdev.flatlaf.FlatDarkLaf;
import com.system.trading.core.Initializer;
import com.system.trading.downloader.di.ApplicationComponent;
import com.system.trading.downloader.di.DaggerApplicationComponent;

public class Main {

    public static void main(String[] args) {
        FlatDarkLaf.install();

        ApplicationComponent applicationComponent = DaggerApplicationComponent.builder().build();
        applicationComponent.initializers().forEach(Initializer::initialize);
        applicationComponent.entryPoint().start();
    }
}
