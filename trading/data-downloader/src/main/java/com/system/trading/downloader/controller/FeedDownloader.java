package com.system.trading.downloader.controller;

import com.system.trading.core.pojo.Instrument;

import java.util.Calendar;

public interface FeedDownloader {
    void downloadHistoricalBar(Instrument instrument, Calendar startDate, Calendar endDate);
}
