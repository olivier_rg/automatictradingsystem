/*
 * Created by JFormDesigner on Sun Nov 15 16:24:16 EST 2020
 */

package com.system.trading.downloader.ui.downloader;

import com.system.trading.core.pojo.Instrument;
import com.system.trading.downloader.DataType;
import com.system.trading.downloader.ui.AbstractComboBoxItemRenderer;
import com.system.trading.downloader.ui.DateLabelFormatter;
import net.miginfocom.swing.MigLayout;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.util.Properties;

@Singleton
public class DownloaderViewImpl extends JFrame implements DownloaderView {
    private DownloaderViewListener downloaderViewListener;

    @Inject
    public DownloaderViewImpl(
            DefaultComboBoxModel<Instrument> instrumentComboBoxModel,
            DefaultComboBoxModel<DataType> dataTypeComboboxModel,
            BoundedRangeModel boundedRangeModel) {

        initComponents(instrumentComboBoxModel, new UtilDateModel(), new UtilDateModel(), dataTypeComboboxModel, boundedRangeModel);

        setTitle("Data Downloader");
        setSize(300, 290);
    }

    private void initComponents(
            ComboBoxModel<Instrument> instrumentComboBoxModel, UtilDateModel dateStartModel, UtilDateModel dateEndModel,
            DefaultComboBoxModel<DataType> dataTypeComboboxModel, BoundedRangeModel boundedRangeModel) {

        var contentPane = getContentPane();
        contentPane.setLayout(new MigLayout(
                "hidemode 3, fill",
                // columns
                "",
                // rows
                ""));

        // DATA INFORMATION COLUMN
        JPanel dataInformationPanel = new JPanel(new MigLayout("hidemode 3, hidemode 3", "[fill][fill]", ""));
        TitledBorder dataInformationTitleBorder = new TitledBorder("Data Information");
        dataInformationPanel.setBorder(dataInformationTitleBorder);
        contentPane.add(dataInformationPanel, "aligny 0, grow");

        // Data Type
        JLabel labelDataType = new JLabel("Data Type");
        dataInformationPanel.add(labelDataType);
        JComboBox<DataType> comboBoxDataType = new JComboBox<>(dataTypeComboboxModel);
        comboBoxDataType.setRenderer(new AbstractComboBoxItemRenderer() {
            @Override
            public String getConcreteText(Object item) {
                DataType dataType = (DataType) item;
                return dataType.getName();
            }
        });
        dataInformationPanel.add(comboBoxDataType, "wrap");

        // Symbol
        JLabel labelSymbol = new JLabel("Symbol ");
        dataInformationPanel.add(labelSymbol);
        JComboBox<Instrument> comboBoxSymbol = new JComboBox<>(instrumentComboBoxModel);
        comboBoxSymbol.setRenderer(new AbstractComboBoxItemRenderer() {
            @Override
            public String getConcreteText(Object item) {
                Instrument instrument = (Instrument) item;
                return instrument.getSymbol();
            }
        });
        dataInformationPanel.add(comboBoxSymbol, "wrap");

        // Date Start
        dataInformationPanel.add(new JLabel("Date Start "));
        JDatePanelImpl dateStartPanel = new JDatePanelImpl(dateStartModel, new Properties());
        JDatePickerImpl dateStartPicker = new JDatePickerImpl(dateStartPanel, new DateLabelFormatter());
        dataInformationPanel.add(dateStartPicker, "wrap");

        // Date End
        dataInformationPanel.add(new JLabel("Date End "));
        JDatePanelImpl dateEndPanel = new JDatePanelImpl(dateEndModel, new Properties());
        JDatePickerImpl dateEndPicker = new JDatePickerImpl(dateEndPanel, new DateLabelFormatter());
        dataInformationPanel.add(dateEndPicker, "wrap, pushy");

        // Button stop and start
        JButton stopButton = new JButton("Stop");
        dataInformationPanel.add(stopButton);
        JButton startButton = new JButton("Start");
        startButton.addActionListener(e -> this.downloaderViewListener.onButtonStart(
                (Instrument) instrumentComboBoxModel.getSelectedItem(),
                dateStartModel.getValue(),
                dateEndModel.getValue(),
                (DataType) dataTypeComboboxModel.getSelectedItem()
        ));
        dataInformationPanel.add(startButton, "span 2, wrap");
        // Progress Bar
        JProgressBar progressBar = new JProgressBar(boundedRangeModel);
        dataInformationPanel.add(progressBar, "span 3");

        pack();
        setLocationRelativeTo(getOwner());
    }

    @Override
    public void set(DownloaderViewListener downloaderViewListener) {
        this.downloaderViewListener = downloaderViewListener;
    }
}
