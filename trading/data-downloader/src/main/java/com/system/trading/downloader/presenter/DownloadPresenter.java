package com.system.trading.downloader.presenter;

import com.system.trading.core.orm.CandleDAO;
import com.system.trading.core.orm.InstrumentDAO;
import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.pojo.Instrument;
import com.system.trading.core.pojo.InstrumentType;
import com.system.trading.core.util.DateUtil;
import com.system.trading.downloader.DataType;
import com.system.trading.downloader.controller.YahooBarDownloader;
import com.system.trading.downloader.ui.downloader.DownloaderView;
import com.system.trading.downloader.ui.downloader.DownloaderViewListener;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

@Singleton
public class DownloadPresenter implements DownloaderViewListener {

    private final DownloaderView view;
    private final CandleDAO candleDAO;
    private final InstrumentDAO instrumentDAO;
    private final YahooBarDownloader yahooBarDownloader;
    private final BoundedRangeModel boundedRangeModel;

    @Inject
    public DownloadPresenter(DownloaderView view, CandleDAO candleDAO, InstrumentDAO instrumentDAO,
                             YahooBarDownloader yahooBarDownloader, BoundedRangeModel boundedRangeModel) {
        this.view = view;
        this.candleDAO = candleDAO;
        this.instrumentDAO = instrumentDAO;
        this.yahooBarDownloader = yahooBarDownloader;
        this.boundedRangeModel = boundedRangeModel;
    }

    @Override
    public void onButtonStart(Instrument instrument, Date startDate, Date endDate, DataType dataType) {
        Executors.newSingleThreadExecutor().execute(() -> {
            Calendar to = createEndCalendar(endDate);

            List<Instrument> instruments = new ArrayList<>();
            if (instrument == null) {
                instruments.addAll(instrumentDAO.getInstrumentsByType(InstrumentType.STOCK));
            } else {
                instruments.add(instrument);
            }

            synchronized (boundedRangeModel){
                boundedRangeModel.setMaximum(instruments.size());
                boundedRangeModel.setMinimum(1);
            }

            int i = 0;
            for (Instrument ticker : instruments) {
                Calendar from = createFromCalendar(ticker, dataType, startDate);
                yahooBarDownloader.downloadHistoricalBar(ticker, from, to);
                synchronized (boundedRangeModel){
                    boundedRangeModel.setValue(i++);
                }
            }
        });
    }


    private Calendar createFromCalendar(Instrument instrument, DataType dataType, Date startDate) {
        var from = Calendar.getInstance();

        if (startDate == null) {
            CandleImpl candle = candleDAO.getLastCandle(instrument);

            if (candle != null && dataType.equals(DataType.BAR_DAILY)) {
                from.setTime(new Date(candle.getTime() + DateUtil.MILLISECOND_IN_DAY));
            } else {
                from.set(2017, 1, 1);
            }
        } else {
            from.setTime(startDate);
        }

        return from;
    }

    private Calendar createEndCalendar(Date endDate) {
        var to = Calendar.getInstance();
        if ((endDate == null)) {
            endDate = new Date();
        }
        to.setTime(endDate);
        return to;
    }
}
