package com.system.trading.downloader.controller;

import com.system.trading.core.orm.CandleDAO;
import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.pojo.Instrument;
import com.system.trading.core.pojo.TimeFrames;
import org.apache.log4j.Logger;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.Calendar;

@Singleton
public class YahooBarDownloader implements FeedDownloader {
    private static final Logger LOGGER = Logger.getLogger(YahooBarDownloader.class.getName());
    private final CandleDAO candleDAO;

    @Inject
    public YahooBarDownloader(CandleDAO candleDAO) {
        this.candleDAO = candleDAO;
    }

    @Override
    public void downloadHistoricalBar(Instrument instrument, Calendar startDate, Calendar endDate) {
        Stock stock = null;

        try {
            stock = YahooFinance.get(instrument.getSymbol(), startDate, endDate, Interval.DAILY);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }

        if (stock != null) {
            try {
                for (HistoricalQuote historicalQuote : stock.getHistory()) {
                    saveCandle(instrument, historicalQuote);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveCandle(Instrument instrument, HistoricalQuote historicalQuote) {
        long time = historicalQuote.getDate().getTime().getTime();
        float open = historicalQuote.getOpen().floatValue();
        float close = historicalQuote.getClose().floatValue();
        float low = historicalQuote.getLow().floatValue();
        float high = historicalQuote.getHigh().floatValue();
        int volume = historicalQuote.getVolume().intValue();
        var candle = new CandleImpl(time, open, close, low, high, volume, TimeFrames.DAILY, instrument);

        candleDAO.insert(candle);
    }
}
