package com.system.trading.downloader.ui;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;

public abstract class AbstractComboBoxItemRenderer extends BasicComboBoxRenderer {
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index,
                isSelected, cellHasFocus);

        if (value != null) {
            //Symbol item = (Symbol) value;
            //setText(item.getSymbol().toUpperCase());
            setText(getConcreteText(value));
        }

        return this;
    }

    public abstract String getConcreteText(Object item);
}
