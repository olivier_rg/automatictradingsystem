package com.system.trading.downloader.controller;

import com.ib.client.Contract;
import com.system.trading.core.pojo.Instrument;
import com.system.trading.ib.IBController;
import com.system.trading.ib.IBObjectName;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

@Singleton
public class IBHistoricalBarDownloader {
    private final IBController ibController;
    private final SimpleDateFormat simpleDateFormat;

    @Inject
    public IBHistoricalBarDownloader(IBController ibController,
                                     @Named(IBObjectName.SIMPLE_DATE_FORMAT_MMDD_yyyyMMdd_HH_mm_ss) SimpleDateFormat simpleDateFormat) {
        this.ibController = ibController;
        this.simpleDateFormat = simpleDateFormat;
        ibController.addCandleListeners(new PostgresqlHistoricalBarListener());
    }

    public void downloadHistoricalBar(String secType, String exchange, List<Instrument> instruments, String currency, Date startDate,
                                      Date endDate) {
        if (endDate == null) {
            endDate = new Date();
        }

        List<Contract> contracts = generateContracts(secType, exchange, currency, instruments);
        long durationInDays = ChronoUnit.DAYS.between(startDate.toInstant(), endDate.toInstant());
        String formattedEndDate = simpleDateFormat.format(endDate);

        Executors.newSingleThreadScheduledExecutor().execute(() -> {
            contracts.forEach(contract -> {
                ibController.reqHistoricalData(contract, formattedEndDate, durationInDays + " D", "1 day");

                // todo wait req finish downloading
            });
        });
    }

    private List<Contract> generateContracts(String secType, String exchange, String currency, List<Instrument> instruments) {
        List<Contract> contracts = new ArrayList<>();

        for (Instrument instrument : instruments) {
            Contract contract = new Contract();
            contract.symbol(instrument.getSymbol());
            contract.exchange(exchange);
            contract.secType(secType);
            contracts.add(contract);
            contract.currency(currency);
            contracts.add(contract);
        }

        return contracts;
    }
}
