package com.system.trading.downloader;

public enum DataSource {
    //  INTERACTIVE_BROKERS("Interactive Brokers"),
    YAHOO_FINANCE("Yahoo Finance");

    private final String name;

    DataSource(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
