package com.system.trading.downloader.controller;

import com.ib.client.Contract;
import com.system.trading.core.orm.InstrumentDAO;
import com.system.trading.core.pojo.Instrument;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.util.DateUtil;
import com.system.trading.ib.IBController;
import com.system.trading.ib.order.HistoricalTickLastListener;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class IBHistoricalTickDownloader implements HistoricalTickLastListener {
    static Logger LOGGER = Logger.getLogger(IBHistoricalTickDownloader.class.getName());
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    private final PostgresqlHistoricalTickLastListener postgresqlHistoricalTickLastListener;
    private final AtomicInteger lastReqId;
    private final InstrumentDAO instrumentDAO;
    private final IBController ibController;
    private Long lastTime;
    private long endDateLong;
    private int currentReqId = 1;

    @Inject
    public IBHistoricalTickDownloader(Properties properties, IBController ibController, InstrumentDAO instrumentDAO) {
        this.ibController = ibController;
        this.instrumentDAO = instrumentDAO;
        lastReqId = new AtomicInteger(0);
        lastTime = 0l;

        postgresqlHistoricalTickLastListener = new PostgresqlHistoricalTickLastListener(properties);
        // register listener that listen to new tick and save to database
        ibController.addHistoricalTickLastListener(postgresqlHistoricalTickLastListener);
        ibController.addHistoricalTickLastListener(this);
    }

    @Override
    public void notify(int reqId, long tickTime, double price, long volume, boolean done) {
        if (done) {
            lastReqId.set(reqId);
            // tick time represent number of second. Multiply by 1000 to convert time to millisecond
            lastTime = tickTime * 1000;
        }
    }

    @Override
    public void notifyEmptyRequest(int reqId) {
        System.out.println("Empty request " + reqId);
    }

    public void downloadHistoricalTicks(Date startDate, Date endDate, List<Instrument> instruments, String secType,
                                        String exchange, String currency, String contractMonth, boolean startFromLatest) {
        postgresqlHistoricalTickLastListener.setExchange(exchange);

        if (endDate == null) {
            endDate = new Date();
        }

        if (startFromLatest) {
            startDate = DateUtil.addSecond(retrieveLatestDownloadedTickDate(instruments.get(0), secType));
        }

        List<Contract> contracts = generateContracts(secType, exchange, currency, contractMonth, instruments);
        String formattedStartDate = simpleDateFormat.format(startDate);

        for (Contract contract : contracts) {
            postgresqlHistoricalTickLastListener.setInstrument(contract.symbol());

            endDateLong = endDate.getTime();
            lastTime = startDate.getTime();

            String timeFormatted = formattedStartDate;

            while (lastTime <= endDateLong) {
                LOGGER.info("New request historical ticks - starting date:" + simpleDateFormat.format(new Date(lastTime)));
                ibController.getClient().reqHistoricalTicks(currentReqId, contract, timeFormatted, null, 999,
                        "TRADES", 0, true,
                        null);

                waitForRequestToFinish();

                Date time = DateUtil.addSecond(new Date(lastTime));
                timeFormatted = simpleDateFormat.format(time);
                currentReqId++;
            }
        }
    }

    private Date retrieveLatestDownloadedTickDate(Instrument instrument, String secType) {
        Tick tick = instrumentDAO.retrieveLatestTick(instrument.getSymbol(), secType);
        Date retDate;

        if (tick != null) {
            retDate = new Date(tick.getDatetime() * DateUtil.MILLISECOND_IN_SECOND);
        } else {
            retDate = new Date();
        }

        return retDate;
    }

    private List<Contract> generateContracts(String secType, String exchange, String currency,
                                             String contractMonth, List<Instrument> instruments) {
        List<Contract> contracts = new ArrayList<>();

        for (Instrument instrument : instruments) {
            Contract contract = new Contract();
            contract.symbol(instrument.getSymbol());
            contract.exchange(exchange);
            contract.secType(secType);
            contract.currency(currency);
            contract.lastTradeDateOrContractMonth(contractMonth);
            contracts.add(contract);
        }

        return contracts;
    }

    private void waitForRequestToFinish() {
        boolean waiting = true;

        while (waiting) {
            if (lastReqId.get() == currentReqId) {
                waiting = false;
            }
        }
    }
}
