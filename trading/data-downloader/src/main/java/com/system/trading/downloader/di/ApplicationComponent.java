package com.system.trading.downloader.di;

import com.system.trading.core.Initializer;
import com.system.trading.core.di.CommonPropertiesModule;
import com.system.trading.core.di.DatabaseModule;
import com.system.trading.downloader.Application;
import com.system.trading.ib.di.ConfigurationModule;
import dagger.Component;

import javax.inject.Singleton;
import java.util.Set;

@Singleton
@Component(modules = {MainModule.class, ConfigurationModule.class, ModelModule.class, DatabaseModule.class,
        CommonPropertiesModule.class, DownloadModule.class})
public interface ApplicationComponent {
    Application entryPoint();
    Set<Initializer> initializers();
}
