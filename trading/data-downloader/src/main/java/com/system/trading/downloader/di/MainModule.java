package com.system.trading.downloader.di;

import com.system.trading.core.Initializer;
import com.system.trading.core.util.PropertiesUtil;
import com.system.trading.downloader.Application;
import com.system.trading.downloader.presenter.DownloadPresenter;
import com.system.trading.downloader.ui.downloader.DownloaderViewImpl;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

import javax.inject.Singleton;
import java.util.Properties;

@Module
public abstract class MainModule {

    @Singleton
    @Provides
    static Application entryPoint(DownloaderViewImpl downloaderViewImpl) {
        return () -> {
            downloaderViewImpl.setVisible(true);
        };
    }

    @Provides
    @IntoSet
    @Singleton
    static Initializer provideInitializer(DownloaderViewImpl downloaderViewImpl, DownloadPresenter downloadPresenter) {
        return () -> {
            downloaderViewImpl.set(downloadPresenter);
        };
    }

    @Singleton
    @Provides
    static Properties provideProperties() {
        return PropertiesUtil.retrievePropertiesFromBaseFolder("settings");
    }
}
