package com.system.trading.core.order;

public enum OrderActionType {
    // IB required to get buy and sell as name.
    BUY,
    SELL
}
