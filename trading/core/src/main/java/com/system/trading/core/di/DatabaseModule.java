package com.system.trading.core.di;

import com.system.trading.core.CoreObjectId;
import com.system.trading.core.SettingPropertyNames;
import com.system.trading.core.pojo.*;
import dagger.Module;
import dagger.Provides;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.inject.Named;
import java.util.Properties;
import java.util.logging.Level;

@Module
public class DatabaseModule {

    @Provides
    SessionFactory provideSessionFactory(@Named(CoreObjectId.PROPERTY_GENERAL) Properties properties) {

        Configuration config = new Configuration();
        config.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        config.setProperty("hibernate.connection.url", properties.getProperty(SettingPropertyNames.DATABASE_URL));
        config.setProperty("hibernate.connection.username", properties.getProperty(SettingPropertyNames.DATABASE_USERNAME));
        config.setProperty("hibernate.connection.password", properties.getProperty(SettingPropertyNames.DATABASE_PASSWORD));
        config.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL82Dialect");
        config.setProperty("log4j.logger.org.hibernate", "OFF");
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
        config.setProperty("hibernate.show_sql", "false");

        config.addAnnotatedClass(Tick.class);
        config.addAnnotatedClass(Instrument.class);
        config.addAnnotatedClass(InstrumentType.class);
        config.addAnnotatedClass(Exchange.class);
        config.addAnnotatedClass(CandleImpl.class);
        config.addAnnotatedClass(TimeFrame.class);
        config.addAnnotatedClass(CandleImpl.class);

        return config.buildSessionFactory();

    }
}
