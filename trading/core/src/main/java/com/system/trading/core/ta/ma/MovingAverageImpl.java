package com.system.trading.core.ta.ma;

import com.system.trading.core.pojo.CandleImpl;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public abstract class MovingAverageImpl implements MovingAverage {
    private static final Logger LOGGER = Logger.getLogger(MovingAverageImpl.class.getName());
    private final List<MovingAverageListener> movingAverageListeners;
    protected final List<Float> values;
    protected final List<Float> candleValues;
    protected final int numberOfValue;


    public MovingAverageImpl(int numberOfValue) {
        this.numberOfValue = numberOfValue;
        values = new ArrayList<>();
        candleValues = new ArrayList<>();
        movingAverageListeners = new ArrayList<>();
    }

    @Override
    public final boolean isReady() {
        return values.size() > 0;
    }

    @Override
    public final double get(int index) {
        double value;
        int calculateIndex = index - 1;

        value = values.get(values.size() + calculateIndex);

        return value;
    }

    @Override
    public final int size() {
        return values.size();
    }

    @Override
    public final void notify(CandleImpl candleImpl, boolean isLoading) {
        candleValues.add(candleImpl.getClose());

        if (candleValues.size() >= numberOfValue) {
            int valueCount = values.size();
            calculateAverage();

            // new value has been added
            if (valueCount < values.size()) {
                float newValue = values.get(values.size() - 1);
                //LOGGER.info(Util.UnixSecondsToString(candle.getTime(), "yyyyMMdd-HH:mm:ss zzz") + " New Value: " + newValue);
                candleValues.remove(0);
                movingAverageListeners.forEach(movingAverageListener -> movingAverageListener.notifyNewMovingAverageValue(newValue));
            }
        }
    }

    protected abstract void calculateAverage();

    @Override
    public final void addListener(MovingAverageListener movingAverageListener) {
        movingAverageListeners.add(movingAverageListener);
    }
}
