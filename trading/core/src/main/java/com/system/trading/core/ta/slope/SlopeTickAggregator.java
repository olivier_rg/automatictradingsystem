package com.system.trading.core.ta.slope;

import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import org.apache.log4j.Logger;

public class SlopeTickAggregator implements TickListener {
    private static final Logger LOGGER = Logger.getLogger(SlopeTickAggregator.class.getName());
    private final Slope slope;
    private Tick previousTick;

    public SlopeTickAggregator(Slope slope) {

        this.slope = slope;
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        if (previousTick != null && previousTick.getPrice() != tick.getPrice())
            slope.add(tick);

        previousTick = tick;
    }
}
