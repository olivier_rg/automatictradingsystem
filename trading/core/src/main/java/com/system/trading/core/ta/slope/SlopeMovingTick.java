package com.system.trading.core.ta.slope;

public interface SlopeMovingTick extends Slope {
    void addListener(SlopeTickListener slopeTickListener);
}
