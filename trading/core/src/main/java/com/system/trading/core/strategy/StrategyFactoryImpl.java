package com.system.trading.core.strategy;

import com.system.trading.core.candle.CandleListFactory;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.OrderManager;
import com.system.trading.core.strategy.par.PriceActionRegressionStrategy;
import com.system.trading.core.ta.ma.MovingAverageFactory;
import com.system.trading.core.ta.slope.SlopeFactory;
import com.system.trading.core.tick.TicksFactory;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Properties;

@Singleton
public class StrategyFactoryImpl implements StrategyFactory {
    private static final Logger LOGGER = Logger.getLogger(StrategyFactoryImpl.class.getName());
    private final MovingAverageFactory movingAverageFactory;
    private final FeedProvider feedProvider;
    private final OrderManager orderManager;
    private final CandleListFactory candleListFactory;
    private final TicksFactory ticksFactory;
    private final SlopeFactory slopeFactory;

    @Inject
    public StrategyFactoryImpl(MovingAverageFactory movingAverageFactory, FeedProvider feedProvider,
                               OrderManager orderManager, CandleListFactory candleListFactory, TicksFactory ticksFactory,
                               SlopeFactory slopeFactory) {
        this.movingAverageFactory = movingAverageFactory;
        this.feedProvider = feedProvider;
        this.orderManager = orderManager;
        this.candleListFactory = candleListFactory;
        this.ticksFactory = ticksFactory;
        this.slopeFactory = slopeFactory;
    }

    @Override
    public <T> Strategy create(Class<T> classname, Properties properties) {
        Strategy strategy = null;

        if (classname.equals(PriceActionSMAStrategy.class)) {
            strategy = new PriceActionSMAStrategy(movingAverageFactory, feedProvider, orderManager);
        }
        else if(classname.equals(FakeOutStrategy.class)){
            strategy = new FakeOutStrategy(candleListFactory,orderManager);
        }
        else if(classname.equals(PriceActionRegressionStrategy.class)){
            if (properties != null)
                strategy = new PriceActionRegressionStrategy(orderManager, ticksFactory, slopeFactory, properties);
            else
                LOGGER.error("Missing strategy properties.");
        }

        if (strategy == null)
            LOGGER.error("Strategy Factory could not instantiated strategy.");

        return strategy;
    }
}
