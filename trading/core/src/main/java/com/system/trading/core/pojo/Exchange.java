package com.system.trading.core.pojo;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "exchange")
public class Exchange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public Exchange() {
    }

    public Exchange(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
