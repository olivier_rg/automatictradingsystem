package com.system.trading.core.memento;

public interface Originator {
    void save(String saveIntoObjectId);
}
