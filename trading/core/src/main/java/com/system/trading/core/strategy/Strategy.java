package com.system.trading.core.strategy;

import com.system.trading.core.Contract;

public interface Strategy {
    void setContract(Contract contract);

    Contract getContract();
}
