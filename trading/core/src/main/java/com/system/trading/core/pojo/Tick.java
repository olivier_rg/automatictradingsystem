package com.system.trading.core.pojo;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tick")
public class Tick {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * datetime is the unix seconds ex: 1602502727 would equal to Monday, October 12, 2020 7:38:47 AM.
     */
    @Column(name = "datetime")
    private long datetime;

    @ManyToOne
    @JoinColumn(name = "instrumentid", nullable = false)
    private Instrument instrument;

    private float price;
    private int volume;

    public Tick() {
    }

    public Tick(long datetime, float price, int volume) {
        this.datetime = datetime;
        this.price = price;
        this.volume = volume;
    }
}
