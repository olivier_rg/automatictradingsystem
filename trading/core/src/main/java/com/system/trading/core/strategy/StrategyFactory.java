package com.system.trading.core.strategy;

import java.util.Properties;

public interface StrategyFactory {
    <T> Strategy create(Class<T> classname, Properties properties);
}
