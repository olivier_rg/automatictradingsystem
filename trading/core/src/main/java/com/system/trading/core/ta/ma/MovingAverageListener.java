package com.system.trading.core.ta.ma;

public interface MovingAverageListener {
    void notifyNewMovingAverageValue(float value);
}
