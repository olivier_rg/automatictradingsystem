package com.system.trading.core.strategy;

import com.google.common.collect.EvictingQueue;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.*;
import com.system.trading.core.ta.ma.MovingAverageFactory;
import com.system.trading.core.ta.ma.MovingAverageListener;
import com.system.trading.core.ta.ma.SimpleMovingAverage;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Singleton
public class PriceActionSMAStrategy extends TickBaseStrategyImpl implements TickListener, MovingAverageListener {
    private static final Logger LOGGER = Logger.getLogger(PriceActionSMAStrategy.class.getName());
    private final SimpleMovingAverage simpleMovingAverage20;
    private final EvictingQueue<Double> prices;
    private double lastMovingAverageValueTraded;

    @Inject
    public PriceActionSMAStrategy(MovingAverageFactory movingAverageFactory, FeedProvider feedProvider,
                                  OrderManager orderManager) {
        super(orderManager);

        simpleMovingAverage20 = movingAverageFactory.createSimpleMovingAverageTick(40, 100000);
        simpleMovingAverage20.addListener(this);
        feedProvider.addTickListener(this);

        // todo: should be able to change the maxSize value when the tick volume change.
        prices = EvictingQueue.create(100000);
        lastMovingAverageValueTraded = 0;
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        prices.add((double) tick.getPrice());

        if (isNotBetweenTradingHours(tick))
            return;

        if (orderManager.hasPosition(getContract()))
            return;

        // Cancel pending orders that their price are way to far from the current tick trading price
        if (orderManager.hasPendingOrders()) {

            List<Integer> orders = orderManager.getLimitPendingOrderIds();

            for (Integer orderId : orders) {
                Order order = orderManager.getLimitPendingOrderById(orderId);

                if (tick.getPrice() > order.getRequestPrice() + 5) {
                    orderManager.cancelPendingOrderById(orderId);
                }
            }

            // Exit notify method if there are still active orders waiting to be fill at a reasonable price.
            if (!orderManager.getLimitPendingOrderIds().isEmpty()) {
                return;
            }
        }

        if (simpleMovingAverage20.size() < 1)
            return;

        if (lastMovingAverageValueTraded == simpleMovingAverage20.get(0))
            return;

        if (priceTraverseAverage()) {
            // todo: Once there is a buy trigger, follow sma (period=4, ticks=2000) and if the price go below or above, sell position.
            double slop = calculatePriceDirection();
            OrderActionType orderActionType;
            OrderPriceInformation runnerOrderPriceInformation;
            if (slop > 0) {
                runnerOrderPriceInformation = OrderPriceInformation.builder().requestPrice(tick.getPrice())
                        .takeProfitPoint(tick.getPrice() + 5f)
                        .trailingStopPoint(5f)
                        .build();
                orderActionType = OrderActionType.BUY;
            } else {
                runnerOrderPriceInformation = OrderPriceInformation.builder().requestPrice(tick.getPrice())
                        .takeProfitPoint(tick.getPrice() - 5f)
                        .trailingStopPoint(5f)
                        .build();
                orderActionType = OrderActionType.SELL;
            }

            try {
                orderManager.buy(getContract(), OrderType.LIMIT, orderActionType, 1, runnerOrderPriceInformation);
            } catch (Exception e) {
                e.printStackTrace();
            }

            lastMovingAverageValueTraded = simpleMovingAverage20.get(0);
        }
    }

    // todo: extract in a DateTimeUtil class isBetweenHours.
    private boolean isNotBetweenTradingHours(Tick tick) {
        long timeMilliseconds = tick.getDatetime() * 1000;
        Date date = new Date(timeMilliseconds);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);

        boolean isTradingHours = false;
        if ((hour >= 10) && (hour <= 16)) {
            isTradingHours = true;
        }

        return isTradingHours;
    }

    private double calculatePriceDirection() {
        SimpleRegression regression = new SimpleRegression();

        final double[] x = {0};
        prices.forEach(aFloat -> regression.addData(aFloat, x[0]++));

        LOGGER.info("Price Action SMA Strategy - Slope Value: " + regression.getSlope());
        return regression.getSlope();
    }

    private boolean priceTraverseAverage() {
        double latestAveragePrice = simpleMovingAverage20.get(0);
        boolean tickExistBeforePrice = false;
        boolean tickExistAfterPrice = false;

        for (Double price : this.prices) {
            if (price < latestAveragePrice) {
                tickExistBeforePrice = true;
            } else {
                tickExistAfterPrice = true;
            }
        }

        return tickExistBeforePrice && tickExistAfterPrice;
    }

    @Override
    public void notifyNewMovingAverageValue(float value) {
        prices.clear();
    }
}
