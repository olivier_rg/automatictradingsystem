package com.system.trading.core.strategy;

import com.system.trading.core.order.OrderManager;
import com.system.trading.core.tick.TickListener;

public abstract class TickBaseStrategyImpl extends StrategyImpl implements TickListener {
    public TickBaseStrategyImpl(OrderManager orderManager) {
        super(orderManager);
    }
}
