package com.system.trading.core.candle;

import com.system.trading.core.feed.FeedProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CandleListFactory {
    private final FeedProvider feedProvider;

    @Inject
    public CandleListFactory(FeedProvider feedProvider){

        this.feedProvider = feedProvider;
    }

    public CandleTicks createCandleTicks(int tickNumber){
        CandleTicks candleTicks = new CandleTicks(tickNumber);
        feedProvider.addTickListener(candleTicks);
        return candleTicks;
    }
}
