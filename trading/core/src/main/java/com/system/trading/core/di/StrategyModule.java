package com.system.trading.core.di;

import com.system.trading.core.CoreObjectId;
import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.strategy.StrategyFactoryImpl;
import com.system.trading.core.strategy.StrategyManager;
import com.system.trading.core.strategy.StrategyManagerImpl;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Properties;

@Module
public class StrategyModule {
    @Singleton
    @Provides
    public StrategyManager strategyManager(StrategyFactoryImpl strategyFactoryImpl, FeedProvider feedProvider,
                                           @Named(CoreObjectId.PROPERTY_STRATEGY) Properties properties) {
        return new StrategyManagerImpl(strategyFactoryImpl, feedProvider, properties);
    }
}
