package com.system.trading.core.ta.ma;

import com.system.trading.core.candle.CandleTicks;
import com.system.trading.core.feed.FeedProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MovingAverageFactory {

    private final FeedProvider feedProvider;

    @Inject
    public MovingAverageFactory(FeedProvider feedProvider) {

        this.feedProvider = feedProvider;
    }

    public SimpleMovingAverage createSimpleMovingAverageTick(int period, int tickNumber) {
        CandleTicks candleTicks = new CandleTicks(tickNumber);
        SimpleMovingAverage simpleMovingAverage = new SimpleMovingAverage(period);
        candleTicks.addCandleListener(simpleMovingAverage);
        feedProvider.addTickListener(candleTicks);
        feedProvider.addCandleListener(candleTicks);
        return simpleMovingAverage;
    }
}
