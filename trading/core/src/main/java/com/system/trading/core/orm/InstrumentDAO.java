package com.system.trading.core.orm;

import com.system.trading.core.pojo.Instrument;
import com.system.trading.core.pojo.InstrumentType;
import com.system.trading.core.pojo.Tick;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Singleton
public class InstrumentDAO {
    static final Logger logger = Logger.getLogger(InstrumentDAO.class.getName());
    private final SessionFactory sessionFactory;

    @Inject
    public InstrumentDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Tick retrieveLatestTick(String symbol, String secType){
        Session session = sessionFactory.openSession();
        String hql = "SELECT t FROM Tick t JOIN Instrument i on t.instrument = i JOIN InstrumentType" +
                " it on i.instrumentType = it WHERE i.symbol = " +
                "'"+symbol+"' AND it.name = '"+secType+"' ORDER BY t.datetime " +
                "DESC, t.id DESC";

        TypedQuery<Tick> query = session.createQuery(hql, Tick.class);
        query.setMaxResults(1);
        List<Tick> results = query.getResultList();

        Tick tick = null;
        if(results.size()>0)
            tick = results.get(0);

        return tick;
    }

    public List<Tick> retrieveTicks(long requestStartDate, long requestEndDate) {
        Session session = sessionFactory.openSession();
        String hql = "SELECT t FROM Tick t WHERE t.datetime > " + requestStartDate + " AND t.datetime " +
                "< " + requestEndDate;
        TypedQuery<Tick> query = session.createQuery(hql, Tick.class);
        List<Tick> results = query.getResultList();

        logger.info("There are " + results.size() + " Ticks Records Available.");

        return null;
    }

    public List<Instrument> getInstruments() {
        return getInstruments(null);
    }

    public List<Instrument> getInstrumentsByType(InstrumentType instrumentType) {
        return getInstruments(instrumentType);
    }

    private List<Instrument> getInstruments(InstrumentType instrumentType) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Instrument> criteriaQuery = criteriaBuilder.createQuery(Instrument.class);
        Root<Instrument> instrumentRoot = criteriaQuery.from(Instrument.class);

        if(instrumentType != null){
            Predicate predicateForInstrumentType
                    = criteriaBuilder.equal(instrumentRoot.get("instrumentType"), instrumentType);
            criteriaQuery.where(predicateForInstrumentType);
        }

        List<Instrument> instruments = session.createQuery(criteriaQuery).getResultList();

        session.close();
        logger.info("There are " + instruments.size() + " Instruments Records Available.");

        return instruments;
    }

    public List<InstrumentType> getExchangeTypes() {
        Session session = sessionFactory.openSession();

        CriteriaQuery<InstrumentType> cq = session.getCriteriaBuilder().createQuery(InstrumentType.class);
        CriteriaQuery<InstrumentType> all = cq.select(cq.from(InstrumentType.class));

        TypedQuery<InstrumentType> allQuery = session.createQuery(all);
        List<InstrumentType> instrumentTypes = allQuery.getResultList();
        session.close();

        logger.info("There are " + instrumentTypes.size() + " Instruments Records Available.");

        return instrumentTypes;
    }
}
