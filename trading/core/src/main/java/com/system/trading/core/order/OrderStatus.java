package com.system.trading.core.order;

public enum OrderStatus {
    REQUESTED,
    FILLED,
    CLOSED
}
