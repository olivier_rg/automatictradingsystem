package com.system.trading.core.order;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class OrderPriceInformation {
    Float requestPrice;
    Float takeProfitPoint;
    Float stopLossPoint;
    Float trailingStopPoint;
    PriceCondition priceCondition;
}
