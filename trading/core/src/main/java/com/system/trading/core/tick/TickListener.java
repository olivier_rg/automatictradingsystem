package com.system.trading.core.tick;

import com.system.trading.core.pojo.Tick;

public interface TickListener {
    void notify(Tick tick, boolean isLoading);
}
