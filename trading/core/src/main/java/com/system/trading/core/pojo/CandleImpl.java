package com.system.trading.core.pojo;

import com.system.trading.core.candle.Candle;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "candlestick")
public class CandleImpl implements Candle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected int id;

    @ManyToOne
    @JoinColumn(name = "timeframeid", nullable = false)
    protected TimeFrame timeFrame;

    @ManyToOne
    @JoinColumn(name = "instrumentid", nullable = false)
    protected Instrument instrument;

    @Getter
    @Column(name = "datetime")
    protected long time;

    @Getter
    @Column(name = "open")
    protected float open;
    @Getter
    @Column(name = "close")
    protected float close;
    @Getter
    @Column(name = "low")
    protected float low;
    @Getter
    @Column(name = "high")
    protected float high;
    @Getter
    @Column(name = "volume")
    protected int volume;

    public CandleImpl(long time, float open, float close, float low, float high, int volume, TimeFrame timeFrame,
                      Instrument instrument) {
        this.time = time;
        this.open = open;
        this.close = close;
        this.low = low;
        this.high = high;
        this.volume = volume;
        this.timeFrame = timeFrame;
        this.instrument = instrument;
    }

    public CandleImpl(long time, float open, float close, float low, float high, int volume) {
        this(time, open, close, low, high, volume, null, null);
    }

    public CandleImpl() {
    }
}
