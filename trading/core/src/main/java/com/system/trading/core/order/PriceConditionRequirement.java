package com.system.trading.core.order;

public enum PriceConditionRequirement {
    SUPERIOR_OR_EQUAL,
    INFERIOR_OR_EQUAL
}
