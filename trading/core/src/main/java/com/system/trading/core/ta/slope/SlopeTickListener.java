package com.system.trading.core.ta.slope;

import com.system.trading.core.pojo.Tick;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.util.List;

public interface SlopeTickListener {
    void calculatedSlopeValue(List<Tick> collect, SimpleRegression regression);
}
