package com.system.trading.core.order;

import com.system.trading.core.Contract;

import java.util.List;

public interface OrderManager {
    void cancelPendingOrders();

    int buy(Contract contract, OrderType orderType, OrderActionType orderActionType, int quantityContract, OrderPriceInformation orderPriceInformation) throws Exception;

    void sell(int orderId);

    boolean hasPosition(Contract contract);

    boolean hasPendingOrders();

    List<Integer> getLimitPendingOrderIds();

    void cancelPendingOrderById(int id);

    Order getLimitPendingOrderById(int orderId);

    void addOrderListener(OrderListener orderListener);
}
