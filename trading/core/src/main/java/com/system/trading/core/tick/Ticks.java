package com.system.trading.core.tick;

import com.system.trading.core.pojo.Tick;

import java.util.ArrayList;
import java.util.List;

public class Ticks implements TickListener {
    private final List<TickListener> tickListeners;

    public Ticks() {
        tickListeners = new ArrayList<>();
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        tickListeners.forEach(tickListener -> tickListener.notify(tick, isLoading));
    }

    public void addListener(TickListener tickListener) {
        tickListeners.add(tickListener);
    }
}
