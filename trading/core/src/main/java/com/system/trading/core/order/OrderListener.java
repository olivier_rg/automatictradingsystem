package com.system.trading.core.order;

public interface OrderListener {
    void notify(Order order);
}
