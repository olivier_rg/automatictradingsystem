package com.system.trading.core.pojo;

public abstract class TimeFrames {
    public final static TimeFrame DAILY = new TimeFrame(1, "Daily");

    private TimeFrames() {
    }
}
