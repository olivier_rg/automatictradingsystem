package com.system.trading.core.ta.slope;

import com.google.common.collect.EvictingQueue;
import com.system.trading.core.Identifier;
import com.system.trading.core.pojo.Tick;
import lombok.Getter;
import org.apache.log4j.Logger;

@Getter
public class SlopeMovingTickModel implements Identifier {
    private static final Logger LOGGER = Logger.getLogger(SlopeMovingTickModel.class.getName());
    private final String id;
    private EvictingQueue<Tick> ticks;
    private int size;

    private SlopeMovingTickModel(){
        id = null;
    }

    public SlopeMovingTickModel(String id, int size) {
        this.id = id;
        this.size = size;
        ticks = EvictingQueue.create(size);
    }

    public void setSize(int size){
        this.size = size;
        ticks.clear();
        ticks = EvictingQueue.create(size);
    }
}
