package com.system.trading.core.strategy;

public interface StrategyListener {
    void addedNewStrategy(String strategyName);
}
