package com.system.trading.core.order;

public interface Order {
    int orderId();

    Float getRequestPrice();

    OrderStatus getOrderStatus();

    Float getFilledPrice();

    /**
     * Retrieve time in unix second when the order has been filled.
     *
     * @return return time
     */
    Long getTimeFilledPrice();

    Float getClosedPrice();

    /**
     * Retrieve time in unix second when the order has been closed.
     *
     * @return return time
     */
    Long getTimeClosePrice();

    boolean isMarketSell();
}

