package com.system.trading.core.di;

import dagger.Module;

@Module(includes = {StrategyModule.class, CaretakerFileXMLModule.class})
public class CoreTradingModule {
}
