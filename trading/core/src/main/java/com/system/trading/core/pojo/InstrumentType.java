package com.system.trading.core.pojo;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "instrumenttype")
public class InstrumentType {
    public final static InstrumentType FUTURE = new InstrumentType(1, "FUT");
    public static final InstrumentType STOCK = new InstrumentType(2, "STK");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public InstrumentType() {
    }

    public InstrumentType(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
