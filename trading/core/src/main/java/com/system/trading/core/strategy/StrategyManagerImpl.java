package com.system.trading.core.strategy;

import com.system.trading.core.Contract;
import com.system.trading.core.CoreObjectId;
import com.system.trading.core.feed.FeedProvider;
import com.google.common.reflect.TypeToken;
import org.apache.log4j.Logger;

import javax.inject.Named;
import java.util.*;

public class StrategyManagerImpl implements StrategyManager {
    private static final Logger LOGGER = Logger.getLogger(StrategyManagerImpl.class.getName());
    private final Map<Contract, List<Strategy>> contractsStrategies = new HashMap<>();
    private final StrategyFactory strategyFactory;
    private final FeedProvider feedProvider;
    private Properties properties;
    private final List<StrategyListener> strategyListeners;


    public StrategyManagerImpl(StrategyFactoryImpl strategyFactory, FeedProvider feedProvider,
                               @Named(CoreObjectId.PROPERTY_STRATEGY)Properties properties) {
        this.strategyFactory = strategyFactory;
        this.feedProvider = feedProvider;
        this.properties = properties;
        strategyListeners = new ArrayList<>();
    }

    @Override
    public <T> void add(Class<T> strategyClassName, Contract contract) {
        // Get derived interface from strategy classname
        TypeToken<T>.TypeSet tt = TypeToken.of(strategyClassName).getTypes().interfaces();

        if (!tt.interfaces().rawTypes().contains(Strategy.class)) {
            LOGGER.error("Strategy class name must implement Strategy interface");
        } else {
            Strategy strategy = strategyFactory.create(strategyClassName, properties);

            if (contractsStrategies.containsKey(contract)) {
                contractsStrategies.get(contract).add(strategy);
            } else {
                List<Strategy> strategies = new ArrayList<>();
                strategies.add(strategy);
                contractsStrategies.put(contract, strategies);
            }

            strategy.setContract(contract);
            feedProvider.add(contract);
            strategyListeners.forEach(strategyListener -> strategyListener.addedNewStrategy(strategyClassName.getName()));
        }
    }

    @Override
    public void addStrategyListener(StrategyListener strategyListener) {
        strategyListeners.add(strategyListener);
    }
}
