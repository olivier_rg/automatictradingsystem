package com.system.trading.core.candle;

import com.system.trading.core.pojo.CandleImpl;

public interface CandleListener {
    void notify(CandleImpl candleImpl, boolean isLoading);
}
