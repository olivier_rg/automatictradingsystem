package com.system.trading.core.pojo;

import javax.persistence.*;

@Entity
@Table(name = "timeframe")
public class TimeFrame {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    public TimeFrame(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public TimeFrame() {
    }
}
