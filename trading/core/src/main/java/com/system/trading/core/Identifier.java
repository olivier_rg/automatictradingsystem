package com.system.trading.core;

public interface Identifier {
    String getId();
}
