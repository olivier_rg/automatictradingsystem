package com.system.trading.core.candle;

import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.pojo.Instrument;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.pojo.TimeFrame;

import java.util.ArrayList;
import java.util.List;

public class CandleTick extends CandleImpl {
    private int numberOfTick;
    private List<Tick> ticks;
    private boolean isCompleted = false;

    public CandleTick(long time, float open, float close, float low, float high, int volume, TimeFrame timeFrame, Instrument instrument) {
        super(time, open, close, low, high, volume, timeFrame, instrument);
    }

    public CandleTick(int numberOfTick) {
        super(0, Float.MIN_NORMAL, Float.MIN_NORMAL, Float.MAX_VALUE, Float.MIN_NORMAL, 0, null, null);
        this.numberOfTick = numberOfTick;
        ticks = new ArrayList<>();
    }

    public void addTick(Tick tick) {
        ticks.add(tick);
        volume = volume + tick.getVolume();

        if (volume >= numberOfTick) {
            calculateProperties();
            isCompleted = true;
        }
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    private void calculateProperties() {
        time = ticks.get(0).getDatetime();
        close = ticks.get(ticks.size() - 1).getPrice();
        open = ticks.get(0).getPrice();

        for (Tick tick : ticks) {
            if (tick.getPrice() > high)
                high = tick.getPrice();

            if (tick.getPrice() < low)
                low = tick.getPrice();
        }

        ticks.clear();
    }

    public void addPartialCandle(CandleImpl candleImpl) {
        if (candleImpl.getHigh() > high) {
            high = candleImpl.getHigh();
        }
        if (candleImpl.getLow() < low) {
            low = candleImpl.getLow();
        }
        if (volume == 0) {
            open = candleImpl.getOpen();
            time = candleImpl.getTime();
        }

        volume += candleImpl.getVolume();
        if (volume >= numberOfTick) {
            close = candleImpl.getClose();
            isCompleted = true;
        }
    }
}
