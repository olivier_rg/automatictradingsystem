package com.system.trading.core.strategy.par;

public class PriceActionRegressionPropertiesName {
    public static final String SLOPE_SIZE = "price_action_regression_strategy.slope_size";
    public static final String TIME_START = "price_action_regression_strategy.time.start";
    public static final String TIME_END = "price_action_regression_strategy.time.end";
    public static final String SLOPE_VALUE = "price_action_regression_strategy.slope_value";
    public static final String TRAILING_STOP_POINT = "price_action_regression_strategy.trailing_stop_point";
    public static final String TAKE_PROFIT_POINT = "price_action_regression_strategy.take_profit_point";
    public static final String STOP_LOSS_POINT = "price_action_regression_strategy.stop_loss_point";
}
