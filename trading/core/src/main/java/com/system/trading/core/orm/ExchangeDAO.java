package com.system.trading.core.orm;

import com.system.trading.core.orm.InstrumentDAO;
import com.system.trading.core.pojo.Exchange;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Singleton
public class ExchangeDAO {
    static Logger logger = Logger.getLogger(InstrumentDAO.class.getName());
    private final SessionFactory sessionFactory;

    @Inject
    public ExchangeDAO(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    public List<Exchange> getExchanges() {
        Session session = sessionFactory.openSession();

        CriteriaQuery<Exchange> cq = session.getCriteriaBuilder().createQuery(Exchange.class);
        CriteriaQuery<Exchange> all = cq.select(cq.from(Exchange.class));

        TypedQuery<Exchange> allQuery = session.createQuery(all);
        List<Exchange> exchanges = allQuery.getResultList();
        session.close();

        logger.info("There are " + exchanges.size() + " Exchange Records Available.");

        return exchanges;
    }
}
