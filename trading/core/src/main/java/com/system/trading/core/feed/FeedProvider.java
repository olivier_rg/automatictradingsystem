package com.system.trading.core.feed;

import com.system.trading.core.Contract;
import com.system.trading.core.candle.CandleListener;
import com.system.trading.core.tick.TickListener;

import java.util.Date;

public interface FeedProvider {
    void addFeedProviderStateListener(FeedProviderStateListener feedProviderStateListener);

    void addTickListener(TickListener tickListener);

    void addCandleListener(CandleListener candleListener);

    void add(Contract contract);

    /**
     * Loading candle listeners with one hour worth of 10 sec candles.
     */
    void loadWithCandle();

    /**
     * Loading tick listeners with tick data. Data is between startLoadingDate and current date time when the method is called.
     *
     * @Param startLoadingDate Start Loading Date
     */
    void loadWithTick(Date startLoadingDate);

    void subscribe();
}
