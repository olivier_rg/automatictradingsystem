package com.system.trading.core.ta.slope;

import com.system.trading.core.pojo.Tick;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class SlopeMovingTickImpl implements SlopeMovingTick {
    private static final Logger LOGGER = Logger.getLogger(SlopeMovingTickImpl.class.getName());
    private final List<SlopeTickListener> slopeTickListeners;
    private final SlopeMovingTickModel slopeMovingTickModel;
    private SimpleRegression regression;

    private SlopeMovingTickImpl() {
        slopeTickListeners = null;
        slopeMovingTickModel = null;
    }

    public SlopeMovingTickImpl(SlopeMovingTickModel slopeMovingTickModel) {
        this.slopeMovingTickModel = slopeMovingTickModel;
        slopeTickListeners = new ArrayList<>();
    }

    @Override
    public double calculate() {
        regression = new SimpleRegression();
        slopeMovingTickModel.getTicks().forEach(tick -> regression.addData(tick.getDatetime(), tick.getPrice()));
        return regression.getSlope();
    }

    @Override
    public int size() {
        return slopeMovingTickModel.getTicks().size();
    }

    @Override
    public void addListener(SlopeTickListener slopeTickListener) {
        slopeTickListeners.add(slopeTickListener);
    }

    @Override
    public <T> void add(T value) {
        if (value instanceof Tick)
            slopeMovingTickModel.getTicks().add((Tick) value);
        else
            LOGGER.info("Added value to slope is not type of Tick.");
    }

    @Override
    public void clear() {
        slopeMovingTickModel.getTicks().clear();
    }

    @Override
    public void registerState() {
        slopeTickListeners.forEach(slopeTickListener -> slopeTickListener.calculatedSlopeValue(new ArrayList<>(slopeMovingTickModel.getTicks()), regression));
    }

    @Override
    public String getId() {
        return slopeMovingTickModel.getId();
    }
}
