package com.system.trading.core.ta.slope;

import com.system.trading.core.Identifier;
import com.system.trading.core.ta.Tool;

public interface Slope extends Tool, Identifier {
    <T> void add(T value);

    void clear();

    double calculate();

    int size();
}
