package com.system.trading.core.ta.ma;

import com.system.trading.core.candle.CandleListener;

public interface MovingAverage extends CandleListener {
    boolean isReady();

    /**
     * Get the moving average value at index. The cursor start at the end of the list. To get the current value,
     * index is equal to 0. To get the previous value, index is equal to -1
     * @param index start from the end
     * @return moving average value
     */
    double get(int index);

    int size();

    void addListener(MovingAverageListener movingAverageListener);
}
