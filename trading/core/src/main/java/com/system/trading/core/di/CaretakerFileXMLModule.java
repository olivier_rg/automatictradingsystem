package com.system.trading.core.di;

import com.system.trading.core.memento.*;
import com.thoughtworks.xstream.XStream;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;

@Module
public class CaretakerFileXMLModule {
    @Singleton
    @Provides
    Caretaker provideCaretakerImpl(
            @Named(MementoObjectId.CARETAKER_XML_FILE_DIRECTORY_PATH) String inputDirectory) {
        return new CaretakerFileXMLImpl(inputDirectory, new XStream());
    }

    @Singleton
    @Provides
    OriginatorFactory provideOriginatorFactory(Caretaker caretaker,
                                               @Named(MementoObjectId.CARETAKER_XML_FILE_DIRECTORY_PATH) String directoryPath) {
        return new OriginatorXMLFileFactory(caretaker, new XStream(), directoryPath);
    }

    @Singleton
    @Provides
    @Named(MementoObjectId.CARETAKER_XML_FILE_DIRECTORY_PATH)
    String provideXMLFileOutputDirectory() {
        File saveDirectory = new File("save/");
        if (!saveDirectory.exists()) {
            saveDirectory.mkdirs();
        }
        return saveDirectory.getAbsolutePath();
    }
}
