package com.system.trading.core.candle;

public interface Candle {
    long getTime();

    float getOpen();

    float getClose();

    float getLow();

    float getHigh();

    int getVolume();
}
