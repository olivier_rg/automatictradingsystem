package com.system.trading.core.di;

import com.system.trading.core.feed.FeedProvider;
import com.system.trading.core.order.OrderManager;
import com.system.trading.core.strategy.StrategyManager;

public interface CoreTradingComponent {
    StrategyManager strategyManager();
    OrderManager orderManager();
    FeedProvider feedProvider();
}

