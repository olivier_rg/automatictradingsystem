package com.system.trading.core.di;

import com.system.trading.core.CoreObjectId;
import com.system.trading.core.util.PropertiesUtil;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Properties;

@Module
public class CommonPropertiesModule {

    @Singleton
    @Provides
    @Named(CoreObjectId.PROPERTY_GENERAL)
    Properties provideGeneralProperties() {
        return PropertiesUtil.retrievePropertiesFromBaseFolder("settings");
    }

    @Singleton
    @Provides
    @Named(CoreObjectId.PROPERTY_STRATEGY)
    Properties provideStrategyProperties() {
        return PropertiesUtil.retrievePropertiesFromBaseFolder("strategies");
    }
}
