package com.system.trading.core.order;

import lombok.Builder;
import lombok.Getter;

@Builder
public class AccountConfiguration {
    @Getter
    float initialCapital;
}
