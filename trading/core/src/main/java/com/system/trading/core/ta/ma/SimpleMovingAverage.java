package com.system.trading.core.ta.ma;

public class SimpleMovingAverage extends MovingAverageImpl {

    public SimpleMovingAverage(int numberOfValue) {
        super(numberOfValue);
    }

    @Override
    protected void calculateAverage() {
        double value = candleValues.stream().mapToDouble(Float::doubleValue).sum() / this.numberOfValue;
        values.add((float) value);
    }
}
