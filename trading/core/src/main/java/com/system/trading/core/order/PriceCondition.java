package com.system.trading.core.order;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
public class PriceCondition {
    @NonNull
    @Getter
    private final Float price;
    @NonNull
    @Getter
    private final PriceConditionRequirement priceConditionRequirement;

    public boolean state(float price) {
        boolean isSuccessful = false;

        switch (priceConditionRequirement) {
            case INFERIOR_OR_EQUAL:
                isSuccessful = price <= this.price;
                break;
            case SUPERIOR_OR_EQUAL:
                isSuccessful = price >= this.price;
                break;
        }

        return isSuccessful;
    }
}
