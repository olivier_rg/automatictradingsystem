package com.system.trading.core.strategy;

import com.system.trading.core.Contract;

public interface StrategyManager {
    <T> void add(Class<T> strategyClassName, Contract contract);

    void addStrategyListener(StrategyListener strategyListener);
}
