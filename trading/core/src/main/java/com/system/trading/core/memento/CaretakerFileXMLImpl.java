package com.system.trading.core.memento;

import com.system.trading.core.Identifier;
import com.system.trading.core.util.CollectorUtil;
import com.system.trading.core.util.FileUtil;
import com.thoughtworks.xstream.XStream;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CaretakerFileXMLImpl implements Caretaker {
    private final List<Originator> originators;
    private final List<Identifier> parsedObject;

    public CaretakerFileXMLImpl(String directoryPath, XStream xStream) {
        originators = new ArrayList<>();
        parsedObject = new ArrayList<>();

        deserializeOriginators(directoryPath, xStream);
    }

    private void deserializeOriginators(String directoryPath, XStream xStream) {
        File directory = new File(directoryPath);

        for (String filename : directory.list()) {
            try {
                String xml = FileUtil.readFile(directoryPath + filename, StandardCharsets.US_ASCII);

                Identifier originator = (Identifier) xStream.fromXML(xml);
                parsedObject.add(originator);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Identifier get(String id) {
        return parsedObject.stream()
                .filter(identifier -> identifier.getId().equals(id))
                .collect(CollectorUtil.toSingleton());
    }

    @Override
    public void add(Originator originator) {
        if (!originators.contains(originator)) {
            originators.add(originator);
        }
    }

    @Override
    public void save() {
        String date = String.valueOf(new Date().getTime());
        originators.forEach(originator -> originator.save(date));
    }
}
