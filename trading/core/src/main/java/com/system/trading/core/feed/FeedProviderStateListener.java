package com.system.trading.core.feed;

public interface FeedProviderStateListener {

    void notify(FeedProviderState feedProviderState);
}
