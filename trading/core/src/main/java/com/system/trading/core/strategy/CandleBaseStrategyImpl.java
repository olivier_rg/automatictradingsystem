package com.system.trading.core.strategy;

import com.system.trading.core.candle.CandleListener;
import com.system.trading.core.order.OrderManager;

public abstract class CandleBaseStrategyImpl extends StrategyImpl implements CandleListener {

    public CandleBaseStrategyImpl(OrderManager orderManager) {
        super(orderManager);
    }
}
