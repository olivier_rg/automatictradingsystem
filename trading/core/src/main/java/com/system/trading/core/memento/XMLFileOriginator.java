package com.system.trading.core.memento;

import com.thoughtworks.xstream.XStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class XMLFileOriginator implements Originator {
    private static int id = 0;
    private final XStream xStream;
    private final Object model;
    private final String outputDirectoryPath;

    public XMLFileOriginator(XStream xStream, Object model, String outputDirectoryPath) {
        this.xStream = xStream;
        this.model = model;
        this.outputDirectoryPath = outputDirectoryPath;
    }

    @Override
    public void save(String saveIntoObjectId) {
        String path = outputDirectoryPath + "/" + model.getClass().getSimpleName() + "-" + saveIntoObjectId + "-" + id +
                ".xml";
        File file = new File(path);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        xStream.toXML(model, fileOutputStream);
        id++;
    }
}