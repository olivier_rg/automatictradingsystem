package com.system.trading.core;

public interface Initializer {
    void initialize();
}
