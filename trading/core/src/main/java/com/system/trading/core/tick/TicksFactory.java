package com.system.trading.core.tick;

import com.system.trading.core.feed.FeedProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TicksFactory {

    private final FeedProvider feedProvider;

    @Inject
    public TicksFactory(FeedProvider feedProvider) {

        this.feedProvider = feedProvider;
    }

    public Ticks create() {
        Ticks ticks = new Ticks();
        feedProvider.addTickListener(ticks);
        return ticks;
    }
}
