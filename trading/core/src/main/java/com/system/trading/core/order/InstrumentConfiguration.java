package com.system.trading.core.order;

public interface InstrumentConfiguration {
    float getPipValue(String symbol);

    float getProfitPipValue(String symbol);

    float getTotalOneSideCommission(String symbol);

    float getMarginRequirement(String symbol);
}
