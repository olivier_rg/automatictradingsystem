package com.system.trading.core.util;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
    private static final Logger LOGGER = Logger.getLogger(PropertiesUtil.class.getName());

    public static Properties retrievePropertiesInResourcesFolder(String propertyFileName) {
        Properties properties = new Properties();

        try (InputStream input = PropertiesUtil.class.getClassLoader().getResourceAsStream(propertyFileName + ".properties")) {
            properties.load(input);
        } catch (IOException ex) {
            LOGGER.error("Cannot load property file from resource folder.");
            LOGGER.error(ex.getMessage());
            LOGGER.error(ex.getStackTrace());
        }

        return properties;
    }

    public static Properties retrievePropertiesFromBaseFolder(String propertiesFileName) {
        Properties properties = new Properties();
        FileInputStream file;
        String path = "./" + propertiesFileName + ".properties";
        try {
            file = new FileInputStream(path);
            properties.load(file);
            file.close();
        } catch (IOException e) {
            LOGGER.error("Properties file not found: " + propertiesFileName);
            LOGGER.error(e.getMessage());
            LOGGER.error(e.getStackTrace());
        }

        return properties;
    }
}
