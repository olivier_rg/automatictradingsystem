package com.system.trading.core.pojo;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "instrument")
public class Instrument {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "symbol")
    private String symbol;

    @ManyToOne
    @JoinColumn(name = "instrumenttypeid", nullable = false)
    private InstrumentType instrumentType;

    public Instrument() {
    }

    public Instrument(int id, String symbol) {
        this.id = id;
        this.symbol = symbol;
    }
}
