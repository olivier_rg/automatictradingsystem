package com.system.trading.core.strategy;

import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.candle.CandleListFactory;
import com.system.trading.core.candle.CandleTicks;
import com.system.trading.core.order.OrderActionType;
import com.system.trading.core.order.OrderManager;
import com.system.trading.core.order.OrderPriceInformation;
import com.system.trading.core.order.OrderType;

public class FakeOutStrategy extends CandleBaseStrategyImpl {
    private final CandleTicks candleTicks;

    public FakeOutStrategy(CandleListFactory candleListFactory, OrderManager orderManager) {
        super(orderManager);
        candleTicks = candleListFactory.createCandleTicks(50);
        candleTicks.addCandleListener(this);
    }

    @Override
    public void notify(CandleImpl currentCandleImpl, boolean isLoading) {
        long time = currentCandleImpl.getTime();
        //String timeReadable = Util.UnixSecondsToString(time, "yyyyMMdd-HH:mm:ss zzz");
        float open = currentCandleImpl.getOpen();
        float close = currentCandleImpl.getClose();
        float high = currentCandleImpl.getHigh();
        float low = currentCandleImpl.getLow();
        int volume = currentCandleImpl.getVolume();

        /*System.out.println();
        System.out.println("Candle: " + timeReadable + " High:" + " " + high + " Low: " + low + " Open: " + open + " " +
                "Close: " + close + " Volume: " + volume);
*/
        CandleImpl previousCandleImpl = getPreviousCandle();
        if (previousCandleImpl == null)
            return;

       /* System.out.println("Previous Candle Time: " + Util.UnixSecondsToString(previousCandle.getTime(), "yyyyMMdd-HH:mm" +
                ":ss zzz"));
        System.out.println("Current Candle Time: " + Util.UnixSecondsToString(currentCandle.getTime(), "yyyyMMdd-HH" +
                ":mm" +
                ":ss zzz"));
*/
        if (orderManager.hasPosition(getContract()))
            return;

        orderManager.cancelPendingOrders();

        float price;
        float stopLoss;
        float takeProfit;
        int quantity = 1;

        // Condition go LONG
        if (isGoingLong(currentCandleImpl, previousCandleImpl)) {
            price = currentCandleImpl.getHigh();
            stopLoss = price - 2;
            takeProfit = price + 0.25f;
/*
            PriceCondition priceCondition =
                    PriceCondition.builder().price(price).priceConditionRequirement(SUPERIOR_OR_EQUAL).build();
*/
            OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(price)
                    .takeProfitPoint(takeProfit).stopLossPoint(stopLoss).build();

            try {
                orderManager.buy(getContract(), OrderType.LIMIT, OrderActionType.BUY, quantity, orderPriceInformation);
            } catch (Exception e) {
                e.printStackTrace();
            }


            System.out.println("Request buy " + getContract().getSymbol() + " Price:" + price + " Stop " +
                    "loss: " + stopLoss + " Take profit: " + takeProfit);
        }
        // Condition go SHORT
        else if (isGoingShort(currentCandleImpl, previousCandleImpl)) {
            price = currentCandleImpl.getLow();
            stopLoss = price + 2;
            takeProfit = price - 0.25f;
/*
            PriceCondition priceCondition =
                    PriceCondition.builder().price(price).priceConditionRequirement(INFERIOR_OR_EQUAL).build();
*/
            OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder().requestPrice(price)
                    .takeProfitPoint(takeProfit).stopLossPoint(stopLoss).build();

            try {
                orderManager.buy(getContract(), OrderType.LIMIT, OrderActionType.SELL, quantity, orderPriceInformation);
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("Request short " + getContract().getSymbol() + " Price" +
                    ":" + price +
                    " Stop " +
                    "loss: " + stopLoss + " Take profit: " + takeProfit);
        }
    }


    private boolean isGoingShort(CandleImpl currentCandleImpl, CandleImpl previousCandleImpl) {
        return currentCandleImpl.getHigh() > previousCandleImpl.getHigh() && currentCandleImpl.getLow() > previousCandleImpl.getLow();
    }

    private boolean isGoingLong(CandleImpl currentCandleImpl, CandleImpl previousCandleImpl) {
        return currentCandleImpl.getHigh() < previousCandleImpl.getHigh() && currentCandleImpl.getLow() < previousCandleImpl.getLow();
    }

    private CandleImpl getPreviousCandle() {
        CandleImpl candleImpl = null;

        try {
            candleImpl = candleTicks.getCandles(-1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return candleImpl;
    }
}
