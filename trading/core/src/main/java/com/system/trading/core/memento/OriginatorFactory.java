package com.system.trading.core.memento;

public interface OriginatorFactory {
    void create(Object model);
}
