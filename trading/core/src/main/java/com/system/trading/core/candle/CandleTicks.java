package com.system.trading.core.candle;

import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class CandleTicks implements TickListener, CandleListener {
    private static final Logger LOGGER = Logger.getLogger(CandleTicks.class.getName());

    private final int numberOfTick;
    private final List<CandleTick> candles;
    private final List<CandleListener> candleListeners;

    public CandleTicks(int numberOfTick) {
        this.numberOfTick = numberOfTick;
        candles = new ArrayList<>();
        candleListeners = new ArrayList<>();
    }

    private void addTick(Tick tick) {
        CandleTick currentCandleImpl = getCurrentCandle();
        currentCandleImpl.addTick(tick);

        // LOGGER.info("Tick: price " + tick.getPrice() + " volume " + tick.getVolume());
        // LOGGER.info("Candle: volume " + currentCandle.getVolume());

        if (currentCandleImpl.isCompleted()) {
            notify(currentCandleImpl);
            candles.add(new CandleTick(this.numberOfTick));
        }
    }

    private CandleTick getCurrentCandle() {
        CandleTick currentCandle;

        if (candles.size() == 0) {
            currentCandle = new CandleTick(this.numberOfTick);
            candles.add(currentCandle);
        } else {
            currentCandle = candles.get(candles.size() - 1);
        }

        return currentCandle;
    }

    public void addCandleListener(CandleListener candleListener) {
        candleListeners.add(candleListener);
    }

    private void notify(CandleImpl candleImpl) {
        candleListeners.forEach(candleListener -> candleListener.notify(candleImpl, false));
    }

    /**
     * Get a candle starting at the end.
     *
     * @param index Index should always be negatif
     * @return candle
     */
    public CandleImpl getCandles(int index) throws Exception {
        if (index > 0) {
            throw new Exception("Index should be negative, not found");
        }

        if (candles.size() == 0) {
            return null;
        }

        int realIndex = candles.size() - 1 + index;

        if (realIndex < 0) {
            return null;
        }

        return candles.get(realIndex);
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        addTick(tick);
    }

    @Override
    public void notify(CandleImpl candleImpl, boolean isLoading) {
        if (isLoading) {
            CandleTick currentCandleImpl = getCurrentCandle();
            currentCandleImpl.addPartialCandle(candleImpl);

            if (currentCandleImpl.isCompleted()) {
                notify(currentCandleImpl);
                candles.add(new CandleTick(this.numberOfTick));
            }
        }
        else{
            LOGGER.error("Notify candle when it is not loading, not implemented");
        }
    }
}
