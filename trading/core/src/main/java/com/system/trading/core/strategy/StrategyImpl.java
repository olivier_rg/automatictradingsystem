package com.system.trading.core.strategy;


import com.system.trading.core.Contract;
import com.system.trading.core.order.OrderManager;

abstract class StrategyImpl implements Strategy {
    protected final OrderManager orderManager;
    private Contract contract;

    public StrategyImpl(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    @Override
    public Contract getContract() {
        return this.contract;
    }

    @Override
    public final void setContract(Contract contract) {
        this.contract = contract;
    }
}