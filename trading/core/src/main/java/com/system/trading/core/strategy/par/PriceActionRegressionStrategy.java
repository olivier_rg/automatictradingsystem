package com.system.trading.core.strategy.par;

import com.system.trading.core.order.*;
import com.system.trading.core.strategy.TickBaseStrategyImpl;
import com.system.trading.core.ta.slope.SlopeFactory;
import com.system.trading.core.ta.slope.SlopeMovingTick;
import com.system.trading.core.ta.slope.SlopeTickAggregator;
import com.system.trading.core.pojo.Tick;
import com.system.trading.core.tick.TickListener;
import com.system.trading.core.tick.Ticks;
import com.system.trading.core.tick.TicksFactory;
import com.system.trading.core.util.DateUtil;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Properties;

@Singleton
public class PriceActionRegressionStrategy extends TickBaseStrategyImpl implements TickListener {
    private static final Logger LOGGER = Logger.getLogger(PriceActionRegressionStrategy.class.getName());
    private final SlopeMovingTick slope;
    private Integer slopeSize = null;
    private Integer timeStart = null;
    private Integer timeEnd = null;
    private Float slopeValue = null;
    private Float trailingStopPoint = null;
    private Float takeProfitPoint = null;
    private Float stopLossPoint = null;

    @Inject
    public PriceActionRegressionStrategy(OrderManager orderManager, TicksFactory ticksFactory,
                                         SlopeFactory slopeFactory, Properties properties) {
        super(orderManager);
        initializeStrategyProperties(properties);

        Ticks ticks = ticksFactory.create();
        ticks.addListener(this);

        slope = slopeFactory.createTickMovingSlope(PriceActionRegressionObjectId.SLOPE_MOVING, slopeSize);
        ticks.addListener(new SlopeTickAggregator(slope));
    }

    private void initializeStrategyProperties(Properties properties) {
        slopeSize = Integer.parseInt(properties.getProperty(PriceActionRegressionPropertiesName.SLOPE_SIZE));
        slopeValue = Float.parseFloat(properties.getProperty(PriceActionRegressionPropertiesName.SLOPE_VALUE));

        try {
            timeStart = Integer.parseInt(properties.getProperty(PriceActionRegressionPropertiesName.TIME_START));
        } catch (Exception ignored) {
        }

        try {
            timeEnd = Integer.parseInt(properties.getProperty(PriceActionRegressionPropertiesName.TIME_END));
        } catch (Exception ignored) {
        }

        try {
            trailingStopPoint =
                    Float.parseFloat(properties.getProperty(PriceActionRegressionPropertiesName.TRAILING_STOP_POINT));
        } catch (Exception ignored) {
        }

        try {
            takeProfitPoint =
                    Float.parseFloat(properties.getProperty(PriceActionRegressionPropertiesName.TAKE_PROFIT_POINT));
        } catch (Exception ignored) {
        }

        try {
            stopLossPoint =
                    Float.parseFloat(properties.getProperty(PriceActionRegressionPropertiesName.STOP_LOSS_POINT));
        } catch (Exception ignored) {
        }
    }

    @Override
    public void notify(Tick tick, boolean isLoading) {
        if (isLoading)
            return;

        if (timeStart != null && timeEnd != null) {
            if (!DateUtil.isBetweenHours(tick.getDatetime(), timeStart, timeEnd)) {
                slope.clear();
                return;
            }
        }

        if (orderManager.hasPosition(getContract())) {
            slope.clear();
            return;
        }

        if (slope.size() < slopeSize)
            return;

        cancelOrdersToFar(tick);

        // Cancel pending orders that their price are way to far from the current tick trading price
        if (orderManager.hasPendingOrders())
            return;

        double slopeValue = slope.calculate();

        OrderActionType orderActionType = null;
        OrderPriceInformation orderPriceInformation = OrderPriceInformation.builder()
                .requestPrice(tick.getPrice())
                .trailingStopPoint(trailingStopPoint)
                .takeProfitPoint(takeProfitPoint)
                .stopLossPoint(stopLossPoint)
                .build();

        if (slopeValue > this.slopeValue)
            orderActionType = OrderActionType.BUY;
        else if (slopeValue < -this.slopeValue)
            orderActionType = OrderActionType.SELL;

        if (orderActionType != null) {
            LOGGER.info("Slope value: " + slopeValue);
            try {
                // todo: It is not the strategy responsibility to trigger the registerState of all tool...
                slope.registerState();
                orderManager.buy(getContract(), OrderType.LIMIT, orderActionType, 1, orderPriceInformation);

                // Print into console to build a feature data set.
                //System.out.print(DateUtil.secondsOfTheDay(tick.getTimeUnixSeconds()) + "," + orderPriceInformation.getRequestPrice() + "," + slopeValue + "," + slope.getTotalNumberContract() + ",");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // todo: move this method to the order manager, so getLimitPendingOrders and getLimitPenderOrderById can be removed
    private void cancelOrdersToFar(Tick tick) {
        List<Integer> orders = orderManager.getLimitPendingOrderIds();

        for (Integer orderId : orders) {
            Order order = orderManager.getLimitPendingOrderById(orderId);

            if (tick.getPrice() > order.getRequestPrice() + 1 || tick.getPrice() < order.getRequestPrice() - 1) {
                LOGGER.info("Cancel order to far id: " + orderId);
                orderManager.cancelPendingOrderById(orderId);
            }
        }
    }
}
