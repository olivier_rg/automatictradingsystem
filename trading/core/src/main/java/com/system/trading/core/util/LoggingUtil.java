package com.system.trading.core.util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class LoggingUtil {
    public static void initializeLoggingLevel() {
        String loggingAction = System.getProperty("logging.level", "");

        Level level = Level.ALL;

        switch (loggingAction.toLowerCase()) {
            case "off":
                level = Level.OFF;
                break;
            case "info":
                level = Level.INFO;
                break;
            case "error":
                level = Level.ERROR;
                break;
            case "warn":
                level = Level.WARN;
                break;
        }

        Logger.getRootLogger().setLevel(level);
    }
}
