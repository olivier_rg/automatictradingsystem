package com.system.trading.core.ta.slope;

import com.system.trading.core.memento.Caretaker;
import com.system.trading.core.memento.OriginatorFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class SlopeFactory {
    private final Map<String, Slope> slopeMap;
    private final Caretaker caretaker;
    private final OriginatorFactory originatorFactory;

    @Inject
    public SlopeFactory(Caretaker caretaker, OriginatorFactory originatorFactory) {
        this.caretaker = caretaker;
        this.originatorFactory = originatorFactory;
        slopeMap = new HashMap<>();
    }

    // todo: to be removed in ATS version 2, once the JMS is implemented
    public Slope retrieveSlope(String id) {
        Slope slope = null;

        if (slopeMap.containsKey(id))
            slope = slopeMap.get(id);

        return slope;
    }

    public SlopeMovingTick createTickMovingSlope(String id, int size) {
        SlopeMovingTickImpl slope;
        SlopeMovingTickModel slopeMovingTickModel = null;

        if (caretaker.get(id) != null) {
            slopeMovingTickModel = (SlopeMovingTickModel) caretaker.get(id);
        }

        if (slopeMap.containsKey(id)) {
            slope = (SlopeMovingTickImpl) slopeMap.get(id);
        } else {
            if(slopeMovingTickModel == null){
                slopeMovingTickModel = new SlopeMovingTickModel(id, size);
            }

            slope = new SlopeMovingTickImpl(slopeMovingTickModel);
            slopeMap.put(id, slope);

            originatorFactory.create(slopeMovingTickModel);
        }

        return slope;
    }
}
