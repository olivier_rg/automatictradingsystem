package com.system.trading.core.memento;

import com.thoughtworks.xstream.XStream;

public class OriginatorXMLFileFactory implements OriginatorFactory {
    private final Caretaker caretaker;
    private final XStream xStream;
    private final String outputDirectoryPath;

    public OriginatorXMLFileFactory(Caretaker caretaker, XStream xStream, String outputDirectoryPath) {
        this.caretaker = caretaker;
        this.xStream = xStream;
        this.outputDirectoryPath = outputDirectoryPath;
    }

    @Override
    public void create(Object model) {
        XMLFileOriginator XMLFileOriginator = new XMLFileOriginator(xStream, model, outputDirectoryPath);
        caretaker.add(XMLFileOriginator);
    }
}
