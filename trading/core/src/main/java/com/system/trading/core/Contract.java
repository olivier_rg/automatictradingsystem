package com.system.trading.core;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Contract implements Comparable<Contract> {
    String symbol;
    SecurityType secType;
    String currency;
    String exchange;
    String lastTradeDateOrContractMonth;

    // todo: override equals instead of compareto
    @Override
    public int compareTo(Contract o) {
        boolean isEqual =
                symbol.equals(o.symbol) && secType.equals(o.secType) && currency.equals(o.currency) && exchange.equals(o.exchange);

        if (isEqual)
            return 0;
        else
            return 1;
    }
}
