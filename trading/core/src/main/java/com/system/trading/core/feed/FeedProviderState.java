package com.system.trading.core.feed;

public enum FeedProviderState {
    SUBSCRIBE,
    FINISHED_SUBSCRIBED
}
