package com.system.trading.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
    public static final int SECONDS_IN_HOUR = 3600;
    public static final int SECONDS_IN_MINUTE = 60;
    public static final int MILLISECOND_IN_SECOND = 1000;
    public static final int MILLISECOND_IN_DAY = 86400000;

    private DateUtil() {
    }

    /**
     * Time is between hours i.e tick.getDatetime(), 930, 1630)
     *
     * @param unixSeconds time in second
     * @param from        example: 930
     * @param to          example: 1630
     * @return boolean
     */
    public static boolean isBetweenHours(long unixSeconds, int from, int to) {
        Date date = new Date(unixSeconds * MILLISECOND_IN_SECOND);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int t = c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE);
        return to > from && t >= from && t <= to || to < from && (t >= from || t <= to);
    }

    /**
     * Retrieve the total number of seconds from that day.
     *
     * @param unixSeconds unix time in seconds
     * @return total number of seconds from that day
     */
    public static int secondsOfTheDay(long unixSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(unixSeconds * MILLISECOND_IN_SECOND));
        return calendar.get(Calendar.HOUR_OF_DAY) * SECONDS_IN_HOUR + calendar.get(Calendar.MINUTE) * SECONDS_IN_MINUTE + calendar.get(Calendar.SECOND);
    }


    /**
     * Increment a date by one day
     *
     * @param date Date
     * @return the incremented date
     */
    public static Date addDay(Date date) {
        return new Date(date.getTime() + MILLISECOND_IN_DAY);
    }

    /**
     * Increment a date by one second
     *
     * @param date Date
     * @return the incremented date
     */
    public static Date addSecond(Date date) {
        return new Date(date.getTime() + MILLISECOND_IN_SECOND);
    }

    /**
     * Create a date based on time of the year.
     *
     * @param year            Year
     * @param month           Month, month start with 0
     * @param dayOfTheMonth   Day Of The Month
     * @param hourOfTheDay    hour
     * @param minuteOfTheHour Minute
     * @return Date
     */
    public static Date createDate(int year, int month, int dayOfTheMonth, int hourOfTheDay, int minuteOfTheHour) {
        return new GregorianCalendar(year, month - 1, dayOfTheMonth, hourOfTheDay, minuteOfTheHour).getTime();
    }

    /**
     * Get millisecond from time by passing it string value and time format.
     *
     * @param time       Time
     * @param timeFormat Time Format
     * @return Time in millisecond
     */
    public static long getMillisecondFromTime(String time, String timeFormat) {
        Date date = getDateFromString(time, timeFormat);

        long timeLong = 0;
        if (date != null) {
            timeLong = date.getTime();
        }

        return timeLong;
    }

    /**
     * Retrieve a date from string using a pre define time format.
     * @param stringDate string formatted date
     * @param timeFormat time format
     * @return date
     */
    public static Date getDateFromString(String stringDate, String timeFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
        Date date = null;

        try {
            date = sdf.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }
}
