package com.system.trading.core.orm;

import com.system.trading.core.pojo.CandleImpl;
import com.system.trading.core.pojo.Instrument;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Singleton
public class CandleDAO {
    private static final Logger LOGGER = Logger.getLogger(CandleDAO.class.getName());
    private final SessionFactory sessionFactory;

    @Inject
    public CandleDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void insert(CandleImpl candleImplToInsert) {
        var session = getSession();
        session.beginTransaction();
        session.save(candleImplToInsert);
        session.getTransaction().commit();
    }

    public List<CandleImpl> getCandlesByInstrument(Instrument instrument) {
        var session = getSession();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<CandleImpl> criteriaQuery = criteriaBuilder.createQuery(CandleImpl.class);
        Root<CandleImpl> candleImplRoot = criteriaQuery.from(CandleImpl.class);

        Predicate predicateForCandle
                = criteriaBuilder.equal(candleImplRoot.get("instrument"), instrument);
        criteriaQuery.where(predicateForCandle);

        List<CandleImpl> candles = session.createQuery(criteriaQuery).getResultList();

        session.close();
        LOGGER.info("There are " + candles.size() + " Candle Records Available.");

        return candles;
    }

    public CandleImpl getLastCandle(Instrument instrument) {

        if(instrument == null){
            return null;
        }

        var session = getSession();
        var query =
                "SELECT c FROM CandleImpl c JOIN Instrument i ON c.instrument = i WHERE i.symbol = '"+instrument.getSymbol()+"' ORDER BY c.time DESC";
        var criteriaQuery = session.createQuery(query);

        List<CandleImpl> candles = criteriaQuery.setMaxResults(1).getResultList();
        CandleImpl candle = null;

        if (candles.size() > 0)
            candle = candles.get(0);

        session.close();

        return candle;
    }

    private Session getSession() {
        return sessionFactory.openSession();
    }
}
