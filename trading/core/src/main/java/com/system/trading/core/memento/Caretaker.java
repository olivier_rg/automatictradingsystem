package com.system.trading.core.memento;

import com.system.trading.core.Identifier;

public interface Caretaker {
    Identifier get(String id);

    void add(Originator originator);

    void save();
}
