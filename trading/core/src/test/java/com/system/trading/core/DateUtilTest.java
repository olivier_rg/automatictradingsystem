package com.system.trading.core;

import com.system.trading.core.util.DateUtil;
import org.junit.Test;

import static org.junit.Assert.*;

public class DateUtilTest {
    @Test
    public void whenSecondIsOutOfRange_ReturnFalse() {
        // 17h30
        assertFalse(DateUtil.isBetweenHours(1601759500, 930, 1630));
    }

    @Test
    public void whenSecondIsInRange_ReturnTrue() {
        // 15h30
        assertTrue(DateUtil.isBetweenHours(1601752500, 930, 1630));
    }

    @Test
    public void whenTimeUnixSecondIsPass_RetrieveNumberOfSecondsFromThatDay() {
        assertEquals(27527, DateUtil.secondsOfTheDay(1602502727), 0.005);
    }
}